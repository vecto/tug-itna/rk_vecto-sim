﻿namespace TUGraz.VectoCommon.Models
{
	public enum MissionType
	{
		LongHaul,
		LongHaulEMS,
		RegionalDelivery,
		RegionalDeliveryEMS,
		UrbanDelivery,
		MunicipalUtility,
		Construction,
		HeavyUrban,
		Urban,
		Suburban,
		Interurban,
		Coach,
		VerificationTest,
		ExemptedMission
	}
}