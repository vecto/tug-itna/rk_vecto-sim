<!-- Note that this is a confidential issue. Only VECTO developers will have access to it. -->
<!-- Once the issue is solved, any confidential data attached will be removed and the issue will be made public for documentation purposes. -->

<!-- PLEASE DO NOT REMOVE THE "/confidential" STATEMENT BELOW. -->

/confidential

<!-- PLEASE, EDIT THE ISSUE TEMPLATE BELOW. -->

## Summary of the Article 10(2) issue

<!-- Summarize your purpose, problems encountered, or functionality missing. -->

## Additional information

<!-- Include the Vehicle Identification Number (VIN). -->
<!-- Explain what data is included. -->

