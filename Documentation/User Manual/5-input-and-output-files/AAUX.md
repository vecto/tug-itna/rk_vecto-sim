## Advanced Auxiliary Input Data (.aaux)

**Example: **

~~~json
{
  "$type": "VectoAuxiliaries.AuxiliaryConfig, BusAuxiliaries",
  "VectoInputs": {
    "$type": "VectoAuxiliaries.VectoInputs, AdvancedAuxiliaryInterfaces",
    "Cycle": "Urban",
    "VehicleWeightKG": 16500.0,
    "PowerNetVoltage": 28.3,
    "FuelMap": "testFuelGoodMap.vmap",
    "FuelDensity": null
  },
  "ElectricalUserInputsConfig": {
    "$type": "VectoAuxiliaries.Electrics.ElectricsUserInputsConfig, BusAuxiliaries",
    "PowerNetVoltage": 28.3,
    "AlternatorMap": "testCombAlternatorMap_1Alt.AALT",
    "AlternatorGearEfficiency": 0.92,
    "ElectricalConsumers": {
      "$type": "VectoAuxiliaries.Electrics.ElectricalConsumerList, BusAuxiliaries",
      "DoorDutyCycleFraction": 0.096,
      "Items": [
        {
          "$type": "VectoAuxiliaries.Electrics.ElectricalConsumer, BusAuxiliaries",
          "BaseVehicle": false,
          "Category": "Doors",
          "ConsumerName": "Doors per Door",
          "NominalConsumptionAmps": 3.0,
          "NumberInActualVehicle": 3,
          "PhaseIdle_TractionOn": 0.096339,
          "PowerNetVoltage": 28.3,
          "Info": ""
        },
        {
          "$type": "VectoAuxiliaries.Electrics.ElectricalConsumer, BusAuxiliaries",
          "BaseVehicle": true,
          "Category": "Veh Electronics &Engine",
          "ConsumerName": "Controllers,Valves etc",
          "NominalConsumptionAmps": 25.0,
          "NumberInActualVehicle": 1,
          "PhaseIdle_TractionOn": 1.0,
          "PowerNetVoltage": 28.3,
          "Info": ""
        },
        {
          "$type": "VectoAuxiliaries.Electrics.ElectricalConsumer, BusAuxiliaries",
          "BaseVehicle": false,
          "Category": "Vehicle basic equipment",
          "ConsumerName": "Radio City",
          "NominalConsumptionAmps": 2.0,
          "NumberInActualVehicle": 1,
          "PhaseIdle_TractionOn": 0.8,
          "PowerNetVoltage": 28.3,
          "Info": ""
        },
        {
          "$type": "VectoAuxiliaries.Electrics.ElectricalConsumer, BusAuxiliaries",
          "BaseVehicle": false,
          "Category": "Vehicle basic equipment",
          "ConsumerName": "Radio Intercity",
          "NominalConsumptionAmps": 5.0,
          "NumberInActualVehicle": 0,
          "PhaseIdle_TractionOn": 0.8,
          "PowerNetVoltage": 28.3,
          "Info": ""
        },
        {
          "$type": "VectoAuxiliaries.Electrics.ElectricalConsumer, BusAuxiliaries",
          "BaseVehicle": false,
          "Category": "Vehicle basic equipment",
          "ConsumerName": "Radio/Audio Tourism",
          "NominalConsumptionAmps": 9.0,
          "NumberInActualVehicle": 0,
          "PhaseIdle_TractionOn": 0.8,
          "PowerNetVoltage": 28.3,
          "Info": ""
        },
        {
          "$type": "VectoAuxiliaries.Electrics.ElectricalConsumer, BusAuxiliaries",
          "BaseVehicle": false,
          "Category": "Vehicle basic equipment",
          "ConsumerName": "Fridge",
          "NominalConsumptionAmps": 4.0,
          "NumberInActualVehicle": 0,
          "PhaseIdle_TractionOn": 0.5,
          "PowerNetVoltage": 28.3,
          "Info": ""
        },
        {
          "$type": "VectoAuxiliaries.Electrics.ElectricalConsumer, BusAuxiliaries",
          "BaseVehicle": false,
          "Category": "Vehicle basic equipment",
          "ConsumerName": "Kitchen Standard",
          "NominalConsumptionAmps": 67.0,
          "NumberInActualVehicle": 0,
          "PhaseIdle_TractionOn": 0.05,
          "PowerNetVoltage": 28.3,
          "Info": ""
        },
        {
          "$type": "VectoAuxiliaries.Electrics.ElectricalConsumer, BusAuxiliaries",
          "BaseVehicle": false,
          "Category": "Vehicle basic equipment",
          "ConsumerName": "Interior lights City/ Intercity + Doorlights [1/m]",
          "NominalConsumptionAmps": 1.0,
          "NumberInActualVehicle": 12,
          "PhaseIdle_TractionOn": 0.7,
          "PowerNetVoltage": 28.3,
          "Info": "1 Per metre length of bus"
        },
        {
          "$type": "VectoAuxiliaries.Electrics.ElectricalConsumer, BusAuxiliaries",
          "BaseVehicle": false,
          "Category": "Vehicle basic equipment",
          "ConsumerName": "LED Interior lights ceiling city/ontercity + door [1/m]",
          "NominalConsumptionAmps": 0.6,
          "NumberInActualVehicle": 0,
          "PhaseIdle_TractionOn": 0.7,
          "PowerNetVoltage": 28.3,
          "Info": "1 Per metre length of bus"
        },
        {
          "$type": "VectoAuxiliaries.Electrics.ElectricalConsumer, BusAuxiliaries",
          "BaseVehicle": false,
          "Category": "Vehicle basic equipment",
          "ConsumerName": "Interior lights Tourism + reading [1/m]",
          "NominalConsumptionAmps": 1.1,
          "NumberInActualVehicle": 0,
          "PhaseIdle_TractionOn": 0.7,
          "PowerNetVoltage": 28.3,
          "Info": "1 Per metre length of bus"
        },
        {
          "$type": "VectoAuxiliaries.Electrics.ElectricalConsumer, BusAuxiliaries",
          "BaseVehicle": false,
          "Category": "Vehicle basic equipment",
          "ConsumerName": "LED Interior lights ceiling Tourism + LED reading [1/m]",
          "NominalConsumptionAmps": 0.66,
          "NumberInActualVehicle": 0,
          "PhaseIdle_TractionOn": 0.7,
          "PowerNetVoltage": 28.3,
          "Info": "1 Per metre length of bus"
        },
        {
          "$type": "VectoAuxiliaries.Electrics.ElectricalConsumer, BusAuxiliaries",
          "BaseVehicle": false,
          "Category": "Customer Specific Equipment",
          "ConsumerName": "External Displays Font/Side/Rear",
          "NominalConsumptionAmps": 2.65017676,
          "NumberInActualVehicle": 4,
          "PhaseIdle_TractionOn": 1.0,
          "PowerNetVoltage": 28.3,
          "Info": ""
        },
        {
          "$type": "VectoAuxiliaries.Electrics.ElectricalConsumer, BusAuxiliaries",
          "BaseVehicle": false,
          "Category": "Customer Specific Equipment",
          "ConsumerName": "Internal display per unit ( front side rear)",
          "NominalConsumptionAmps": 1.06007063,
          "NumberInActualVehicle": 1,
          "PhaseIdle_TractionOn": 1.0,
          "PowerNetVoltage": 28.3,
          "Info": ""
        },
        {
          "$type": "VectoAuxiliaries.Electrics.ElectricalConsumer, BusAuxiliaries",
          "BaseVehicle": false,
          "Category": "Customer Specific Equipment",
          "ConsumerName": "CityBus Ref EBSF Table4 Devices ITS No Displays",
          "NominalConsumptionAmps": 9.3,
          "NumberInActualVehicle": 1,
          "PhaseIdle_TractionOn": 1.0,
          "PowerNetVoltage": 28.3,
          "Info": ""
        },
        {
          "$type": "VectoAuxiliaries.Electrics.ElectricalConsumer, BusAuxiliaries",
          "BaseVehicle": false,
          "Category": "Lights",
          "ConsumerName": "Exterior Lights BULB",
          "NominalConsumptionAmps": 7.4,
          "NumberInActualVehicle": 1,
          "PhaseIdle_TractionOn": 1.0,
          "PowerNetVoltage": 28.3,
          "Info": ""
        },
        {
          "$type": "VectoAuxiliaries.Electrics.ElectricalConsumer, BusAuxiliaries",
          "BaseVehicle": false,
          "Category": "Lights",
          "ConsumerName": "Day running lights LED bonus",
          "NominalConsumptionAmps": -0.723,
          "NumberInActualVehicle": 1,
          "PhaseIdle_TractionOn": 1.0,
          "PowerNetVoltage": 28.3,
          "Info": ""
        },
        {
          "$type": "VectoAuxiliaries.Electrics.ElectricalConsumer, BusAuxiliaries",
          "BaseVehicle": false,
          "Category": "Lights",
          "ConsumerName": "Antifog rear lights LED bonus",
          "NominalConsumptionAmps": -0.17,
          "NumberInActualVehicle": 1,
          "PhaseIdle_TractionOn": 1.0,
          "PowerNetVoltage": 28.3,
          "Info": ""
        },
        {
          "$type": "VectoAuxiliaries.Electrics.ElectricalConsumer, BusAuxiliaries",
          "BaseVehicle": false,
          "Category": "Lights",
          "ConsumerName": "Position lights LED bonus",
          "NominalConsumptionAmps": -1.2,
          "NumberInActualVehicle": 1,
          "PhaseIdle_TractionOn": 1.0,
          "PowerNetVoltage": 28.3,
          "Info": ""
        },
        {
          "$type": "VectoAuxiliaries.Electrics.ElectricalConsumer, BusAuxiliaries",
          "BaseVehicle": false,
          "Category": "Lights",
          "ConsumerName": "Direction lights LED bonus",
          "NominalConsumptionAmps": -0.3,
          "NumberInActualVehicle": 1,
          "PhaseIdle_TractionOn": 1.0,
          "PowerNetVoltage": 28.3,
          "Info": ""
        },
        {
          "$type": "VectoAuxiliaries.Electrics.ElectricalConsumer, BusAuxiliaries",
          "BaseVehicle": false,
          "Category": "Lights",
          "ConsumerName": "Brake Lights LED bonus",
          "NominalConsumptionAmps": -1.2,
          "NumberInActualVehicle": 1,
          "PhaseIdle_TractionOn": 1.0,
          "PowerNetVoltage": 28.3,
          "Info": ""
        }
      ]
    },
    "DoorActuationTimeSecond": 4,
    "ResultCardIdle": {
      "$type": "VectoAuxiliaries.Electrics.ResultCard, BusAuxiliaries",
      "Results": []
    },
    "ResultCardTraction": {
      "$type": "VectoAuxiliaries.Electrics.ResultCard, BusAuxiliaries",
      "Results": []
    },
    "ResultCardOverrun": {
      "$type": "VectoAuxiliaries.Electrics.ResultCard, BusAuxiliaries",
      "Results": []
    },
    "SmartElectrical": false
  },
  "PneumaticUserInputsConfig": {
    "$type": "VectoAuxiliaries.Pneumatics.PneumaticUserInputsConfig, BusAuxiliaries",
    "CompressorMap": "DEFAULT_2-Cylinder_1-Stage_650ccm.ACMP",
    "CompressorGearRatio": 1.0,
    "CompressorGearEfficiency": 0.8,
    "AdBlueDosing": "Pneumatic",
    "AirSuspensionControl": "Electrically",
    "Doors": "Pneumatic",
    "KneelingHeightMillimeters": 80.0,
    "ActuationsMap": "testPneumaticActuationsMap.APAC",
    "RetarderBrake": true,
    "SmartAirCompression": true,
    "SmartRegeneration": true
  },
  "PneumaticAuxillariesConfig": {
    "$type": "VectoAuxiliaries.Pneumatics.PneumaticsAuxilliariesConfig, BusAuxiliaries",
    "AdBlueNIperMinute": 21.25,
    "AirControlledSuspensionNIperMinute": 15.0,
    "BrakingNoRetarderNIperKG": 0.00081,
    "BrakingWithRetarderNIperKG": 0.0006,
    "BreakingPerKneelingNIperKGinMM": 6.6E-05,
    "DeadVolBlowOutsPerLitresperHour": 24.0,
    "DeadVolumeLitres": 30.0,
    "NonSmartRegenFractionTotalAirDemand": 0.26,
    "OverrunUtilisationForCompressionFraction": 0.97,
    "PerDoorOpeningNI": 12.7,
    "PerStopBrakeActuationNIperKG": 0.00064,
    "SmartRegenFractionTotalAirDemand": 0.12
  },
  "HvacUserInputsConfig": {
    "$type": "VectoAuxiliaries.Hvac.HVACUserInputsConfig, BusAuxiliaries",
    "SSMFilePath": "testHVACssm.AHSM",
    "BusDatabasePath": "BusDatabase.abdb",
    "SSMDisabled": false
  },
  "Signals": {
    "$type": "VectoAuxiliaries.Signals, AdvancedAuxiliaryInterfaces",
    "ClutchEngaged": false,
    "EngineDrivelinePower": 0.0,
    "EngineDrivelineTorque": 0.0,
    "EngineMotoringPower": 0.0,
    "EngineSpeed": 2000,
    "SmartElectrics": false,
    "SmartPneumatics": false,
    "TotalCycleTimeSeconds": 3114,
    "CurrentCycleTimeInSeconds": 0,
    "PreExistingAuxPower": 0.0,
    "Idle": false,
    "InNeutral": false,
    "AuxiliaryEventReportingLevel": 0,
    "EngineStopped": false,
    "DeclarationMode": false,
    "WHTC": 1.0,
    "EngineIdleSpeed": 0.0
  }
}
~~~

