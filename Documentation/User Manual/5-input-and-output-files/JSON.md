## JSON
Configuration and component files in Vecto use [JSON](http://en.wikipedia.org/wiki/JSON) ![](pics/external-icon%2012x12.png) as common file format.

Following files use JSON:

* [Job](#job-file)
* [Vehicle](#vehicle-file-.vveh)
* [Engine](#engine-file-.veng)
* [Gearbox](#gearbox-file-.vgbx)
