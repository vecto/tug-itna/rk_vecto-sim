## XML Declaration Report

<div class="declaration">
In Declaration Mode VECTO generates two reports according to the Technical Annex for vehicle certification:

* Manufacturer Report
* Customer Information Report

Both reports are in XML format and contain a description of the simulated vehicle and the simulation results. The format is described in the following resources:

* [Manufacturer Report XML schema](../XML/XSD/VectoOutputManufacturer.xsd)
* [Manufacturer Report XML schema diagram](VectoOutputDeclaration.pdf)
* [Customer Report XML schema](../XML/XSD/VectoOutputCustomer.xsd)
* [Customer Report XML schema diagram](VectoOutputDeclarationCustomer.pdf)

Sample reports are distributed with the generic vehicles.

**Note: ** For better readability and improved presentation, the XML has attached a stylesheet that allows nice rendering in web-browsers. If you open an XML report in your browser, you may be asked the credentials for the CITnet SVN server (same credentials as you need for downloading VECTO) as the CSS is hosted on CITnet.

</div>
