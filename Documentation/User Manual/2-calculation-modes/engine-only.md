## Engine-Only Mode

When this mode is enabled in the Job File then VECTO only calculates the fuel consumption based on a load cycle (engine speed and torque). In the [Job File](#job-file) only the following parameters are needed:

-   Filepath to the Engine File (.veng)
-   Driving Cycles including engine torque (or power) and engine speed

The driving cycle also has to be in a special format which is described here: [Engine Only Driving Cycle](#engine-only-mode-engine-only-driving-cycle).

