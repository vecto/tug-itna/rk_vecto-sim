﻿
Namespace Electrics


Public Interface IM0_5_SmartAlternatorSetEfficiency


readonly property SmartIdleCurrent() As single
readonly property AlternatorsEfficiencyIdleResultCard( ) As single
readonly property SmartTractionCurrent As Single
readonly property AlternatorsEfficiencyTractionOnResultCard() As Single
readonly property SmartOverrunCurrent As Single
readonly property AlternatorsEfficiencyOverrunResultCard() As single



End Interface



End Namespace



