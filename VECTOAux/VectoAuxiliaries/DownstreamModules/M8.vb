﻿' Copyright 2017 European Union.
' Licensed under the EUPL (the 'Licence');
'
' * You may not use this work except in compliance with the Licence.
' * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl
' * Unless required by applicable law or agreed to in writing,
'   software distributed under the Licence is distributed on an "AS IS" basis,
'   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'
' See the LICENSE.txt for the specific language governing permissions and limitations.


Imports TUGraz.VectoCommon.Utils
Imports VectoAuxiliaries.Electrics
Imports VectoAuxiliaries.Pneumatics
Imports VectoAuxiliaries.Hvac

Namespace DownstreamModules
	Public Class M8
		Implements IM8

#Region "Private Fields"

		Private _m1 As IM1_AverageHVACLoadDemand
		Private _m6 As IM6
		Private _m7 As IM7
		Private _signals As ISignals

#End Region

#Region "Internal Sums and Switches"

		'Internal Staged Sums and Switches
		Private ReadOnly Property Sum1 As Watt
			Get
				Return _
					_m7.SmartElectricalAndPneumaticAuxAltPowerGenAtCrank() + _m7.SmartElectricalAndPneumaticAuxAirCompPowerGenAtCrank()
			End Get
		End Property

		Private ReadOnly Property Sum2 As Watt
			Get
				Return _m7.SmartElectricalOnlyAuxAltPowerGenAtCrank() + _m6.AveragePowerDemandAtCrankFromPneumatics
			End Get
		End Property

		Private ReadOnly Property Sum3 As Watt
			Get
				Return _m7.SmartPneumaticOnlyAuxAirCompPowerGenAtCrank + _m6.AvgPowerDemandAtCrankFromElectricsIncHVAC
			End Get
		End Property

		Private ReadOnly Property Sum4 As Watt
			Get
				Return _m6.AvgPowerDemandAtCrankFromElectricsIncHVAC + _m6.AveragePowerDemandAtCrankFromPneumatics
			End Get
		End Property

		Private ReadOnly Property Sum5 As Watt
			Get
				Return _m1.AveragePowerDemandAtCrankFromHVACMechanicalsWatts + SW5
			End Get
		End Property

		Private ReadOnly Property Sum6 As Watt
			Get
				Return If(SW6, Sum5, SIBase(Of Watt).Create(0))
			End Get
		End Property

		Private ReadOnly Property SW1 As Watt
			Get
				Return If(_signals.SmartPneumatics, Sum1, Sum2)
			End Get
		End Property

		Private ReadOnly Property SW2 As Watt
			Get
				Return If(_signals.SmartPneumatics, Sum3, Sum4)
			End Get
		End Property

		Private ReadOnly Property SW3 As Watt
			Get
				Return _
					If _
						(_signals.SmartPneumatics, _m7.SmartElectricalAndPneumaticAuxAltPowerGenAtCrank(),
						_m7.SmartElectricalOnlyAuxAltPowerGenAtCrank())
			End Get
		End Property

		Private ReadOnly Property SW4 As Boolean
			Get
				Return If(_signals.SmartElectrics, _m6.SmartElecAndPneumaticsCompressorFlag, _m6.SmartPneumaticsOnlyCompressorFlag)
			End Get
		End Property

		Private ReadOnly Property SW5 As Watt
			Get
				Return If(_signals.SmartElectrics, SW1, SW2)
			End Get
		End Property

		Private ReadOnly Property SW6 As Boolean
			Get
				Return Not _signals.EngineStopped
			End Get
		End Property

#End Region

		'OUT1
		Public ReadOnly Property AuxPowerAtCrankFromElectricalHVACAndPneumaticsAncillaries As Watt _
			Implements IM8.AuxPowerAtCrankFromElectricalHVACAndPneumaticsAncillaries
			Get
				Return Sum6
			End Get
		End Property
		'OUT2
		Public ReadOnly Property SmartElectricalAlternatorPowerGenAtCrank As Watt _
			Implements IM8.SmartElectricalAlternatorPowerGenAtCrank
			Get
				Return SW3
			End Get
		End Property
		'OUT3
		Public ReadOnly Property CompressorFlag As Boolean Implements IM8.CompressorFlag
			Get
				Return SW4
			End Get
		End Property

		'Constructor
		Public Sub New(m1 As IM1_AverageHVACLoadDemand, m6 As IM6, m7 As IM7, signals As ISignals)

			_m1 = m1
			_m6 = m6
			_m7 = m7
			_signals = signals
		End Sub
	End Class
End Namespace


