﻿' Copyright 2017 European Union.
' Licensed under the EUPL (the 'Licence');
'
' * You may not use this work except in compliance with the Licence.
' * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl
' * Unless required by applicable law or agreed to in writing,
'   software distributed under the Licence is distributed on an "AS IS" basis,
'   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'
' See the LICENSE.txt for the specific language governing permissions and limitations.
Imports System.Globalization
Imports System.IO
Imports System.Text
Imports TUGraz.VectoCommon.Utils

Namespace Electrics
	Public Class AlternatorMap
		Implements IAlternatorMap

		Private ReadOnly filePath As String

		Private _map As New List(Of MapPoint)
		Private _yRange As List(Of Double)
		Private _xRange As List(Of Double)
		Private _minX, _minY, _maxX, _maxY As Double

		'Required Action Test or Interpolation Type
		Public Function OnBoundaryYInterpolatedX(x As Double, y As Double) As Boolean
			Return _yRange.Contains(y) AndAlso Not _xRange.Contains(x)
		End Function

		Public Function OnBoundaryXInterpolatedY(x As Double, y As Double) As Boolean
			Return Not _yRange.Contains(y) AndAlso _xRange.Contains(x)
		End Function

		Public Function ONBoundaryXY(x As Double, y As Double) As Boolean
			Return (From sector In _map Where sector.Y = y AndAlso sector.x = x).Count = 1
		End Function

		'Determine Value Methods
		Private Function GetOnBoundaryXY(x As Double, y As Double) As Double
			Return (From sector In _map Where sector.Y = y AndAlso sector.x = x).First().v
		End Function

		Private Function GetOnBoundaryYInterpolatedX(x As Double, y As Double) As Double

			Dim x0, x1, v0, v1, slope, dx As Double

			x0 = (From p In _xRange Order By p Where p < x).Last()
			x1 = (From p In _xRange Order By p Where p > x).First()
			dx = x1 - x0

			v0 = GetOnBoundaryXY(x0, y)
			v1 = GetOnBoundaryXY(x1, y)

			slope = (v1 - v0) / (x1 - x0)

			Return v0 + ((x - x0) * slope)
		End Function

		Private Function GetOnBoundaryXInterpolatedY(x As Double, y As Double) As Double

			Dim y0, y1, v0, v1, dy, v, slope As Double

			y0 = (From p In _yRange Order By p Where p < y).Last()
			y1 = (From p In _yRange Order By p Where p > y).First()
			dy = y1 - y0

			v0 = GetOnBoundaryXY(x, y0)
			v1 = GetOnBoundaryXY(x, y1)

			slope = (v1 - v0) / (y1 - y0)

			v = v0 + ((y - y0) * slope)

			Return v
		End Function

		Private Function GetBiLinearInterpolatedValue(x As Double, y As Double) As Double

			Dim q11, q12, q21, q22, x1, x2, y1, y2, r1, r2, p As Double

			y1 = (From mapSector As MapPoint In _map Where mapSector.Y < y).Last().Y
			y2 = (From mapSector As MapPoint In _map Where mapSector.Y > y).First().Y

			x1 = (From mapSector As MapPoint In _map Where mapSector.x < x).Last().x
			x2 = (From mapSector As MapPoint In _map Where mapSector.x > x).First().x

			q11 = GetOnBoundaryXY(x1, y1)
			q12 = GetOnBoundaryXY(x1, y2)

			q21 = GetOnBoundaryXY(x2, y1)
			q22 = GetOnBoundaryXY(x2, y2)

			r1 = ((x2 - x) / (x2 - x1)) * q11 + ((x - x1) / (x2 - x1)) * q21

			r2 = ((x2 - x) / (x2 - x1)) * q12 + ((x - x1) / (x2 - x1)) * q22


			p = ((y2 - y) / (y2 - y1)) * r1 + ((y - y1) / (y2 - y1)) * r2


			Return p
		End Function

		'Utilities
		Private Sub fillMapWithDefaults()


			_map.Add(New MapPoint(10, 1500, 0.615))
			_map.Add(New MapPoint(27, 1500, 0.7))
			_map.Add(New MapPoint(53, 1500, 0.1947))
			_map.Add(New MapPoint(63, 1500, 0.0))
			_map.Add(New MapPoint(68, 1500, 0.0))
			_map.Add(New MapPoint(125, 1500, 0.0))
			_map.Add(New MapPoint(136, 1500, 0.0))
			_map.Add(New MapPoint(10, 2000, 0.62))
			_map.Add(New MapPoint(27, 2000, 0.7))
			_map.Add(New MapPoint(53, 2000, 0.3))
			_map.Add(New MapPoint(63, 2000, 0.1462))
			_map.Add(New MapPoint(68, 2000, 0.692))
			_map.Add(New MapPoint(125, 2000, 0.0))
			_map.Add(New MapPoint(136, 2000, 0.0))
			_map.Add(New MapPoint(10, 4000, 0.64))
			_map.Add(New MapPoint(27, 4000, 0.6721))
			_map.Add(New MapPoint(53, 4000, 0.7211))
			_map.Add(New MapPoint(63, 4000, 0.74))
			_map.Add(New MapPoint(68, 4000, 0.7352))
			_map.Add(New MapPoint(125, 4000, 0.68))
			_map.Add(New MapPoint(136, 4000, 0.6694))
			_map.Add(New MapPoint(10, 6000, 0.53))
			_map.Add(New MapPoint(27, 6000, 0.5798))
			_map.Add(New MapPoint(53, 6000, 0.656))
			_map.Add(New MapPoint(63, 6000, 0.6853))
			_map.Add(New MapPoint(68, 6000, 0.7))
			_map.Add(New MapPoint(125, 6000, 0.6329))
			_map.Add(New MapPoint(136, 6000, 0.62))
			_map.Add(New MapPoint(10, 7000, 0.475))
			_map.Add(New MapPoint(27, 7000, 0.5337))
			_map.Add(New MapPoint(53, 7000, 0.6235))
			_map.Add(New MapPoint(63, 7000, 0.658))
			_map.Add(New MapPoint(68, 7000, 0.6824))
			_map.Add(New MapPoint(125, 7000, 0.6094))
			_map.Add(New MapPoint(136, 7000, 0.5953))
		End Sub

		Private Sub getMapRanges()

			_yRange = (From coords As MapPoint In _map Order By coords.Y Select coords.Y Distinct).ToList()
			_xRange = (From coords As MapPoint In _map Order By coords.x Select coords.x Distinct).ToList()

			_minX = _xRange.First
			_maxX = _xRange.Last
			_minY = _yRange.First
			_maxY = _yRange.Last
		End Sub

		'Single entry point to determine Value on map
		Public Function GetValue(x As Double, y As Double) As Double


			If x < _minX OrElse x > _maxX OrElse y < _minY OrElse y > _maxY Then

				'OnAuxiliaryEvent(String.Format("Alternator Map Limiting : RPM{0}, AMPS{1}",x,y),AdvancedAuxiliaryMessageType.Warning)


				'Limiting
				If x < _minX Then x = _minX
				If x > _maxX Then x = _maxX
				If y < _minY Then y = _minY
				If y > _maxY Then y = _maxY

			End If


			'Satisfies both data points - non interpolated value
			If ONBoundaryXY(x, y) Then Return GetOnBoundaryXY(x, y)

			'Satisfies only x or y - single interpolation value
			If OnBoundaryXInterpolatedY(x, y) Then Return GetOnBoundaryXInterpolatedY(x, y)
			If OnBoundaryYInterpolatedX(x, y) Then Return GetOnBoundaryYInterpolatedX(x, y)

			'satisfies no data points - Bi-Linear interpolation
			Return GetBiLinearInterpolatedValue(x, y)
		End Function

		Public Function ReturnDefaultMapValueTests() As String

			Dim sb As StringBuilder = New StringBuilder()
			Dim x, y As Single

			'All Sector Values
			sb.AppendLine("All Values From Map")
			sb.AppendLine("-------------------")
			For Each x In _xRange

				For Each y In _yRange
					sb.AppendLine(String.Format("X:{0}, Y:{1}, V:{2}", x, y, GetValue(x, y)))
				Next

			Next

			sb.AppendLine("")
			sb.AppendLine("Four Corners with interpolated other")
			sb.AppendLine("-------------------")
			x = 1500
			y = 18.5
			sb.AppendLine(String.Format("X:{0}, Y:{1}, V:{2}", x, y, GetValue(x, y)))
			x = 7000
			y = 96.5
			sb.AppendLine(String.Format("X:{0}, Y:{1}, V:{2}", x, y, GetValue(x, y)))
			x = 1750
			y = 10
			sb.AppendLine(String.Format("X:{0}, Y:{1}, V:{2}", x, y, GetValue(x, y)))
			x = 6500
			y = 10
			sb.AppendLine(String.Format("X:{0}, Y:{1}, V:{2}", x, y, GetValue(x, y)))

			sb.AppendLine("")
			sb.AppendLine("Interpolated both")
			sb.AppendLine("-------------------")

			Dim mx, my As Double
			Dim x2, y2 As Integer
			For x2 = 0 To _xRange.Count - 2

				For y2 = 0 To _yRange.Count - 2

					mx = _xRange(x2) + (_xRange(x2 + 1) - _xRange(x2)) / 2
					my = _yRange(y2) + (_yRange(y2 + 1) - _yRange(y2)) / 2

					sb.AppendLine(String.Format("X:{0}, Y:{1}, V:{2}", mx, my, GetValue(mx, my)))


				Next

			Next

			sb.AppendLine("")
			sb.AppendLine("MIKE -> 40 & 1000")
			sb.AppendLine("-------------------")
			x = 1000
			y = 40
			sb.AppendLine(String.Format("X:{0}, Y:{1}, V:{2}", x, y, GetValue(x, y)))


			Return sb.ToString()
		End Function

		'Constructors
		Public Sub New(filepath As String)

			Me.filePath = filepath

			Initialise()

			getMapRanges()
		End Sub

		Private Class MapPoint
			Public Y As Double
			Public x As Double
			Public v As Double

			Public Sub New(y As Double, x As Double, v As Double)

				Me.Y = y
				Me.x = x
				Me.v = v
			End Sub
		End Class

		'Get Alternator Efficiency
		Public Function GetEfficiency(rpm As Double, amps As Ampere) As AlternatorMapValues _
			Implements IAlternatorMap.GetEfficiency

			Return New AlternatorMapValues(GetValue(rpm, amps.Value()))
		End Function

		'Initialises the map.
		Public Function Initialise() As Boolean Implements IAlternatorMap.Initialise
			If File.Exists(filePath) Then
				Using sr As StreamReader = New StreamReader(filePath)
					'get array og lines fron csv
					Dim lines() As String = sr.ReadToEnd().Split(CType(Environment.NewLine, Char()),
																StringSplitOptions.RemoveEmptyEntries)

					'Must have at least 2 entries in map to make it usable [dont forget the header row]
					If (lines.Count() < 3) Then
						Throw New ArgumentException("Insufficient rows in csv to build a usable map")
					End If

					_map = New List(Of MapPoint)
					Dim firstline As Boolean = True

					For Each line As String In lines
						If Not firstline Then

							'Advanced Alternator Source Check.
							If line.Contains("[MODELSOURCE") Then Exit For

							'split the line
							Dim elements() As String = line.Split(New Char() {","c}, StringSplitOptions.RemoveEmptyEntries)
							'3 entries per line required
							If (elements.Length <> 3) Then
								Throw New ArgumentException("Incorrect number of values in csv file")
							End If
							'add values to map

							'Create AlternatorKey
							Dim newPoint As MapPoint = New MapPoint(Single.Parse(elements(0), CultureInfo.InvariantCulture),
																	Single.Parse(elements(1), CultureInfo.InvariantCulture),
																	Single.Parse(elements(2), CultureInfo.InvariantCulture))
							_map.Add(newPoint)

						Else
							firstline = False
						End If
					Next line
				End Using
				Return True
			Else
				Throw New ArgumentException("Supplied input file does not exist")
			End If
		End Function


		'Public Events
		Public Event AuxiliaryEvent(ByRef sender As Object, message As String, messageType As AdvancedAuxiliaryMessageType) _
			Implements IAuxiliaryEvent.AuxiliaryEvent

		Protected Sub OnAuxiliaryEvent(message As String, messageType As AdvancedAuxiliaryMessageType)

			RaiseEvent AuxiliaryEvent(Me, message, messageType)
		End Sub
	End Class
End Namespace


