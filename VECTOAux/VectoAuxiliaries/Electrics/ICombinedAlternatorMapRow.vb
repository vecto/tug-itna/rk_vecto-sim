﻿
Namespace Electrics
'Reflects stored data in pesisted CombinedAlternator Map .AALT
	Public Interface ICombinedAlternatorMapRow
		Property AlternatorName As String
		Property RPM As Single
		Property Amps As Single
		Property Efficiency As Single
		Property PulleyRatio As Single
	End Interface
End Namespace

