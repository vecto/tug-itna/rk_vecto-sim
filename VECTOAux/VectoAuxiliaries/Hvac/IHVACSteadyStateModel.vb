﻿' Copyright 2017 European Union.
' Licensed under the EUPL (the 'Licence');
'
' * You may not use this work except in compliance with the Licence.
' * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl
' * Unless required by applicable law or agreed to in writing,
'   software distributed under the Licence is distributed on an "AS IS" basis,
'   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'
' See the LICENSE.txt for the specific language governing permissions and limitations.

Namespace Hvac
	Public Interface IHVACSteadyStateModel
		''' <summary>
		''' Initialised Values From Map
		''' </summary>
		''' <param name="filePath"></param>
		''' <param name="message"></param>
		''' <returns>True if successfull, and False if not.</returns>
		''' <remarks></remarks>
		Function SetValuesFromMap(byval filePath As String, ByRef message As string) As Boolean

		''' <summary>
		''' HVAC Mechanical Load Power  (W)
		''' </summary>
		''' <value></value>
		''' <returns></returns>
		''' <remarks></remarks>
		Property HVACMechanicalLoadPowerWatts As Single

		''' <summary>
		''' HVAC Electrical Load Power (W)
		''' </summary>
		''' <value></value>
		''' <returns></returns>
		''' <remarks></remarks>
		Property HVACElectricalLoadPowerWatts As Single

		''' <summary>
		''' HVAC Fuelling (L/H)
		''' </summary>
		''' <value></value>
		''' <returns>Liters per hour</returns>
		''' <remarks></remarks>
		Property HVACFuellingLitresPerHour As single
	End Interface
End Namespace


