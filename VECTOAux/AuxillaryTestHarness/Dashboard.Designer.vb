﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Dashboard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.pnlMain = New System.Windows.Forms.Panel()
        Me.btnLoad = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.tabMain = New System.Windows.Forms.TabControl()
        Me.tabGeneralConfig = New System.Windows.Forms.TabPage()
        Me.btnFuelMap = New System.Windows.Forms.Button()
        Me.lblFuelMap = New System.Windows.Forms.Label()
        Me.txtFuelMap = New System.Windows.Forms.TextBox()
        Me.cboCycle = New System.Windows.Forms.ComboBox()
        Me.lblCycle = New System.Windows.Forms.Label()
        Me.lblVehiceWeight = New System.Windows.Forms.Label()
        Me.txtVehicleWeightKG = New System.Windows.Forms.TextBox()
        Me.tabElectricalConfig = New System.Windows.Forms.TabPage()
        Me.btnAlternatorMapPath = New System.Windows.Forms.Button()
        Me.gvResultsCardOverrun = New System.Windows.Forms.DataGridView()
        Me.gvResultsCardTraction = New System.Windows.Forms.DataGridView()
        Me.gvResultsCardIdle = New System.Windows.Forms.DataGridView()
        Me.lblResultsOverrun = New System.Windows.Forms.Label()
        Me.lblResultsTractionOn = New System.Windows.Forms.Label()
        Me.lblResultsIdle = New System.Windows.Forms.Label()
        Me.chkSmartElectricals = New System.Windows.Forms.CheckBox()
        Me.lblElectricalConsumables = New System.Windows.Forms.Label()
        Me.gvElectricalConsumables = New System.Windows.Forms.DataGridView()
        Me.txtDoorActuationTimeSeconds = New System.Windows.Forms.TextBox()
        Me.txtAlternatorGearEfficiency = New System.Windows.Forms.TextBox()
        Me.txtAlternatorMapPath = New System.Windows.Forms.TextBox()
        Me.txtPowernetVoltage = New System.Windows.Forms.TextBox()
        Me.lblDoorActuationTimeSeconds = New System.Windows.Forms.Label()
        Me.lblAlternatorGearEfficiency = New System.Windows.Forms.Label()
        Me.lblAlternatormapPath = New System.Windows.Forms.Label()
        Me.lblPowerNetVoltage = New System.Windows.Forms.Label()
        Me.tabPneumaticConfig = New System.Windows.Forms.TabPage()
        Me.pnlPneumaticsUserInput = New System.Windows.Forms.Panel()
        Me.btnActuationsMap = New System.Windows.Forms.Button()
        Me.btnCompressorMap = New System.Windows.Forms.Button()
        Me.lblPneumaticsVariablesTitle = New System.Windows.Forms.Label()
        Me.lblActuationsMap = New System.Windows.Forms.Label()
        Me.chkSmartAirCompression = New System.Windows.Forms.CheckBox()
        Me.chkSmartRegeneration = New System.Windows.Forms.CheckBox()
        Me.lblAdBlueDosing = New System.Windows.Forms.Label()
        Me.chkRetarderBrake = New System.Windows.Forms.CheckBox()
        Me.txtKneelingHeightMillimeters = New System.Windows.Forms.TextBox()
        Me.lblAirSuspensionControl = New System.Windows.Forms.Label()
        Me.cboDoors = New System.Windows.Forms.ComboBox()
        Me.txtCompressorMap = New System.Windows.Forms.TextBox()
        Me.lblCompressorGearEfficiency = New System.Windows.Forms.Label()
        Me.txtCompressorGearRatio = New System.Windows.Forms.TextBox()
        Me.lblCompressorGearRatio = New System.Windows.Forms.Label()
        Me.txtCompressorGearEfficiency = New System.Windows.Forms.TextBox()
        Me.lblCompressorMap = New System.Windows.Forms.Label()
        Me.cboAirSuspensionControl = New System.Windows.Forms.ComboBox()
        Me.lblDoors = New System.Windows.Forms.Label()
        Me.cboAdBlueDosing = New System.Windows.Forms.ComboBox()
        Me.lblKneelingHeightMillimeters = New System.Windows.Forms.Label()
        Me.txtActuationsMap = New System.Windows.Forms.TextBox()
        Me.pnlPneumaticAuxillaries = New System.Windows.Forms.Panel()
        Me.lblPneumaticAuxillariesTitle = New System.Windows.Forms.Label()
        Me.lblAdBlueNIperMinute = New System.Windows.Forms.Label()
        Me.lblAirControlledSuspensionNIperMinute = New System.Windows.Forms.Label()
        Me.lblBrakingNoRetarderNIperKG = New System.Windows.Forms.Label()
        Me.lblBrakingWithRetarderNIperKG = New System.Windows.Forms.Label()
        Me.lblBreakingPerKneelingNIperKGinMM = New System.Windows.Forms.Label()
        Me.lblDeadVolBlowOutsPerLitresperHour = New System.Windows.Forms.Label()
        Me.lblDeadVolumeLitres = New System.Windows.Forms.Label()
        Me.lblNonSmartRegenFractionTotalAirDemand = New System.Windows.Forms.Label()
        Me.lblOverrunUtilisationForCompressionFraction = New System.Windows.Forms.Label()
        Me.lblPerDoorOpeningNI = New System.Windows.Forms.Label()
        Me.lblPerStopBrakeActuationNIperKG = New System.Windows.Forms.Label()
        Me.lblSmartRegenFractionTotalAirDemand = New System.Windows.Forms.Label()
        Me.txtAdBlueNIperMinute = New System.Windows.Forms.TextBox()
        Me.txtAirControlledSuspensionNIperMinute = New System.Windows.Forms.TextBox()
        Me.txtBrakingNoRetarderNIperKG = New System.Windows.Forms.TextBox()
        Me.txtBrakingWithRetarderNIperKG = New System.Windows.Forms.TextBox()
        Me.txtBreakingPerKneelingNIperKGinMM = New System.Windows.Forms.TextBox()
        Me.txtDeadVolBlowOutsPerLitresperHour = New System.Windows.Forms.TextBox()
        Me.txtDeadVolumeLitres = New System.Windows.Forms.TextBox()
        Me.txtNonSmartRegenFractionTotalAirDemand = New System.Windows.Forms.TextBox()
        Me.txtOverrunUtilisationForCompressionFraction = New System.Windows.Forms.TextBox()
        Me.txtPerDoorOpeningNI = New System.Windows.Forms.TextBox()
        Me.txtPerStopBrakeActuationNIperKG = New System.Windows.Forms.TextBox()
        Me.txtSmartRegenFractionTotalAirDemand = New System.Windows.Forms.TextBox()
        Me.tabHVACConfig = New System.Windows.Forms.TabPage()
        Me.btnSSMBSource = New System.Windows.Forms.Button()
        Me.lblSSMFilePath = New System.Windows.Forms.Label()
        Me.txtSSMFilePath = New System.Windows.Forms.TextBox()
        Me.lblHVACTitle = New System.Windows.Forms.Label()
        Me.txtHVACFuellingLitresPerHour = New System.Windows.Forms.TextBox()
        Me.lblHVACFuellingLitresPerHour = New System.Windows.Forms.Label()
        Me.txtHVACMechanicalLoadPowerWatts = New System.Windows.Forms.TextBox()
        Me.lblHVACMechanicalLoadPowerWatts = New System.Windows.Forms.Label()
        Me.txtHVACElectricalLoadPowerWatts = New System.Windows.Forms.TextBox()
        Me.lblHVACElectricalLoadPowerWatts = New System.Windows.Forms.Label()
        Me.tabPlayground = New System.Windows.Forms.TabPage()
        Me.chkEngineStopped = New System.Windows.Forms.CheckBox()
        Me.pnlM13 = New System.Windows.Forms.Panel()
        Me.txtM13_out_TotalCycleFuelCalculationGramsLITRES = New System.Windows.Forms.TextBox()
        Me.lblM13TotalFuelConsumptionTotalCycleLitres = New System.Windows.Forms.Label()
        Me.lblM13_FinalFCOverTheCycle = New System.Windows.Forms.Label()
        Me.txtM13_out_TotalCycleFuelCalculationGramsGRAMS = New System.Windows.Forms.TextBox()
        Me.lblM13_Title = New System.Windows.Forms.Label()
        Me.pnlM12 = New System.Windows.Forms.Panel()
        Me.lblM12_FuelConsumptionWithSmartElectricsAndAveragePneumaticPowerDemand = New System.Windows.Forms.Label()
        Me.txtM12_out_FuelConsumptionWithSmartElectricsAndAveragePneumaticPowerDemand = New System.Windows.Forms.TextBox()
        Me.lblM12_Title = New System.Windows.Forms.Label()
        Me.mblM11_Title = New System.Windows.Forms.Label()
        Me.pnlM11 = New System.Windows.Forms.Panel()
        Me.lblM11_TotalCycleFuelConsumptionZeroElectricalLoad = New System.Windows.Forms.Label()
        Me.txtM11_out_TotalCycleFuelConsumptionZeroElectricalLoad = New System.Windows.Forms.TextBox()
        Me.lblM11_TotalCycleFuelConsumptionSmartElectricalLoad = New System.Windows.Forms.Label()
        Me.txtM11_out_TotalCycleFuelConsumptionSmartElectricalLoad = New System.Windows.Forms.TextBox()
        Me.lblM11_TotalCycleElectricalDemand = New System.Windows.Forms.Label()
        Me.txtM11_out_TotalCycleElectricalDemand = New System.Windows.Forms.TextBox()
        Me.lblM11_SmartElectricalTotalCycleElectricalEnergyGenerated = New System.Windows.Forms.Label()
        Me.lblM11_TotalCycleElectricalEnergyGenOverrunOnly = New System.Windows.Forms.Label()
        Me.txtM11_out_SmartElectricalTotalCycleElectricalEnergyGenerated = New System.Windows.Forms.TextBox()
        Me.txtM11_out_TotalCycleElectricalEnergyGenOverrunOnly = New System.Windows.Forms.TextBox()
        Me.chkSignalsSmartAirCompression = New System.Windows.Forms.CheckBox()
        Me.chkSignalsSmartElectrics = New System.Windows.Forms.CheckBox()
        Me.pnlM10 = New System.Windows.Forms.Panel()
        Me.lblM10_FuelConsumptionWithSmartPneumaticsAndAverageElectricalPowerDemand = New System.Windows.Forms.Label()
        Me.lblM10_BaseFuelConsumptionAithAverageAuxLoads = New System.Windows.Forms.Label()
        Me.txtM10_out_FCWithSmartPSAndAvgElecPowerDemand = New System.Windows.Forms.TextBox()
        Me.txtM10_out_BaseFCWithAvgAuxLoads = New System.Windows.Forms.TextBox()
        Me.lblM10_Title = New System.Windows.Forms.Label()
        Me.lblM9Title = New System.Windows.Forms.Label()
        Me.pnlM9 = New System.Windows.Forms.Panel()
        Me.lblM9TotalCycleFuelConsumptionCompressorOFFContinuously = New System.Windows.Forms.Label()
        Me.txtM9_out_TotalCycleFuelConsumptionCompressorOFFContinuously = New System.Windows.Forms.TextBox()
        Me.lblM9TotalCycleFuelConsumptionCompressorOnContinuously = New System.Windows.Forms.Label()
        Me.txtM9_out_TotalCycleFuelConsumptionCompressorOnContinuously = New System.Windows.Forms.TextBox()
        Me.lblM9LitresOfAirCompressoryOnlyOnInOverrun = New System.Windows.Forms.Label()
        Me.lblM9LitresOfAirCompressorOnContinuously = New System.Windows.Forms.Label()
        Me.txtM9_out_LitresOfAirCompressorOnlyOnInOverrun = New System.Windows.Forms.TextBox()
        Me.txtM9_out_LitresOfAirConsumptionCompressorONContinuously = New System.Windows.Forms.TextBox()
        Me.lblM8_Title = New System.Windows.Forms.Label()
        Me.pnlM8 = New System.Windows.Forms.Panel()
        Me.lblM8CompressorFlag = New System.Windows.Forms.Label()
        Me.txtM8_out_CompressorFlag = New System.Windows.Forms.TextBox()
        Me.lblM8SmartElectricalAltPwrGenAtCrank = New System.Windows.Forms.Label()
        Me.lblM8AuxPowerAtCrankFromAllAncillaries = New System.Windows.Forms.Label()
        Me.txtM8_out_SmartElectricalAltPwrGenAtCrank = New System.Windows.Forms.TextBox()
        Me.txtM8_out_AuxPowerAtCrankFromAllAncillaries = New System.Windows.Forms.TextBox()
        Me.lblM7_Title = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.lblM7_SmartPneumaticsOnlyAux_AirCompPwrRegenAtCrank = New System.Windows.Forms.Label()
        Me.lblM7_SmartElecOnly_AltPwrGenAtCrank = New System.Windows.Forms.Label()
        Me.txtM7_out_SmartPneumaticsOnlyAux_AirCompPwrRegenAtCrank = New System.Windows.Forms.TextBox()
        Me.txtM7_out_SmartElecOnlyAux_AltPwrGenAtCrank = New System.Windows.Forms.TextBox()
        Me.lblM7_SmartElectricalAndPneumaticAux_AirCompPowerGenAtCrank = New System.Windows.Forms.Label()
        Me.lblM7_SmarElectricalAndPneumaticsAux_AltPowerGenAtCrank = New System.Windows.Forms.Label()
        Me.txtM7_out_SmartElectricalAndPneumaticAux_AirCompPowerGenAtCrank = New System.Windows.Forms.TextBox()
        Me.txtM7_out_SmartElectricalAndPneumaticsAux_AltPowerGenAtCrank = New System.Windows.Forms.TextBox()
        Me.lblM6Title = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblM6_SmartPneumaticsOnlyCompressorFlag = New System.Windows.Forms.Label()
        Me.txtM6_out_SmartPneumaticsOnlyCompressorFlag = New System.Windows.Forms.TextBox()
        Me.lblM6_AveragePowerDemandAtCrankFromElectricsIncHVAC = New System.Windows.Forms.Label()
        Me.lblM6_SmartPneumaticOnlyAirCompPowerGenAtCrank = New System.Windows.Forms.Label()
        Me.txtM6_out_AveragePowerDemandAtCrankFromElectricsIncHVAC = New System.Windows.Forms.TextBox()
        Me.txtM6_out_SmartPneumaticOnlyAirCompPowerGenAtCrank = New System.Windows.Forms.TextBox()
        Me.lblM6_AveragePowerDemandAtCrankFromPneumatics = New System.Windows.Forms.Label()
        Me.lblM6_SmarElectricalOnlyAltPowerGenAtCrank = New System.Windows.Forms.Label()
        Me.txtM6_out_AveragePowerDemandAtCrankFromPneumatics = New System.Windows.Forms.TextBox()
        Me.txtM6_out_SmarElectricalOnlyAltPowerGenAtCrank = New System.Windows.Forms.TextBox()
        Me.lblM6_SmartElectricalAndPneumaticAirCompPowerGenAtCrank = New System.Windows.Forms.Label()
        Me.lblM6_SmartElectriclAndPneumaticsAltPowerGenAtCrank = New System.Windows.Forms.Label()
        Me.txtM6_out_SmartElectricalAndPneumaticAirCompPowerGenAtCrank = New System.Windows.Forms.TextBox()
        Me.txtM6_out_SmartElectriclAndPneumaticsAltPowerGenAtCrank = New System.Windows.Forms.TextBox()
        Me.lblM6_SmartElectricalAndPneumaticsCompressorFlag = New System.Windows.Forms.Label()
        Me.lblM6_OverrunFlag = New System.Windows.Forms.Label()
        Me.txtM6_out_SmartElectricalAndPneumaticsCompressorFlag = New System.Windows.Forms.TextBox()
        Me.txtM6_out_OverrunFlag = New System.Windows.Forms.TextBox()
        Me.lblPreExistingAuxPower = New System.Windows.Forms.Label()
        Me.txtPreExistingAuxPower = New System.Windows.Forms.TextBox()
        Me.chkIdle = New System.Windows.Forms.CheckBox()
        Me.chkInNeutral = New System.Windows.Forms.CheckBox()
        Me.lblM5_SmartAltSetGeneration = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lblM5_AlternatorRegenPowerAtCrankTractionWatts = New System.Windows.Forms.Label()
        Me.M5_AlternatorGenerationPowerAtCrankOverrunWatts = New System.Windows.Forms.Label()
        Me.txtM5_out_AltRegenPowerAtCrankOverrunWatts = New System.Windows.Forms.TextBox()
        Me.lblM5_AltRegenPowerAtCrankIdleW = New System.Windows.Forms.Label()
        Me.txtM5_out_AltRegenPowerAtCrankTractionWatts = New System.Windows.Forms.TextBox()
        Me.txtM5_out_AltRegenPowerAtCrankIdleWatts = New System.Windows.Forms.TextBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.lblM4_AirCompressor = New System.Windows.Forms.Label()
        Me.btnFinish = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblM4_PowerAtCrankFromPSCompressorON = New System.Windows.Forms.Label()
        Me.lblM4_PowerAtCrankFromPSCompressorOFF = New System.Windows.Forms.Label()
        Me.lblM4_CompressorOnOffPowerDelta = New System.Windows.Forms.Label()
        Me.lblM4_CompressorFlowRate = New System.Windows.Forms.Label()
        Me.txtM4_out_PowerAtCrankFromPneumaticsCompressorON = New System.Windows.Forms.TextBox()
        Me.txtM4_out_PowerAtCrankFromPneumaticsCompressorOFF = New System.Windows.Forms.TextBox()
        Me.txtM4_out_CompresssorPwrOnMinusPwrOff = New System.Windows.Forms.TextBox()
        Me.txtM4_out_CompressorFlowRate = New System.Windows.Forms.TextBox()
        Me.lblM3_AveragePneumaticLoad = New System.Windows.Forms.Label()
        Me.pnl_M3_Displays = New System.Windows.Forms.Panel()
        Me.lblM3_TotAirConsumptionPerCycleLitres = New System.Windows.Forms.Label()
        Me.lbl_M3_AvgPowerAtCrankFromPneumatics = New System.Windows.Forms.Label()
        Me.txtM3_out_TotalAirConsumedPerCycleInLitres = New System.Windows.Forms.TextBox()
        Me.txtM3_out_AveragePowerAtCrankFromPneumatics = New System.Windows.Forms.TextBox()
        Me.btnStart = New System.Windows.Forms.Button()
        Me.lblM2AverageElectricalLoadTitle = New System.Windows.Forms.Label()
        Me.pnl_M2_Displays = New System.Windows.Forms.Panel()
        Me.lblM2_AvgPwrDmdAtCrankFromElectrics = New System.Windows.Forms.Label()
        Me.lblM2_AveragePowerDemandAtAlternatorFromElectrics = New System.Windows.Forms.Label()
        Me.txtM2_out_AvgPowerAtCrankFromElectrics = New System.Windows.Forms.TextBox()
        Me.txtM2_out_AvgPowerAtAltFromElectrics = New System.Windows.Forms.TextBox()
        Me.lblM1_HVACAverageLoad = New System.Windows.Forms.Label()
        Me.pnl_M1_Displays = New System.Windows.Forms.Panel()
        Me.lblM1_HVACFuelling = New System.Windows.Forms.Label()
        Me.lblM1_AvgPowerDemandAtCrankHVACElectrics = New System.Windows.Forms.Label()
        Me.lblM1_AveragePowerDemandAtAlternatorElectrics = New System.Windows.Forms.Label()
        Me.lblM1_AveragePowerDemandAtCrank = New System.Windows.Forms.Label()
        Me.txtM1_out_HVACFuelling = New System.Windows.Forms.TextBox()
        Me.txtM1_out_AvgPwrAtCrankFromHVACElec = New System.Windows.Forms.TextBox()
        Me.txtM1_out_AvgPowerDemandAtAlternatorHvacElectrics = New System.Windows.Forms.TextBox()
        Me.txtM1_out_AvgPowerDemandAtCrankMech = New System.Windows.Forms.TextBox()
        Me.lblM05SmartalternatorSetEfficiency = New System.Windows.Forms.Label()
        Me.pnl_M05_Displays = New System.Windows.Forms.Panel()
        Me.lblM05_AlternatorsEfficiencyOverrun = New System.Windows.Forms.Label()
        Me.lblM05SmartOverrunCurrent = New System.Windows.Forms.Label()
        Me.lblM05_AlternatorsEfficiencyTraction = New System.Windows.Forms.Label()
        Me.lblM05_SmartTractionCurrent = New System.Windows.Forms.Label()
        Me.lblM05AlternatorsEfficiencyIdle = New System.Windows.Forms.Label()
        Me.lblM05_SmartIdleCurrent = New System.Windows.Forms.Label()
        Me.txtM05_out_AlternatorsEfficiencyOverrun = New System.Windows.Forms.TextBox()
        Me.txtM05_out_SmartOverrunCurrent = New System.Windows.Forms.TextBox()
        Me.txtM05_out_AlternatorsEfficiencyTraction = New System.Windows.Forms.TextBox()
        Me.txtM05_out_SmartTractionCurrent = New System.Windows.Forms.TextBox()
        Me.txtM05_Out_AlternatorsEfficiencyIdle = New System.Windows.Forms.TextBox()
        Me.txtM05_OutSmartIdleCurrent = New System.Windows.Forms.TextBox()
        Me.lblM0Outputs = New System.Windows.Forms.Label()
        Me.pnl_M0_Displays = New System.Windows.Forms.Panel()
        Me.lblOutHVACElectricalCurrentDemand = New System.Windows.Forms.Label()
        Me.lblAlternatorsEfficiency = New System.Windows.Forms.Label()
        Me.txtM0_Out_AlternatorsEfficiency = New System.Windows.Forms.TextBox()
        Me.txtM0_Out_HVacElectricalCurrentDemand = New System.Windows.Forms.TextBox()
        Me.chkClutchEngaged = New System.Windows.Forms.CheckBox()
        Me.lblTotalCycleTimeSeconds = New System.Windows.Forms.Label()
        Me.lblEngineSpeed = New System.Windows.Forms.Label()
        Me.lblEngineMotoringPower = New System.Windows.Forms.Label()
        Me.lblEngineDrivelineTorque = New System.Windows.Forms.Label()
        Me.lblEngineDrivelinePower = New System.Windows.Forms.Label()
        Me.txtTotalCycleTimeSeconds = New System.Windows.Forms.TextBox()
        Me.txtEngineSpeed = New System.Windows.Forms.TextBox()
        Me.txtEngineMotoringPower = New System.Windows.Forms.TextBox()
        Me.txtEngineDrivelineTorque = New System.Windows.Forms.TextBox()
        Me.txtEngineDrivelinePower = New System.Windows.Forms.TextBox()
        Me.resultCardContextMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.DeleteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.pnlMain.SuspendLayout
        Me.tabMain.SuspendLayout
        Me.tabGeneralConfig.SuspendLayout
        Me.tabElectricalConfig.SuspendLayout
        CType(Me.gvResultsCardOverrun,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.gvResultsCardTraction,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.gvResultsCardIdle,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.gvElectricalConsumables,System.ComponentModel.ISupportInitialize).BeginInit
        Me.tabPneumaticConfig.SuspendLayout
        Me.pnlPneumaticsUserInput.SuspendLayout
        Me.pnlPneumaticAuxillaries.SuspendLayout
        Me.tabHVACConfig.SuspendLayout
        Me.tabPlayground.SuspendLayout
        Me.pnlM13.SuspendLayout
        Me.pnlM12.SuspendLayout
        Me.pnlM11.SuspendLayout
        Me.pnlM10.SuspendLayout
        Me.pnlM9.SuspendLayout
        Me.pnlM8.SuspendLayout
        Me.Panel4.SuspendLayout
        Me.Panel2.SuspendLayout
        Me.Panel3.SuspendLayout
        CType(Me.PictureBox1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.Panel1.SuspendLayout
        Me.pnl_M3_Displays.SuspendLayout
        Me.pnl_M2_Displays.SuspendLayout
        Me.pnl_M1_Displays.SuspendLayout
        Me.pnl_M05_Displays.SuspendLayout
        Me.pnl_M0_Displays.SuspendLayout
        Me.resultCardContextMenu.SuspendLayout
        CType(Me.ErrorProvider,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.btnLoad)
        Me.pnlMain.Controls.Add(Me.btnCancel)
        Me.pnlMain.Controls.Add(Me.btnSave)
        Me.pnlMain.Controls.Add(Me.tabMain)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(1502, 794)
        Me.pnlMain.TabIndex = 1
        '
        'btnLoad
        '
        Me.btnLoad.Location = New System.Drawing.Point(946, 749)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(78, 25)
        Me.btnLoad.TabIndex = 11
        Me.btnLoad.Text = "Load"
        Me.btnLoad.UseVisualStyleBackColor = true
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(1156, 749)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 5
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = true
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(842, 749)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(85, 25)
        Me.btnSave.TabIndex = 10
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = true
        '
        'tabMain
        '
        Me.tabMain.AccessibleDescription = ""
        Me.tabMain.Controls.Add(Me.tabGeneralConfig)
        Me.tabMain.Controls.Add(Me.tabElectricalConfig)
        Me.tabMain.Controls.Add(Me.tabPneumaticConfig)
        Me.tabMain.Controls.Add(Me.tabHVACConfig)
        Me.tabMain.Controls.Add(Me.tabPlayground)
        Me.tabMain.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed
        Me.tabMain.Location = New System.Drawing.Point(12, 12)
        Me.tabMain.Name = "tabMain"
        Me.tabMain.SelectedIndex = 0
        Me.tabMain.Size = New System.Drawing.Size(1478, 730)
        Me.tabMain.TabIndex = 0
        Me.tabMain.Tag = ""
        '
        'tabGeneralConfig
        '
        Me.tabGeneralConfig.Controls.Add(Me.btnFuelMap)
        Me.tabGeneralConfig.Controls.Add(Me.lblFuelMap)
        Me.tabGeneralConfig.Controls.Add(Me.txtFuelMap)
        Me.tabGeneralConfig.Controls.Add(Me.cboCycle)
        Me.tabGeneralConfig.Controls.Add(Me.lblCycle)
        Me.tabGeneralConfig.Controls.Add(Me.lblVehiceWeight)
        Me.tabGeneralConfig.Controls.Add(Me.txtVehicleWeightKG)
        Me.tabGeneralConfig.Location = New System.Drawing.Point(4, 22)
        Me.tabGeneralConfig.Name = "tabGeneralConfig"
        Me.tabGeneralConfig.Padding = New System.Windows.Forms.Padding(3)
        Me.tabGeneralConfig.Size = New System.Drawing.Size(1470, 704)
        Me.tabGeneralConfig.TabIndex = 0
        Me.tabGeneralConfig.Text = "General"
        Me.tabGeneralConfig.UseVisualStyleBackColor = true
        '
        'btnFuelMap
        '
        Me.btnFuelMap.Location = New System.Drawing.Point(778, 126)
        Me.btnFuelMap.Name = "btnFuelMap"
        Me.btnFuelMap.Size = New System.Drawing.Size(38, 23)
        Me.btnFuelMap.TabIndex = 9
        Me.btnFuelMap.Text = ". . . "
        Me.btnFuelMap.UseVisualStyleBackColor = true
        '
        'lblFuelMap
        '
        Me.lblFuelMap.AutoSize = true
        Me.lblFuelMap.Location = New System.Drawing.Point(31, 131)
        Me.lblFuelMap.Name = "lblFuelMap"
        Me.lblFuelMap.Size = New System.Drawing.Size(51, 13)
        Me.lblFuelMap.TabIndex = 8
        Me.lblFuelMap.Text = "Fuel Map"
        '
        'txtFuelMap
        '
        Me.txtFuelMap.Location = New System.Drawing.Point(123, 127)
        Me.txtFuelMap.Name = "txtFuelMap"
        Me.txtFuelMap.Size = New System.Drawing.Size(649, 20)
        Me.txtFuelMap.TabIndex = 7
        '
        'cboCycle
        '
        Me.cboCycle.FormattingEnabled = true
        Me.cboCycle.Items.AddRange(New Object() {"Urban", "Heavy urban", "Suburban", "Interurban", "Coach"})
        Me.cboCycle.Location = New System.Drawing.Point(123, 78)
        Me.cboCycle.Name = "cboCycle"
        Me.cboCycle.Size = New System.Drawing.Size(121, 21)
        Me.cboCycle.TabIndex = 6
        '
        'lblCycle
        '
        Me.lblCycle.AutoSize = true
        Me.lblCycle.Location = New System.Drawing.Point(31, 82)
        Me.lblCycle.Name = "lblCycle"
        Me.lblCycle.Size = New System.Drawing.Size(33, 13)
        Me.lblCycle.TabIndex = 5
        Me.lblCycle.Text = "Cycle"
        '
        'lblVehiceWeight
        '
        Me.lblVehiceWeight.AutoSize = true
        Me.lblVehiceWeight.Location = New System.Drawing.Point(31, 38)
        Me.lblVehiceWeight.Name = "lblVehiceWeight"
        Me.lblVehiceWeight.Size = New System.Drawing.Size(88, 13)
        Me.lblVehiceWeight.TabIndex = 3
        Me.lblVehiceWeight.Text = "Vehicle Mass KG"
        '
        'txtVehicleWeightKG
        '
        Me.txtVehicleWeightKG.Location = New System.Drawing.Point(123, 34)
        Me.txtVehicleWeightKG.Name = "txtVehicleWeightKG"
        Me.txtVehicleWeightKG.Size = New System.Drawing.Size(100, 20)
        Me.txtVehicleWeightKG.TabIndex = 2
        '
        'tabElectricalConfig
        '
        Me.tabElectricalConfig.Controls.Add(Me.btnAlternatorMapPath)
        Me.tabElectricalConfig.Controls.Add(Me.gvResultsCardOverrun)
        Me.tabElectricalConfig.Controls.Add(Me.gvResultsCardTraction)
        Me.tabElectricalConfig.Controls.Add(Me.gvResultsCardIdle)
        Me.tabElectricalConfig.Controls.Add(Me.lblResultsOverrun)
        Me.tabElectricalConfig.Controls.Add(Me.lblResultsTractionOn)
        Me.tabElectricalConfig.Controls.Add(Me.lblResultsIdle)
        Me.tabElectricalConfig.Controls.Add(Me.chkSmartElectricals)
        Me.tabElectricalConfig.Controls.Add(Me.lblElectricalConsumables)
        Me.tabElectricalConfig.Controls.Add(Me.gvElectricalConsumables)
        Me.tabElectricalConfig.Controls.Add(Me.txtDoorActuationTimeSeconds)
        Me.tabElectricalConfig.Controls.Add(Me.txtAlternatorGearEfficiency)
        Me.tabElectricalConfig.Controls.Add(Me.txtAlternatorMapPath)
        Me.tabElectricalConfig.Controls.Add(Me.txtPowernetVoltage)
        Me.tabElectricalConfig.Controls.Add(Me.lblDoorActuationTimeSeconds)
        Me.tabElectricalConfig.Controls.Add(Me.lblAlternatorGearEfficiency)
        Me.tabElectricalConfig.Controls.Add(Me.lblAlternatormapPath)
        Me.tabElectricalConfig.Controls.Add(Me.lblPowerNetVoltage)
        Me.tabElectricalConfig.Location = New System.Drawing.Point(4, 22)
        Me.tabElectricalConfig.Name = "tabElectricalConfig"
        Me.tabElectricalConfig.Padding = New System.Windows.Forms.Padding(3)
        Me.tabElectricalConfig.Size = New System.Drawing.Size(1470, 704)
        Me.tabElectricalConfig.TabIndex = 1
        Me.tabElectricalConfig.Text = "Electrics"
        Me.tabElectricalConfig.UseVisualStyleBackColor = true
        '
        'btnAlternatorMapPath
        '
        Me.btnAlternatorMapPath.Location = New System.Drawing.Point(497, 43)
        Me.btnAlternatorMapPath.Name = "btnAlternatorMapPath"
        Me.btnAlternatorMapPath.Size = New System.Drawing.Size(38, 20)
        Me.btnAlternatorMapPath.TabIndex = 19
        Me.btnAlternatorMapPath.Text = ". . ."
        Me.btnAlternatorMapPath.UseVisualStyleBackColor = true
        '
        'gvResultsCardOverrun
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gvResultsCardOverrun.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.gvResultsCardOverrun.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gvResultsCardOverrun.DefaultCellStyle = DataGridViewCellStyle2
        Me.gvResultsCardOverrun.Location = New System.Drawing.Point(590, 451)
        Me.gvResultsCardOverrun.Name = "gvResultsCardOverrun"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gvResultsCardOverrun.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.gvResultsCardOverrun.Size = New System.Drawing.Size(246, 125)
        Me.gvResultsCardOverrun.TabIndex = 18
        '
        'gvResultsCardTraction
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gvResultsCardTraction.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.gvResultsCardTraction.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gvResultsCardTraction.DefaultCellStyle = DataGridViewCellStyle5
        Me.gvResultsCardTraction.Location = New System.Drawing.Point(311, 451)
        Me.gvResultsCardTraction.Name = "gvResultsCardTraction"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gvResultsCardTraction.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.gvResultsCardTraction.Size = New System.Drawing.Size(258, 125)
        Me.gvResultsCardTraction.TabIndex = 17
        '
        'gvResultsCardIdle
        '
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gvResultsCardIdle.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.gvResultsCardIdle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gvResultsCardIdle.DefaultCellStyle = DataGridViewCellStyle8
        Me.gvResultsCardIdle.Location = New System.Drawing.Point(35, 451)
        Me.gvResultsCardIdle.Name = "gvResultsCardIdle"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gvResultsCardIdle.RowHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.gvResultsCardIdle.Size = New System.Drawing.Size(256, 125)
        Me.gvResultsCardIdle.TabIndex = 16
        '
        'lblResultsOverrun
        '
        Me.lblResultsOverrun.AutoSize = true
        Me.lblResultsOverrun.Location = New System.Drawing.Point(587, 435)
        Me.lblResultsOverrun.Name = "lblResultsOverrun"
        Me.lblResultsOverrun.Size = New System.Drawing.Size(109, 13)
        Me.lblResultsOverrun.TabIndex = 15
        Me.lblResultsOverrun.Text = "Result Card : Overrun"
        '
        'lblResultsTractionOn
        '
        Me.lblResultsTractionOn.AutoSize = true
        Me.lblResultsTractionOn.Location = New System.Drawing.Point(308, 435)
        Me.lblResultsTractionOn.Name = "lblResultsTractionOn"
        Me.lblResultsTractionOn.Size = New System.Drawing.Size(124, 13)
        Me.lblResultsTractionOn.TabIndex = 14
        Me.lblResultsTractionOn.Text = "Result Card : TractionOn"
        '
        'lblResultsIdle
        '
        Me.lblResultsIdle.AutoSize = true
        Me.lblResultsIdle.Location = New System.Drawing.Point(33, 435)
        Me.lblResultsIdle.Name = "lblResultsIdle"
        Me.lblResultsIdle.Size = New System.Drawing.Size(91, 13)
        Me.lblResultsIdle.TabIndex = 13
        Me.lblResultsIdle.Text = "Result Card :  Idle"
        '
        'chkSmartElectricals
        '
        Me.chkSmartElectricals.AutoSize = true
        Me.chkSmartElectricals.Location = New System.Drawing.Point(162, 132)
        Me.chkSmartElectricals.Name = "chkSmartElectricals"
        Me.chkSmartElectricals.Size = New System.Drawing.Size(96, 17)
        Me.chkSmartElectricals.TabIndex = 12
        Me.chkSmartElectricals.Text = "Smart Electrics"
        Me.chkSmartElectricals.UseVisualStyleBackColor = true
        '
        'lblElectricalConsumables
        '
        Me.lblElectricalConsumables.AutoSize = true
        Me.lblElectricalConsumables.Location = New System.Drawing.Point(34, 154)
        Me.lblElectricalConsumables.Name = "lblElectricalConsumables"
        Me.lblElectricalConsumables.Size = New System.Drawing.Size(116, 13)
        Me.lblElectricalConsumables.TabIndex = 11
        Me.lblElectricalConsumables.Text = "Electrical Consumables"
        '
        'gvElectricalConsumables
        '
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gvElectricalConsumables.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.gvElectricalConsumables.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gvElectricalConsumables.DefaultCellStyle = DataGridViewCellStyle11
        Me.gvElectricalConsumables.Location = New System.Drawing.Point(33, 170)
        Me.gvElectricalConsumables.Name = "gvElectricalConsumables"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gvElectricalConsumables.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.gvElectricalConsumables.Size = New System.Drawing.Size(803, 250)
        Me.gvElectricalConsumables.TabIndex = 10
        '
        'txtDoorActuationTimeSeconds
        '
        Me.txtDoorActuationTimeSeconds.Location = New System.Drawing.Point(162, 101)
        Me.txtDoorActuationTimeSeconds.Name = "txtDoorActuationTimeSeconds"
        Me.txtDoorActuationTimeSeconds.Size = New System.Drawing.Size(100, 20)
        Me.txtDoorActuationTimeSeconds.TabIndex = 9
        '
        'txtAlternatorGearEfficiency
        '
        Me.txtAlternatorGearEfficiency.Location = New System.Drawing.Point(162, 71)
        Me.txtAlternatorGearEfficiency.Name = "txtAlternatorGearEfficiency"
        Me.txtAlternatorGearEfficiency.Size = New System.Drawing.Size(100, 20)
        Me.txtAlternatorGearEfficiency.TabIndex = 6
        '
        'txtAlternatorMapPath
        '
        Me.txtAlternatorMapPath.Location = New System.Drawing.Point(162, 43)
        Me.txtAlternatorMapPath.Name = "txtAlternatorMapPath"
        Me.txtAlternatorMapPath.Size = New System.Drawing.Size(319, 20)
        Me.txtAlternatorMapPath.TabIndex = 4
        '
        'txtPowernetVoltage
        '
        Me.txtPowernetVoltage.Location = New System.Drawing.Point(162, 16)
        Me.txtPowernetVoltage.Name = "txtPowernetVoltage"
        Me.txtPowernetVoltage.Size = New System.Drawing.Size(100, 20)
        Me.txtPowernetVoltage.TabIndex = 2
        '
        'lblDoorActuationTimeSeconds
        '
        Me.lblDoorActuationTimeSeconds.AutoSize = true
        Me.lblDoorActuationTimeSeconds.Location = New System.Drawing.Point(30, 105)
        Me.lblDoorActuationTimeSeconds.Name = "lblDoorActuationTimeSeconds"
        Me.lblDoorActuationTimeSeconds.Size = New System.Drawing.Size(114, 13)
        Me.lblDoorActuationTimeSeconds.TabIndex = 8
        Me.lblDoorActuationTimeSeconds.Text = "Door ActuationTime(S)"
        '
        'lblAlternatorGearEfficiency
        '
        Me.lblAlternatorGearEfficiency.AutoSize = true
        Me.lblAlternatorGearEfficiency.Location = New System.Drawing.Point(30, 75)
        Me.lblAlternatorGearEfficiency.Name = "lblAlternatorGearEfficiency"
        Me.lblAlternatorGearEfficiency.Size = New System.Drawing.Size(132, 13)
        Me.lblAlternatorGearEfficiency.TabIndex = 7
        Me.lblAlternatorGearEfficiency.Text = "Alternator Pulley Efficiency"
        '
        'lblAlternatormapPath
        '
        Me.lblAlternatormapPath.AutoSize = true
        Me.lblAlternatormapPath.Location = New System.Drawing.Point(30, 47)
        Me.lblAlternatormapPath.Name = "lblAlternatormapPath"
        Me.lblAlternatormapPath.Size = New System.Drawing.Size(76, 13)
        Me.lblAlternatormapPath.TabIndex = 5
        Me.lblAlternatormapPath.Text = "Alternator Map"
        '
        'lblPowerNetVoltage
        '
        Me.lblPowerNetVoltage.AutoSize = true
        Me.lblPowerNetVoltage.Location = New System.Drawing.Point(30, 20)
        Me.lblPowerNetVoltage.Name = "lblPowerNetVoltage"
        Me.lblPowerNetVoltage.Size = New System.Drawing.Size(91, 13)
        Me.lblPowerNetVoltage.TabIndex = 3
        Me.lblPowerNetVoltage.Text = "Powernet Voltage"
        '
        'tabPneumaticConfig
        '
        Me.tabPneumaticConfig.Controls.Add(Me.pnlPneumaticsUserInput)
        Me.tabPneumaticConfig.Controls.Add(Me.pnlPneumaticAuxillaries)
        Me.tabPneumaticConfig.Location = New System.Drawing.Point(4, 22)
        Me.tabPneumaticConfig.Name = "tabPneumaticConfig"
        Me.tabPneumaticConfig.Size = New System.Drawing.Size(1470, 704)
        Me.tabPneumaticConfig.TabIndex = 2
        Me.tabPneumaticConfig.Text = "Pneumatics"
        Me.tabPneumaticConfig.UseVisualStyleBackColor = true
        '
        'pnlPneumaticsUserInput
        '
        Me.pnlPneumaticsUserInput.Controls.Add(Me.btnActuationsMap)
        Me.pnlPneumaticsUserInput.Controls.Add(Me.btnCompressorMap)
        Me.pnlPneumaticsUserInput.Controls.Add(Me.lblPneumaticsVariablesTitle)
        Me.pnlPneumaticsUserInput.Controls.Add(Me.lblActuationsMap)
        Me.pnlPneumaticsUserInput.Controls.Add(Me.chkSmartAirCompression)
        Me.pnlPneumaticsUserInput.Controls.Add(Me.chkSmartRegeneration)
        Me.pnlPneumaticsUserInput.Controls.Add(Me.lblAdBlueDosing)
        Me.pnlPneumaticsUserInput.Controls.Add(Me.chkRetarderBrake)
        Me.pnlPneumaticsUserInput.Controls.Add(Me.txtKneelingHeightMillimeters)
        Me.pnlPneumaticsUserInput.Controls.Add(Me.lblAirSuspensionControl)
        Me.pnlPneumaticsUserInput.Controls.Add(Me.cboDoors)
        Me.pnlPneumaticsUserInput.Controls.Add(Me.txtCompressorMap)
        Me.pnlPneumaticsUserInput.Controls.Add(Me.lblCompressorGearEfficiency)
        Me.pnlPneumaticsUserInput.Controls.Add(Me.txtCompressorGearRatio)
        Me.pnlPneumaticsUserInput.Controls.Add(Me.lblCompressorGearRatio)
        Me.pnlPneumaticsUserInput.Controls.Add(Me.txtCompressorGearEfficiency)
        Me.pnlPneumaticsUserInput.Controls.Add(Me.lblCompressorMap)
        Me.pnlPneumaticsUserInput.Controls.Add(Me.cboAirSuspensionControl)
        Me.pnlPneumaticsUserInput.Controls.Add(Me.lblDoors)
        Me.pnlPneumaticsUserInput.Controls.Add(Me.cboAdBlueDosing)
        Me.pnlPneumaticsUserInput.Controls.Add(Me.lblKneelingHeightMillimeters)
        Me.pnlPneumaticsUserInput.Controls.Add(Me.txtActuationsMap)
        Me.pnlPneumaticsUserInput.Location = New System.Drawing.Point(403, 17)
        Me.pnlPneumaticsUserInput.Name = "pnlPneumaticsUserInput"
        Me.pnlPneumaticsUserInput.Size = New System.Drawing.Size(491, 576)
        Me.pnlPneumaticsUserInput.TabIndex = 53
        '
        'btnActuationsMap
        '
        Me.btnActuationsMap.Location = New System.Drawing.Point(449, 343)
        Me.btnActuationsMap.Name = "btnActuationsMap"
        Me.btnActuationsMap.Size = New System.Drawing.Size(35, 23)
        Me.btnActuationsMap.TabIndex = 21
        Me.btnActuationsMap.Text = ". . ."
        Me.btnActuationsMap.UseVisualStyleBackColor = true
        '
        'btnCompressorMap
        '
        Me.btnCompressorMap.Location = New System.Drawing.Point(450, 58)
        Me.btnCompressorMap.Name = "btnCompressorMap"
        Me.btnCompressorMap.Size = New System.Drawing.Size(35, 23)
        Me.btnCompressorMap.TabIndex = 13
        Me.btnCompressorMap.Text = ". . ."
        Me.btnCompressorMap.UseVisualStyleBackColor = true
        '
        'lblPneumaticsVariablesTitle
        '
        Me.lblPneumaticsVariablesTitle.AutoSize = true
        Me.lblPneumaticsVariablesTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblPneumaticsVariablesTitle.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblPneumaticsVariablesTitle.Location = New System.Drawing.Point(20, 18)
        Me.lblPneumaticsVariablesTitle.Name = "lblPneumaticsVariablesTitle"
        Me.lblPneumaticsVariablesTitle.Size = New System.Drawing.Size(122, 13)
        Me.lblPneumaticsVariablesTitle.TabIndex = 52
        Me.lblPneumaticsVariablesTitle.Text = "Pneumatic Variables"
        '
        'lblActuationsMap
        '
        Me.lblActuationsMap.AutoSize = true
        Me.lblActuationsMap.Location = New System.Drawing.Point(13, 348)
        Me.lblActuationsMap.Name = "lblActuationsMap"
        Me.lblActuationsMap.Size = New System.Drawing.Size(81, 13)
        Me.lblActuationsMap.TabIndex = 25
        Me.lblActuationsMap.Text = "Actuations Map"
        '
        'chkSmartAirCompression
        '
        Me.chkSmartAirCompression.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkSmartAirCompression.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.chkSmartAirCompression.Location = New System.Drawing.Point(16, 430)
        Me.chkSmartAirCompression.MinimumSize = New System.Drawing.Size(150, 0)
        Me.chkSmartAirCompression.Name = "chkSmartAirCompression"
        Me.chkSmartAirCompression.Size = New System.Drawing.Size(153, 22)
        Me.chkSmartAirCompression.TabIndex = 23
        Me.chkSmartAirCompression.Text = "Smart Pneumatics"
        Me.chkSmartAirCompression.UseVisualStyleBackColor = true
        '
        'chkSmartRegeneration
        '
        Me.chkSmartRegeneration.CheckAlign = System.Drawing.ContentAlignment.BottomRight
        Me.chkSmartRegeneration.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.chkSmartRegeneration.Location = New System.Drawing.Point(16, 469)
        Me.chkSmartRegeneration.MinimumSize = New System.Drawing.Size(150, 0)
        Me.chkSmartRegeneration.Name = "chkSmartRegeneration"
        Me.chkSmartRegeneration.Size = New System.Drawing.Size(153, 22)
        Me.chkSmartRegeneration.TabIndex = 24
        Me.chkSmartRegeneration.Text = "Smart Regeneration"
        Me.chkSmartRegeneration.UseVisualStyleBackColor = true
        '
        'lblAdBlueDosing
        '
        Me.lblAdBlueDosing.AutoSize = true
        Me.lblAdBlueDosing.Location = New System.Drawing.Point(13, 184)
        Me.lblAdBlueDosing.Name = "lblAdBlueDosing"
        Me.lblAdBlueDosing.Size = New System.Drawing.Size(77, 13)
        Me.lblAdBlueDosing.TabIndex = 26
        Me.lblAdBlueDosing.Text = "AdBlue Dosing"
        '
        'chkRetarderBrake
        '
        Me.chkRetarderBrake.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkRetarderBrake.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.chkRetarderBrake.Location = New System.Drawing.Point(16, 389)
        Me.chkRetarderBrake.MinimumSize = New System.Drawing.Size(150, 0)
        Me.chkRetarderBrake.Name = "chkRetarderBrake"
        Me.chkRetarderBrake.Size = New System.Drawing.Size(153, 22)
        Me.chkRetarderBrake.TabIndex = 22
        Me.chkRetarderBrake.TabStop = false
        Me.chkRetarderBrake.Text = "Retarder Brake"
        Me.chkRetarderBrake.UseVisualStyleBackColor = true
        '
        'txtKneelingHeightMillimeters
        '
        Me.txtKneelingHeightMillimeters.Location = New System.Drawing.Point(156, 304)
        Me.txtKneelingHeightMillimeters.Name = "txtKneelingHeightMillimeters"
        Me.txtKneelingHeightMillimeters.Size = New System.Drawing.Size(120, 20)
        Me.txtKneelingHeightMillimeters.TabIndex = 19
        '
        'lblAirSuspensionControl
        '
        Me.lblAirSuspensionControl.AutoSize = true
        Me.lblAirSuspensionControl.Location = New System.Drawing.Point(13, 225)
        Me.lblAirSuspensionControl.Name = "lblAirSuspensionControl"
        Me.lblAirSuspensionControl.Size = New System.Drawing.Size(113, 13)
        Me.lblAirSuspensionControl.TabIndex = 29
        Me.lblAirSuspensionControl.Text = "Air Suspension Control"
        '
        'cboDoors
        '
        Me.cboDoors.FormattingEnabled = true
        Me.cboDoors.Items.AddRange(New Object() {"<Select>", "Pneumatic", "Electric"})
        Me.cboDoors.Location = New System.Drawing.Point(156, 262)
        Me.cboDoors.Name = "cboDoors"
        Me.cboDoors.Size = New System.Drawing.Size(121, 21)
        Me.cboDoors.TabIndex = 18
        '
        'txtCompressorMap
        '
        Me.txtCompressorMap.Location = New System.Drawing.Point(156, 60)
        Me.txtCompressorMap.Name = "txtCompressorMap"
        Me.txtCompressorMap.Size = New System.Drawing.Size(275, 20)
        Me.txtCompressorMap.TabIndex = 12
        '
        'lblCompressorGearEfficiency
        '
        Me.lblCompressorGearEfficiency.AutoSize = true
        Me.lblCompressorGearEfficiency.Location = New System.Drawing.Point(13, 142)
        Me.lblCompressorGearEfficiency.Name = "lblCompressorGearEfficiency"
        Me.lblCompressorGearEfficiency.Size = New System.Drawing.Size(137, 13)
        Me.lblCompressorGearEfficiency.TabIndex = 30
        Me.lblCompressorGearEfficiency.Text = "Compressor Gear Efficiency"
        '
        'txtCompressorGearRatio
        '
        Me.txtCompressorGearRatio.ForeColor = System.Drawing.Color.Black
        Me.txtCompressorGearRatio.Location = New System.Drawing.Point(156, 98)
        Me.txtCompressorGearRatio.Name = "txtCompressorGearRatio"
        Me.txtCompressorGearRatio.Size = New System.Drawing.Size(121, 20)
        Me.txtCompressorGearRatio.TabIndex = 14
        '
        'lblCompressorGearRatio
        '
        Me.lblCompressorGearRatio.AutoSize = true
        Me.lblCompressorGearRatio.Location = New System.Drawing.Point(13, 99)
        Me.lblCompressorGearRatio.Name = "lblCompressorGearRatio"
        Me.lblCompressorGearRatio.Size = New System.Drawing.Size(116, 13)
        Me.lblCompressorGearRatio.TabIndex = 14
        Me.lblCompressorGearRatio.Text = "Compressor Gear Ratio"
        '
        'txtCompressorGearEfficiency
        '
        Me.txtCompressorGearEfficiency.Location = New System.Drawing.Point(156, 139)
        Me.txtCompressorGearEfficiency.Name = "txtCompressorGearEfficiency"
        Me.txtCompressorGearEfficiency.Size = New System.Drawing.Size(121, 20)
        Me.txtCompressorGearEfficiency.TabIndex = 15
        '
        'lblCompressorMap
        '
        Me.lblCompressorMap.AutoSize = true
        Me.lblCompressorMap.Location = New System.Drawing.Point(13, 60)
        Me.lblCompressorMap.Name = "lblCompressorMap"
        Me.lblCompressorMap.Size = New System.Drawing.Size(86, 13)
        Me.lblCompressorMap.TabIndex = 32
        Me.lblCompressorMap.Text = "Compressor Map"
        '
        'cboAirSuspensionControl
        '
        Me.cboAirSuspensionControl.FormattingEnabled = true
        Me.cboAirSuspensionControl.Items.AddRange(New Object() {"<Select>", "Mechanically", "Electrically"})
        Me.cboAirSuspensionControl.Location = New System.Drawing.Point(156, 220)
        Me.cboAirSuspensionControl.Name = "cboAirSuspensionControl"
        Me.cboAirSuspensionControl.Size = New System.Drawing.Size(121, 21)
        Me.cboAirSuspensionControl.TabIndex = 17
        '
        'lblDoors
        '
        Me.lblDoors.AutoSize = true
        Me.lblDoors.Location = New System.Drawing.Point(13, 266)
        Me.lblDoors.Name = "lblDoors"
        Me.lblDoors.Size = New System.Drawing.Size(79, 13)
        Me.lblDoors.TabIndex = 34
        Me.lblDoors.Text = "Door Operation"
        '
        'cboAdBlueDosing
        '
        Me.cboAdBlueDosing.FormattingEnabled = true
        Me.cboAdBlueDosing.Items.AddRange(New Object() {"<Select>", "Pneumatic", "Electric"})
        Me.cboAdBlueDosing.Location = New System.Drawing.Point(156, 180)
        Me.cboAdBlueDosing.Name = "cboAdBlueDosing"
        Me.cboAdBlueDosing.Size = New System.Drawing.Size(121, 21)
        Me.cboAdBlueDosing.TabIndex = 16
        '
        'lblKneelingHeightMillimeters
        '
        Me.lblKneelingHeightMillimeters.AutoSize = true
        Me.lblKneelingHeightMillimeters.Location = New System.Drawing.Point(13, 308)
        Me.lblKneelingHeightMillimeters.Name = "lblKneelingHeightMillimeters"
        Me.lblKneelingHeightMillimeters.Size = New System.Drawing.Size(133, 13)
        Me.lblKneelingHeightMillimeters.TabIndex = 35
        Me.lblKneelingHeightMillimeters.Text = "Kneeling Height Millimeters"
        '
        'txtActuationsMap
        '
        Me.txtActuationsMap.Location = New System.Drawing.Point(156, 345)
        Me.txtActuationsMap.Name = "txtActuationsMap"
        Me.txtActuationsMap.Size = New System.Drawing.Size(275, 20)
        Me.txtActuationsMap.TabIndex = 20
        '
        'pnlPneumaticAuxillaries
        '
        Me.pnlPneumaticAuxillaries.Controls.Add(Me.lblPneumaticAuxillariesTitle)
        Me.pnlPneumaticAuxillaries.Controls.Add(Me.lblAdBlueNIperMinute)
        Me.pnlPneumaticAuxillaries.Controls.Add(Me.lblAirControlledSuspensionNIperMinute)
        Me.pnlPneumaticAuxillaries.Controls.Add(Me.lblBrakingNoRetarderNIperKG)
        Me.pnlPneumaticAuxillaries.Controls.Add(Me.lblBrakingWithRetarderNIperKG)
        Me.pnlPneumaticAuxillaries.Controls.Add(Me.lblBreakingPerKneelingNIperKGinMM)
        Me.pnlPneumaticAuxillaries.Controls.Add(Me.lblDeadVolBlowOutsPerLitresperHour)
        Me.pnlPneumaticAuxillaries.Controls.Add(Me.lblDeadVolumeLitres)
        Me.pnlPneumaticAuxillaries.Controls.Add(Me.lblNonSmartRegenFractionTotalAirDemand)
        Me.pnlPneumaticAuxillaries.Controls.Add(Me.lblOverrunUtilisationForCompressionFraction)
        Me.pnlPneumaticAuxillaries.Controls.Add(Me.lblPerDoorOpeningNI)
        Me.pnlPneumaticAuxillaries.Controls.Add(Me.lblPerStopBrakeActuationNIperKG)
        Me.pnlPneumaticAuxillaries.Controls.Add(Me.lblSmartRegenFractionTotalAirDemand)
        Me.pnlPneumaticAuxillaries.Controls.Add(Me.txtAdBlueNIperMinute)
        Me.pnlPneumaticAuxillaries.Controls.Add(Me.txtAirControlledSuspensionNIperMinute)
        Me.pnlPneumaticAuxillaries.Controls.Add(Me.txtBrakingNoRetarderNIperKG)
        Me.pnlPneumaticAuxillaries.Controls.Add(Me.txtBrakingWithRetarderNIperKG)
        Me.pnlPneumaticAuxillaries.Controls.Add(Me.txtBreakingPerKneelingNIperKGinMM)
        Me.pnlPneumaticAuxillaries.Controls.Add(Me.txtDeadVolBlowOutsPerLitresperHour)
        Me.pnlPneumaticAuxillaries.Controls.Add(Me.txtDeadVolumeLitres)
        Me.pnlPneumaticAuxillaries.Controls.Add(Me.txtNonSmartRegenFractionTotalAirDemand)
        Me.pnlPneumaticAuxillaries.Controls.Add(Me.txtOverrunUtilisationForCompressionFraction)
        Me.pnlPneumaticAuxillaries.Controls.Add(Me.txtPerDoorOpeningNI)
        Me.pnlPneumaticAuxillaries.Controls.Add(Me.txtPerStopBrakeActuationNIperKG)
        Me.pnlPneumaticAuxillaries.Controls.Add(Me.txtSmartRegenFractionTotalAirDemand)
        Me.pnlPneumaticAuxillaries.Location = New System.Drawing.Point(20, 17)
        Me.pnlPneumaticAuxillaries.Name = "pnlPneumaticAuxillaries"
        Me.pnlPneumaticAuxillaries.Size = New System.Drawing.Size(363, 576)
        Me.pnlPneumaticAuxillaries.TabIndex = 52
        '
        'lblPneumaticAuxillariesTitle
        '
        Me.lblPneumaticAuxillariesTitle.AutoSize = true
        Me.lblPneumaticAuxillariesTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblPneumaticAuxillariesTitle.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblPneumaticAuxillariesTitle.Location = New System.Drawing.Point(23, 18)
        Me.lblPneumaticAuxillariesTitle.Name = "lblPneumaticAuxillariesTitle"
        Me.lblPneumaticAuxillariesTitle.Size = New System.Drawing.Size(158, 13)
        Me.lblPneumaticAuxillariesTitle.TabIndex = 24
        Me.lblPneumaticAuxillariesTitle.Text = "Pneumatic Auxiliaries Data"
        '
        'lblAdBlueNIperMinute
        '
        Me.lblAdBlueNIperMinute.AutoSize = true
        Me.lblAdBlueNIperMinute.Location = New System.Drawing.Point(20, 60)
        Me.lblAdBlueNIperMinute.MinimumSize = New System.Drawing.Size(120, 0)
        Me.lblAdBlueNIperMinute.Name = "lblAdBlueNIperMinute"
        Me.lblAdBlueNIperMinute.Size = New System.Drawing.Size(120, 13)
        Me.lblAdBlueNIperMinute.TabIndex = 12
        Me.lblAdBlueNIperMinute.Text = "AdBlue NI per Minute "
        '
        'lblAirControlledSuspensionNIperMinute
        '
        Me.lblAirControlledSuspensionNIperMinute.AutoSize = true
        Me.lblAirControlledSuspensionNIperMinute.Location = New System.Drawing.Point(20, 99)
        Me.lblAirControlledSuspensionNIperMinute.MinimumSize = New System.Drawing.Size(120, 0)
        Me.lblAirControlledSuspensionNIperMinute.Name = "lblAirControlledSuspensionNIperMinute"
        Me.lblAirControlledSuspensionNIperMinute.Size = New System.Drawing.Size(184, 13)
        Me.lblAirControlledSuspensionNIperMinute.TabIndex = 13
        Me.lblAirControlledSuspensionNIperMinute.Text = "Air Controlled Suspension NI / Minute"
        '
        'lblBrakingNoRetarderNIperKG
        '
        Me.lblBrakingNoRetarderNIperKG.AutoSize = true
        Me.lblBrakingNoRetarderNIperKG.Location = New System.Drawing.Point(20, 140)
        Me.lblBrakingNoRetarderNIperKG.MinimumSize = New System.Drawing.Size(120, 0)
        Me.lblBrakingNoRetarderNIperKG.Name = "lblBrakingNoRetarderNIperKG"
        Me.lblBrakingNoRetarderNIperKG.Size = New System.Drawing.Size(138, 13)
        Me.lblBrakingNoRetarderNIperKG.TabIndex = 14
        Me.lblBrakingNoRetarderNIperKG.Text = "Braking No Retarder NI/KG"
        '
        'lblBrakingWithRetarderNIperKG
        '
        Me.lblBrakingWithRetarderNIperKG.AutoSize = true
        Me.lblBrakingWithRetarderNIperKG.Location = New System.Drawing.Point(20, 183)
        Me.lblBrakingWithRetarderNIperKG.MinimumSize = New System.Drawing.Size(120, 0)
        Me.lblBrakingWithRetarderNIperKG.Name = "lblBrakingWithRetarderNIperKG"
        Me.lblBrakingWithRetarderNIperKG.Size = New System.Drawing.Size(146, 13)
        Me.lblBrakingWithRetarderNIperKG.TabIndex = 15
        Me.lblBrakingWithRetarderNIperKG.Text = "Braking With Retarder NI/KG"
        '
        'lblBreakingPerKneelingNIperKGinMM
        '
        Me.lblBreakingPerKneelingNIperKGinMM.AutoSize = true
        Me.lblBreakingPerKneelingNIperKGinMM.Location = New System.Drawing.Point(20, 222)
        Me.lblBreakingPerKneelingNIperKGinMM.MinimumSize = New System.Drawing.Size(120, 0)
        Me.lblBreakingPerKneelingNIperKGinMM.Name = "lblBreakingPerKneelingNIperKGinMM"
        Me.lblBreakingPerKneelingNIperKGinMM.Size = New System.Drawing.Size(178, 13)
        Me.lblBreakingPerKneelingNIperKGinMM.TabIndex = 16
        Me.lblBreakingPerKneelingNIperKGinMM.Text = "Breaking Per Kneeling NI/KG in MM"
        '
        'lblDeadVolBlowOutsPerLitresperHour
        '
        Me.lblDeadVolBlowOutsPerLitresperHour.AutoSize = true
        Me.lblDeadVolBlowOutsPerLitresperHour.Location = New System.Drawing.Point(20, 263)
        Me.lblDeadVolBlowOutsPerLitresperHour.MinimumSize = New System.Drawing.Size(120, 0)
        Me.lblDeadVolBlowOutsPerLitresperHour.Name = "lblDeadVolBlowOutsPerLitresperHour"
        Me.lblDeadVolBlowOutsPerLitresperHour.Size = New System.Drawing.Size(148, 13)
        Me.lblDeadVolBlowOutsPerLitresperHour.TabIndex = 17
        Me.lblDeadVolBlowOutsPerLitresperHour.Text = "Dead Vol Blowouts /L / Hour "
        '
        'lblDeadVolumeLitres
        '
        Me.lblDeadVolumeLitres.AutoSize = true
        Me.lblDeadVolumeLitres.Location = New System.Drawing.Point(20, 303)
        Me.lblDeadVolumeLitres.MinimumSize = New System.Drawing.Size(120, 0)
        Me.lblDeadVolumeLitres.Name = "lblDeadVolumeLitres"
        Me.lblDeadVolumeLitres.Size = New System.Drawing.Size(120, 13)
        Me.lblDeadVolumeLitres.TabIndex = 18
        Me.lblDeadVolumeLitres.Text = "Dead Volume Litres"
        '
        'lblNonSmartRegenFractionTotalAirDemand
        '
        Me.lblNonSmartRegenFractionTotalAirDemand.AutoSize = true
        Me.lblNonSmartRegenFractionTotalAirDemand.Location = New System.Drawing.Point(20, 346)
        Me.lblNonSmartRegenFractionTotalAirDemand.MinimumSize = New System.Drawing.Size(120, 0)
        Me.lblNonSmartRegenFractionTotalAirDemand.Name = "lblNonSmartRegenFractionTotalAirDemand"
        Me.lblNonSmartRegenFractionTotalAirDemand.Size = New System.Drawing.Size(218, 13)
        Me.lblNonSmartRegenFractionTotalAirDemand.TabIndex = 19
        Me.lblNonSmartRegenFractionTotalAirDemand.Text = "Non Smart Regen Fraction Total Air Demand"
        '
        'lblOverrunUtilisationForCompressionFraction
        '
        Me.lblOverrunUtilisationForCompressionFraction.AutoSize = true
        Me.lblOverrunUtilisationForCompressionFraction.Location = New System.Drawing.Point(20, 388)
        Me.lblOverrunUtilisationForCompressionFraction.MinimumSize = New System.Drawing.Size(120, 0)
        Me.lblOverrunUtilisationForCompressionFraction.Name = "lblOverrunUtilisationForCompressionFraction"
        Me.lblOverrunUtilisationForCompressionFraction.Size = New System.Drawing.Size(215, 13)
        Me.lblOverrunUtilisationForCompressionFraction.TabIndex = 20
        Me.lblOverrunUtilisationForCompressionFraction.Text = "Overrun Utilisation For Compression Fraction"
        '
        'lblPerDoorOpeningNI
        '
        Me.lblPerDoorOpeningNI.AutoSize = true
        Me.lblPerDoorOpeningNI.Location = New System.Drawing.Point(20, 427)
        Me.lblPerDoorOpeningNI.MinimumSize = New System.Drawing.Size(120, 0)
        Me.lblPerDoorOpeningNI.Name = "lblPerDoorOpeningNI"
        Me.lblPerDoorOpeningNI.Size = New System.Drawing.Size(120, 13)
        Me.lblPerDoorOpeningNI.TabIndex = 21
        Me.lblPerDoorOpeningNI.Text = "Per Door Opening NI"
        '
        'lblPerStopBrakeActuationNIperKG
        '
        Me.lblPerStopBrakeActuationNIperKG.AutoSize = true
        Me.lblPerStopBrakeActuationNIperKG.Location = New System.Drawing.Point(20, 469)
        Me.lblPerStopBrakeActuationNIperKG.MinimumSize = New System.Drawing.Size(120, 0)
        Me.lblPerStopBrakeActuationNIperKG.Name = "lblPerStopBrakeActuationNIperKG"
        Me.lblPerStopBrakeActuationNIperKG.Size = New System.Drawing.Size(161, 13)
        Me.lblPerStopBrakeActuationNIperKG.TabIndex = 22
        Me.lblPerStopBrakeActuationNIperKG.Text = "Per Stop Brake Actuation NI/KG"
        '
        'lblSmartRegenFractionTotalAirDemand
        '
        Me.lblSmartRegenFractionTotalAirDemand.AutoSize = true
        Me.lblSmartRegenFractionTotalAirDemand.Location = New System.Drawing.Point(20, 510)
        Me.lblSmartRegenFractionTotalAirDemand.MinimumSize = New System.Drawing.Size(120, 0)
        Me.lblSmartRegenFractionTotalAirDemand.Name = "lblSmartRegenFractionTotalAirDemand"
        Me.lblSmartRegenFractionTotalAirDemand.Size = New System.Drawing.Size(195, 13)
        Me.lblSmartRegenFractionTotalAirDemand.TabIndex = 23
        Me.lblSmartRegenFractionTotalAirDemand.Text = "Smart Regen Fraction Total Air Demand"
        '
        'txtAdBlueNIperMinute
        '
        Me.txtAdBlueNIperMinute.Location = New System.Drawing.Point(242, 60)
        Me.txtAdBlueNIperMinute.Name = "txtAdBlueNIperMinute"
        Me.txtAdBlueNIperMinute.Size = New System.Drawing.Size(100, 20)
        Me.txtAdBlueNIperMinute.TabIndex = 0
        '
        'txtAirControlledSuspensionNIperMinute
        '
        Me.txtAirControlledSuspensionNIperMinute.Location = New System.Drawing.Point(242, 98)
        Me.txtAirControlledSuspensionNIperMinute.Name = "txtAirControlledSuspensionNIperMinute"
        Me.txtAirControlledSuspensionNIperMinute.Size = New System.Drawing.Size(100, 20)
        Me.txtAirControlledSuspensionNIperMinute.TabIndex = 1
        '
        'txtBrakingNoRetarderNIperKG
        '
        Me.txtBrakingNoRetarderNIperKG.Location = New System.Drawing.Point(242, 139)
        Me.txtBrakingNoRetarderNIperKG.Name = "txtBrakingNoRetarderNIperKG"
        Me.txtBrakingNoRetarderNIperKG.Size = New System.Drawing.Size(100, 20)
        Me.txtBrakingNoRetarderNIperKG.TabIndex = 2
        '
        'txtBrakingWithRetarderNIperKG
        '
        Me.txtBrakingWithRetarderNIperKG.Location = New System.Drawing.Point(242, 180)
        Me.txtBrakingWithRetarderNIperKG.Name = "txtBrakingWithRetarderNIperKG"
        Me.txtBrakingWithRetarderNIperKG.Size = New System.Drawing.Size(100, 20)
        Me.txtBrakingWithRetarderNIperKG.TabIndex = 3
        '
        'txtBreakingPerKneelingNIperKGinMM
        '
        Me.txtBreakingPerKneelingNIperKGinMM.Location = New System.Drawing.Point(242, 221)
        Me.txtBreakingPerKneelingNIperKGinMM.Name = "txtBreakingPerKneelingNIperKGinMM"
        Me.txtBreakingPerKneelingNIperKGinMM.Size = New System.Drawing.Size(100, 20)
        Me.txtBreakingPerKneelingNIperKGinMM.TabIndex = 4
        '
        'txtDeadVolBlowOutsPerLitresperHour
        '
        Me.txtDeadVolBlowOutsPerLitresperHour.Location = New System.Drawing.Point(242, 262)
        Me.txtDeadVolBlowOutsPerLitresperHour.Name = "txtDeadVolBlowOutsPerLitresperHour"
        Me.txtDeadVolBlowOutsPerLitresperHour.Size = New System.Drawing.Size(100, 20)
        Me.txtDeadVolBlowOutsPerLitresperHour.TabIndex = 5
        '
        'txtDeadVolumeLitres
        '
        Me.txtDeadVolumeLitres.Location = New System.Drawing.Point(242, 303)
        Me.txtDeadVolumeLitres.Name = "txtDeadVolumeLitres"
        Me.txtDeadVolumeLitres.Size = New System.Drawing.Size(100, 20)
        Me.txtDeadVolumeLitres.TabIndex = 6
        '
        'txtNonSmartRegenFractionTotalAirDemand
        '
        Me.txtNonSmartRegenFractionTotalAirDemand.Location = New System.Drawing.Point(242, 344)
        Me.txtNonSmartRegenFractionTotalAirDemand.Name = "txtNonSmartRegenFractionTotalAirDemand"
        Me.txtNonSmartRegenFractionTotalAirDemand.Size = New System.Drawing.Size(100, 20)
        Me.txtNonSmartRegenFractionTotalAirDemand.TabIndex = 7
        '
        'txtOverrunUtilisationForCompressionFraction
        '
        Me.txtOverrunUtilisationForCompressionFraction.Location = New System.Drawing.Point(242, 385)
        Me.txtOverrunUtilisationForCompressionFraction.Name = "txtOverrunUtilisationForCompressionFraction"
        Me.txtOverrunUtilisationForCompressionFraction.Size = New System.Drawing.Size(100, 20)
        Me.txtOverrunUtilisationForCompressionFraction.TabIndex = 8
        '
        'txtPerDoorOpeningNI
        '
        Me.txtPerDoorOpeningNI.Location = New System.Drawing.Point(242, 426)
        Me.txtPerDoorOpeningNI.Name = "txtPerDoorOpeningNI"
        Me.txtPerDoorOpeningNI.Size = New System.Drawing.Size(100, 20)
        Me.txtPerDoorOpeningNI.TabIndex = 9
        '
        'txtPerStopBrakeActuationNIperKG
        '
        Me.txtPerStopBrakeActuationNIperKG.Location = New System.Drawing.Point(242, 467)
        Me.txtPerStopBrakeActuationNIperKG.Name = "txtPerStopBrakeActuationNIperKG"
        Me.txtPerStopBrakeActuationNIperKG.Size = New System.Drawing.Size(100, 20)
        Me.txtPerStopBrakeActuationNIperKG.TabIndex = 10
        '
        'txtSmartRegenFractionTotalAirDemand
        '
        Me.txtSmartRegenFractionTotalAirDemand.Location = New System.Drawing.Point(242, 508)
        Me.txtSmartRegenFractionTotalAirDemand.Name = "txtSmartRegenFractionTotalAirDemand"
        Me.txtSmartRegenFractionTotalAirDemand.Size = New System.Drawing.Size(100, 20)
        Me.txtSmartRegenFractionTotalAirDemand.TabIndex = 11
        '
        'tabHVACConfig
        '
        Me.tabHVACConfig.Controls.Add(Me.btnSSMBSource)
        Me.tabHVACConfig.Controls.Add(Me.lblSSMFilePath)
        Me.tabHVACConfig.Controls.Add(Me.txtSSMFilePath)
        Me.tabHVACConfig.Controls.Add(Me.lblHVACTitle)
        Me.tabHVACConfig.Controls.Add(Me.txtHVACFuellingLitresPerHour)
        Me.tabHVACConfig.Controls.Add(Me.lblHVACFuellingLitresPerHour)
        Me.tabHVACConfig.Controls.Add(Me.txtHVACMechanicalLoadPowerWatts)
        Me.tabHVACConfig.Controls.Add(Me.lblHVACMechanicalLoadPowerWatts)
        Me.tabHVACConfig.Controls.Add(Me.txtHVACElectricalLoadPowerWatts)
        Me.tabHVACConfig.Controls.Add(Me.lblHVACElectricalLoadPowerWatts)
        Me.tabHVACConfig.Location = New System.Drawing.Point(4, 22)
        Me.tabHVACConfig.Name = "tabHVACConfig"
        Me.tabHVACConfig.Size = New System.Drawing.Size(1470, 704)
        Me.tabHVACConfig.TabIndex = 3
        Me.tabHVACConfig.Text = "HVAC"
        Me.tabHVACConfig.UseVisualStyleBackColor = true
        '
        'btnSSMBSource
        '
        Me.btnSSMBSource.Location = New System.Drawing.Point(706, 93)
        Me.btnSSMBSource.Name = "btnSSMBSource"
        Me.btnSSMBSource.Size = New System.Drawing.Size(45, 23)
        Me.btnSSMBSource.TabIndex = 31
        Me.btnSSMBSource.Text = ". . ."
        Me.btnSSMBSource.UseVisualStyleBackColor = true
        '
        'lblSSMFilePath
        '
        Me.lblSSMFilePath.AutoSize = true
        Me.lblSSMFilePath.Location = New System.Drawing.Point(25, 99)
        Me.lblSSMFilePath.Name = "lblSSMFilePath"
        Me.lblSSMFilePath.Size = New System.Drawing.Size(171, 13)
        Me.lblSSMFilePath.TabIndex = 30
        Me.lblSSMFilePath.Text = "Steady State Model File ( .AHSM  )"
        '
        'txtSSMFilePath
        '
        Me.txtSSMFilePath.Location = New System.Drawing.Point(204, 96)
        Me.txtSSMFilePath.Name = "txtSSMFilePath"
        Me.txtSSMFilePath.ReadOnly = true
        Me.txtSSMFilePath.Size = New System.Drawing.Size(485, 20)
        Me.txtSSMFilePath.TabIndex = 29
        '
        'lblHVACTitle
        '
        Me.lblHVACTitle.AutoSize = true
        Me.lblHVACTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblHVACTitle.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblHVACTitle.Location = New System.Drawing.Point(31, 37)
        Me.lblHVACTitle.Name = "lblHVACTitle"
        Me.lblHVACTitle.Size = New System.Drawing.Size(164, 13)
        Me.lblHVACTitle.TabIndex = 25
        Me.lblHVACTitle.Text = "Steady State Output Values"
        '
        'txtHVACFuellingLitresPerHour
        '
        Me.txtHVACFuellingLitresPerHour.Location = New System.Drawing.Point(186, 265)
        Me.txtHVACFuellingLitresPerHour.Name = "txtHVACFuellingLitresPerHour"
        Me.txtHVACFuellingLitresPerHour.Size = New System.Drawing.Size(100, 20)
        Me.txtHVACFuellingLitresPerHour.TabIndex = 5
        '
        'lblHVACFuellingLitresPerHour
        '
        Me.lblHVACFuellingLitresPerHour.AutoSize = true
        Me.lblHVACFuellingLitresPerHour.Location = New System.Drawing.Point(25, 269)
        Me.lblHVACFuellingLitresPerHour.Name = "lblHVACFuellingLitresPerHour"
        Me.lblHVACFuellingLitresPerHour.Size = New System.Drawing.Size(116, 13)
        Me.lblHVACFuellingLitresPerHour.TabIndex = 4
        Me.lblHVACFuellingLitresPerHour.Text = "Fuelling Litres Per Hour"
        '
        'txtHVACMechanicalLoadPowerWatts
        '
        Me.txtHVACMechanicalLoadPowerWatts.Location = New System.Drawing.Point(186, 213)
        Me.txtHVACMechanicalLoadPowerWatts.Name = "txtHVACMechanicalLoadPowerWatts"
        Me.txtHVACMechanicalLoadPowerWatts.Size = New System.Drawing.Size(100, 20)
        Me.txtHVACMechanicalLoadPowerWatts.TabIndex = 3
        '
        'lblHVACMechanicalLoadPowerWatts
        '
        Me.lblHVACMechanicalLoadPowerWatts.AutoSize = true
        Me.lblHVACMechanicalLoadPowerWatts.Location = New System.Drawing.Point(25, 217)
        Me.lblHVACMechanicalLoadPowerWatts.Name = "lblHVACMechanicalLoadPowerWatts"
        Me.lblHVACMechanicalLoadPowerWatts.Size = New System.Drawing.Size(153, 13)
        Me.lblHVACMechanicalLoadPowerWatts.TabIndex = 2
        Me.lblHVACMechanicalLoadPowerWatts.Text = "Mechanical Load Power Watts"
        '
        'txtHVACElectricalLoadPowerWatts
        '
        Me.txtHVACElectricalLoadPowerWatts.Location = New System.Drawing.Point(186, 161)
        Me.txtHVACElectricalLoadPowerWatts.Name = "txtHVACElectricalLoadPowerWatts"
        Me.txtHVACElectricalLoadPowerWatts.Size = New System.Drawing.Size(100, 20)
        Me.txtHVACElectricalLoadPowerWatts.TabIndex = 1
        '
        'lblHVACElectricalLoadPowerWatts
        '
        Me.lblHVACElectricalLoadPowerWatts.AutoSize = true
        Me.lblHVACElectricalLoadPowerWatts.Location = New System.Drawing.Point(25, 165)
        Me.lblHVACElectricalLoadPowerWatts.Name = "lblHVACElectricalLoadPowerWatts"
        Me.lblHVACElectricalLoadPowerWatts.Size = New System.Drawing.Size(141, 13)
        Me.lblHVACElectricalLoadPowerWatts.TabIndex = 0
        Me.lblHVACElectricalLoadPowerWatts.Text = "Electrical Load Power Watts"
        '
        'tabPlayground
        '
        Me.tabPlayground.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tabPlayground.Controls.Add(Me.chkEngineStopped)
        Me.tabPlayground.Controls.Add(Me.pnlM13)
        Me.tabPlayground.Controls.Add(Me.lblM13_Title)
        Me.tabPlayground.Controls.Add(Me.pnlM12)
        Me.tabPlayground.Controls.Add(Me.lblM12_Title)
        Me.tabPlayground.Controls.Add(Me.mblM11_Title)
        Me.tabPlayground.Controls.Add(Me.pnlM11)
        Me.tabPlayground.Controls.Add(Me.chkSignalsSmartAirCompression)
        Me.tabPlayground.Controls.Add(Me.chkSignalsSmartElectrics)
        Me.tabPlayground.Controls.Add(Me.pnlM10)
        Me.tabPlayground.Controls.Add(Me.lblM10_Title)
        Me.tabPlayground.Controls.Add(Me.lblM9Title)
        Me.tabPlayground.Controls.Add(Me.pnlM9)
        Me.tabPlayground.Controls.Add(Me.lblM8_Title)
        Me.tabPlayground.Controls.Add(Me.pnlM8)
        Me.tabPlayground.Controls.Add(Me.lblM7_Title)
        Me.tabPlayground.Controls.Add(Me.Panel4)
        Me.tabPlayground.Controls.Add(Me.lblM6Title)
        Me.tabPlayground.Controls.Add(Me.Panel2)
        Me.tabPlayground.Controls.Add(Me.lblPreExistingAuxPower)
        Me.tabPlayground.Controls.Add(Me.txtPreExistingAuxPower)
        Me.tabPlayground.Controls.Add(Me.chkIdle)
        Me.tabPlayground.Controls.Add(Me.chkInNeutral)
        Me.tabPlayground.Controls.Add(Me.lblM5_SmartAltSetGeneration)
        Me.tabPlayground.Controls.Add(Me.Panel3)
        Me.tabPlayground.Controls.Add(Me.PictureBox1)
        Me.tabPlayground.Controls.Add(Me.lblM4_AirCompressor)
        Me.tabPlayground.Controls.Add(Me.btnFinish)
        Me.tabPlayground.Controls.Add(Me.Panel1)
        Me.tabPlayground.Controls.Add(Me.lblM3_AveragePneumaticLoad)
        Me.tabPlayground.Controls.Add(Me.pnl_M3_Displays)
        Me.tabPlayground.Controls.Add(Me.btnStart)
        Me.tabPlayground.Controls.Add(Me.lblM2AverageElectricalLoadTitle)
        Me.tabPlayground.Controls.Add(Me.pnl_M2_Displays)
        Me.tabPlayground.Controls.Add(Me.lblM1_HVACAverageLoad)
        Me.tabPlayground.Controls.Add(Me.pnl_M1_Displays)
        Me.tabPlayground.Controls.Add(Me.lblM05SmartalternatorSetEfficiency)
        Me.tabPlayground.Controls.Add(Me.pnl_M05_Displays)
        Me.tabPlayground.Controls.Add(Me.lblM0Outputs)
        Me.tabPlayground.Controls.Add(Me.pnl_M0_Displays)
        Me.tabPlayground.Controls.Add(Me.chkClutchEngaged)
        Me.tabPlayground.Controls.Add(Me.lblTotalCycleTimeSeconds)
        Me.tabPlayground.Controls.Add(Me.lblEngineSpeed)
        Me.tabPlayground.Controls.Add(Me.lblEngineMotoringPower)
        Me.tabPlayground.Controls.Add(Me.lblEngineDrivelineTorque)
        Me.tabPlayground.Controls.Add(Me.lblEngineDrivelinePower)
        Me.tabPlayground.Controls.Add(Me.txtTotalCycleTimeSeconds)
        Me.tabPlayground.Controls.Add(Me.txtEngineSpeed)
        Me.tabPlayground.Controls.Add(Me.txtEngineMotoringPower)
        Me.tabPlayground.Controls.Add(Me.txtEngineDrivelineTorque)
        Me.tabPlayground.Controls.Add(Me.txtEngineDrivelinePower)
        Me.tabPlayground.Location = New System.Drawing.Point(4, 22)
        Me.tabPlayground.Name = "tabPlayground"
        Me.tabPlayground.Size = New System.Drawing.Size(1470, 704)
        Me.tabPlayground.TabIndex = 4
        Me.tabPlayground.Text = "Playground"
        Me.tabPlayground.UseVisualStyleBackColor = true
        '
        'chkEngineStopped
        '
        Me.chkEngineStopped.AutoSize = true
        Me.chkEngineStopped.Location = New System.Drawing.Point(10, 144)
        Me.chkEngineStopped.Name = "chkEngineStopped"
        Me.chkEngineStopped.Size = New System.Drawing.Size(102, 17)
        Me.chkEngineStopped.TabIndex = 60
        Me.chkEngineStopped.Text = "Engine Stopped"
        Me.chkEngineStopped.UseVisualStyleBackColor = true
        '
        'pnlM13
        '
        Me.pnlM13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlM13.Controls.Add(Me.txtM13_out_TotalCycleFuelCalculationGramsLITRES)
        Me.pnlM13.Controls.Add(Me.lblM13TotalFuelConsumptionTotalCycleLitres)
        Me.pnlM13.Controls.Add(Me.lblM13_FinalFCOverTheCycle)
        Me.pnlM13.Controls.Add(Me.txtM13_out_TotalCycleFuelCalculationGramsGRAMS)
        Me.pnlM13.Location = New System.Drawing.Point(1157, 435)
        Me.pnlM13.Name = "pnlM13"
        Me.pnlM13.Size = New System.Drawing.Size(308, 120)
        Me.pnlM13.TabIndex = 59
        '
        'txtM13_out_TotalCycleFuelCalculationGramsLITRES
        '
        Me.txtM13_out_TotalCycleFuelCalculationGramsLITRES.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM13_out_TotalCycleFuelCalculationGramsLITRES.Location = New System.Drawing.Point(12, 80)
        Me.txtM13_out_TotalCycleFuelCalculationGramsLITRES.Name = "txtM13_out_TotalCycleFuelCalculationGramsLITRES"
        Me.txtM13_out_TotalCycleFuelCalculationGramsLITRES.Size = New System.Drawing.Size(100, 20)
        Me.txtM13_out_TotalCycleFuelCalculationGramsLITRES.TabIndex = 4
        '
        'lblM13TotalFuelConsumptionTotalCycleLitres
        '
        Me.lblM13TotalFuelConsumptionTotalCycleLitres.AutoSize = true
        Me.lblM13TotalFuelConsumptionTotalCycleLitres.Location = New System.Drawing.Point(6, 64)
        Me.lblM13TotalFuelConsumptionTotalCycleLitres.MaximumSize = New System.Drawing.Size(260, 0)
        Me.lblM13TotalFuelConsumptionTotalCycleLitres.Name = "lblM13TotalFuelConsumptionTotalCycleLitres"
        Me.lblM13TotalFuelConsumptionTotalCycleLitres.Size = New System.Drawing.Size(191, 13)
        Me.lblM13TotalFuelConsumptionTotalCycleLitres.TabIndex = 3
        Me.lblM13TotalFuelConsumptionTotalCycleLitres.Text = "Total Cycle Fuel Calculation ( LITRES )"
        '
        'lblM13_FinalFCOverTheCycle
        '
        Me.lblM13_FinalFCOverTheCycle.AutoSize = true
        Me.lblM13_FinalFCOverTheCycle.Location = New System.Drawing.Point(6, 12)
        Me.lblM13_FinalFCOverTheCycle.MaximumSize = New System.Drawing.Size(260, 0)
        Me.lblM13_FinalFCOverTheCycle.Name = "lblM13_FinalFCOverTheCycle"
        Me.lblM13_FinalFCOverTheCycle.Size = New System.Drawing.Size(192, 13)
        Me.lblM13_FinalFCOverTheCycle.TabIndex = 2
        Me.lblM13_FinalFCOverTheCycle.Text = "Total Cycle Fuel Calculation ( GRAMS )"
        '
        'txtM13_out_TotalCycleFuelCalculationGramsGRAMS
        '
        Me.txtM13_out_TotalCycleFuelCalculationGramsGRAMS.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM13_out_TotalCycleFuelCalculationGramsGRAMS.Location = New System.Drawing.Point(12, 33)
        Me.txtM13_out_TotalCycleFuelCalculationGramsGRAMS.Name = "txtM13_out_TotalCycleFuelCalculationGramsGRAMS"
        Me.txtM13_out_TotalCycleFuelCalculationGramsGRAMS.Size = New System.Drawing.Size(100, 20)
        Me.txtM13_out_TotalCycleFuelCalculationGramsGRAMS.TabIndex = 0
        '
        'lblM13_Title
        '
        Me.lblM13_Title.AutoSize = true
        Me.lblM13_Title.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblM13_Title.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lblM13_Title.Location = New System.Drawing.Point(1157, 419)
        Me.lblM13_Title.MaximumSize = New System.Drawing.Size(270, 0)
        Me.lblM13_Title.Name = "lblM13_Title"
        Me.lblM13_Title.Size = New System.Drawing.Size(240, 13)
        Me.lblM13_Title.TabIndex = 58
        Me.lblM13_Title.Text = "M13: Final FC calculation over the cycle "
        '
        'pnlM12
        '
        Me.pnlM12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlM12.Controls.Add(Me.lblM12_FuelConsumptionWithSmartElectricsAndAveragePneumaticPowerDemand)
        Me.pnlM12.Controls.Add(Me.txtM12_out_FuelConsumptionWithSmartElectricsAndAveragePneumaticPowerDemand)
        Me.pnlM12.Location = New System.Drawing.Point(1157, 325)
        Me.pnlM12.Name = "pnlM12"
        Me.pnlM12.Size = New System.Drawing.Size(308, 75)
        Me.pnlM12.TabIndex = 57
        '
        'lblM12_FuelConsumptionWithSmartElectricsAndAveragePneumaticPowerDemand
        '
        Me.lblM12_FuelConsumptionWithSmartElectricsAndAveragePneumaticPowerDemand.AutoSize = true
        Me.lblM12_FuelConsumptionWithSmartElectricsAndAveragePneumaticPowerDemand.Location = New System.Drawing.Point(6, 12)
        Me.lblM12_FuelConsumptionWithSmartElectricsAndAveragePneumaticPowerDemand.MaximumSize = New System.Drawing.Size(260, 0)
        Me.lblM12_FuelConsumptionWithSmartElectricsAndAveragePneumaticPowerDemand.Name = "lblM12_FuelConsumptionWithSmartElectricsAndAveragePneumaticPowerDemand"
        Me.lblM12_FuelConsumptionWithSmartElectricsAndAveragePneumaticPowerDemand.Size = New System.Drawing.Size(257, 26)
        Me.lblM12_FuelConsumptionWithSmartElectricsAndAveragePneumaticPowerDemand.TabIndex = 2
        Me.lblM12_FuelConsumptionWithSmartElectricsAndAveragePneumaticPowerDemand.Text = "Fuel Consumption With Smart Electrics And Average Pneumatic Power Demand"
        '
        'txtM12_out_FuelConsumptionWithSmartElectricsAndAveragePneumaticPowerDemand
        '
        Me.txtM12_out_FuelConsumptionWithSmartElectricsAndAveragePneumaticPowerDemand.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM12_out_FuelConsumptionWithSmartElectricsAndAveragePneumaticPowerDemand.Location = New System.Drawing.Point(8, 44)
        Me.txtM12_out_FuelConsumptionWithSmartElectricsAndAveragePneumaticPowerDemand.Name = "txtM12_out_FuelConsumptionWithSmartElectricsAndAveragePneumaticPowerDemand"
        Me.txtM12_out_FuelConsumptionWithSmartElectricsAndAveragePneumaticPowerDemand.Size = New System.Drawing.Size(100, 20)
        Me.txtM12_out_FuelConsumptionWithSmartElectricsAndAveragePneumaticPowerDemand.TabIndex = 0
        '
        'lblM12_Title
        '
        Me.lblM12_Title.AutoSize = true
        Me.lblM12_Title.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblM12_Title.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lblM12_Title.Location = New System.Drawing.Point(1155, 289)
        Me.lblM12_Title.MaximumSize = New System.Drawing.Size(270, 0)
        Me.lblM12_Title.Name = "lblM12_Title"
        Me.lblM12_Title.Size = New System.Drawing.Size(265, 26)
        Me.lblM12_Title.TabIndex = 56
        Me.lblM12_Title.Text = "M12  Actual FC calculation for true electrical demand for smart Electrics over th"& _ 
    "e cycle "
        '
        'mblM11_Title
        '
        Me.mblM11_Title.AutoSize = true
        Me.mblM11_Title.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.mblM11_Title.ForeColor = System.Drawing.SystemColors.Highlight
        Me.mblM11_Title.Location = New System.Drawing.Point(1152, 6)
        Me.mblM11_Title.MaximumSize = New System.Drawing.Size(270, 0)
        Me.mblM11_Title.Name = "mblM11_Title"
        Me.mblM11_Title.Size = New System.Drawing.Size(259, 26)
        Me.mblM11_Title.TabIndex = 55
        Me.mblM11_Title.Text = "M11 Electrical Energy FC Call for Smart and Non Smart Pneumatics over the cycle"
        '
        'pnlM11
        '
        Me.pnlM11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlM11.Controls.Add(Me.lblM11_TotalCycleFuelConsumptionZeroElectricalLoad)
        Me.pnlM11.Controls.Add(Me.txtM11_out_TotalCycleFuelConsumptionZeroElectricalLoad)
        Me.pnlM11.Controls.Add(Me.lblM11_TotalCycleFuelConsumptionSmartElectricalLoad)
        Me.pnlM11.Controls.Add(Me.txtM11_out_TotalCycleFuelConsumptionSmartElectricalLoad)
        Me.pnlM11.Controls.Add(Me.lblM11_TotalCycleElectricalDemand)
        Me.pnlM11.Controls.Add(Me.txtM11_out_TotalCycleElectricalDemand)
        Me.pnlM11.Controls.Add(Me.lblM11_SmartElectricalTotalCycleElectricalEnergyGenerated)
        Me.pnlM11.Controls.Add(Me.lblM11_TotalCycleElectricalEnergyGenOverrunOnly)
        Me.pnlM11.Controls.Add(Me.txtM11_out_SmartElectricalTotalCycleElectricalEnergyGenerated)
        Me.pnlM11.Controls.Add(Me.txtM11_out_TotalCycleElectricalEnergyGenOverrunOnly)
        Me.pnlM11.Location = New System.Drawing.Point(1153, 36)
        Me.pnlM11.Name = "pnlM11"
        Me.pnlM11.Size = New System.Drawing.Size(308, 244)
        Me.pnlM11.TabIndex = 54
        '
        'lblM11_TotalCycleFuelConsumptionZeroElectricalLoad
        '
        Me.lblM11_TotalCycleFuelConsumptionZeroElectricalLoad.AutoSize = true
        Me.lblM11_TotalCycleFuelConsumptionZeroElectricalLoad.Location = New System.Drawing.Point(9, 192)
        Me.lblM11_TotalCycleFuelConsumptionZeroElectricalLoad.Name = "lblM11_TotalCycleFuelConsumptionZeroElectricalLoad"
        Me.lblM11_TotalCycleFuelConsumptionZeroElectricalLoad.Size = New System.Drawing.Size(251, 13)
        Me.lblM11_TotalCycleFuelConsumptionZeroElectricalLoad.TabIndex = 10
        Me.lblM11_TotalCycleFuelConsumptionZeroElectricalLoad.Text = "Total Cycle : Fuel Consumption Zero Electrical Load"
        '
        'txtM11_out_TotalCycleFuelConsumptionZeroElectricalLoad
        '
        Me.txtM11_out_TotalCycleFuelConsumptionZeroElectricalLoad.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM11_out_TotalCycleFuelConsumptionZeroElectricalLoad.Location = New System.Drawing.Point(9, 208)
        Me.txtM11_out_TotalCycleFuelConsumptionZeroElectricalLoad.Name = "txtM11_out_TotalCycleFuelConsumptionZeroElectricalLoad"
        Me.txtM11_out_TotalCycleFuelConsumptionZeroElectricalLoad.Size = New System.Drawing.Size(100, 20)
        Me.txtM11_out_TotalCycleFuelConsumptionZeroElectricalLoad.TabIndex = 9
        '
        'lblM11_TotalCycleFuelConsumptionSmartElectricalLoad
        '
        Me.lblM11_TotalCycleFuelConsumptionSmartElectricalLoad.AutoSize = true
        Me.lblM11_TotalCycleFuelConsumptionSmartElectricalLoad.Location = New System.Drawing.Point(9, 149)
        Me.lblM11_TotalCycleFuelConsumptionSmartElectricalLoad.Name = "lblM11_TotalCycleFuelConsumptionSmartElectricalLoad"
        Me.lblM11_TotalCycleFuelConsumptionSmartElectricalLoad.Size = New System.Drawing.Size(256, 13)
        Me.lblM11_TotalCycleFuelConsumptionSmartElectricalLoad.TabIndex = 8
        Me.lblM11_TotalCycleFuelConsumptionSmartElectricalLoad.Text = "Total Cycle : Fuel Consumption Smart Electrical Load"
        '
        'txtM11_out_TotalCycleFuelConsumptionSmartElectricalLoad
        '
        Me.txtM11_out_TotalCycleFuelConsumptionSmartElectricalLoad.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM11_out_TotalCycleFuelConsumptionSmartElectricalLoad.Location = New System.Drawing.Point(9, 165)
        Me.txtM11_out_TotalCycleFuelConsumptionSmartElectricalLoad.Name = "txtM11_out_TotalCycleFuelConsumptionSmartElectricalLoad"
        Me.txtM11_out_TotalCycleFuelConsumptionSmartElectricalLoad.Size = New System.Drawing.Size(100, 20)
        Me.txtM11_out_TotalCycleFuelConsumptionSmartElectricalLoad.TabIndex = 7
        '
        'lblM11_TotalCycleElectricalDemand
        '
        Me.lblM11_TotalCycleElectricalDemand.AutoSize = true
        Me.lblM11_TotalCycleElectricalDemand.Location = New System.Drawing.Point(6, 102)
        Me.lblM11_TotalCycleElectricalDemand.Name = "lblM11_TotalCycleElectricalDemand"
        Me.lblM11_TotalCycleElectricalDemand.Size = New System.Drawing.Size(149, 13)
        Me.lblM11_TotalCycleElectricalDemand.TabIndex = 6
        Me.lblM11_TotalCycleElectricalDemand.Text = "Total Cycle Electrical Demand"
        '
        'txtM11_out_TotalCycleElectricalDemand
        '
        Me.txtM11_out_TotalCycleElectricalDemand.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM11_out_TotalCycleElectricalDemand.Location = New System.Drawing.Point(6, 118)
        Me.txtM11_out_TotalCycleElectricalDemand.Name = "txtM11_out_TotalCycleElectricalDemand"
        Me.txtM11_out_TotalCycleElectricalDemand.Size = New System.Drawing.Size(100, 20)
        Me.txtM11_out_TotalCycleElectricalDemand.TabIndex = 4
        '
        'lblM11_SmartElectricalTotalCycleElectricalEnergyGenerated
        '
        Me.lblM11_SmartElectricalTotalCycleElectricalEnergyGenerated.AutoSize = true
        Me.lblM11_SmartElectricalTotalCycleElectricalEnergyGenerated.Location = New System.Drawing.Point(6, 59)
        Me.lblM11_SmartElectricalTotalCycleElectricalEnergyGenerated.Name = "lblM11_SmartElectricalTotalCycleElectricalEnergyGenerated"
        Me.lblM11_SmartElectricalTotalCycleElectricalEnergyGenerated.Size = New System.Drawing.Size(274, 13)
        Me.lblM11_SmartElectricalTotalCycleElectricalEnergyGenerated.TabIndex = 3
        Me.lblM11_SmartElectricalTotalCycleElectricalEnergyGenerated.Text = "Smart Electrical : TotalCycle Electrical Energy Generated"
        '
        'lblM11_TotalCycleElectricalEnergyGenOverrunOnly
        '
        Me.lblM11_TotalCycleElectricalEnergyGenOverrunOnly.AutoSize = true
        Me.lblM11_TotalCycleElectricalEnergyGenOverrunOnly.Location = New System.Drawing.Point(6, 12)
        Me.lblM11_TotalCycleElectricalEnergyGenOverrunOnly.Name = "lblM11_TotalCycleElectricalEnergyGenOverrunOnly"
        Me.lblM11_TotalCycleElectricalEnergyGenOverrunOnly.Size = New System.Drawing.Size(269, 13)
        Me.lblM11_TotalCycleElectricalEnergyGenOverrunOnly.TabIndex = 2
        Me.lblM11_TotalCycleElectricalEnergyGenOverrunOnly.Text = "Total Cycle : Electrical Energy Generated  Overrun Only"
        '
        'txtM11_out_SmartElectricalTotalCycleElectricalEnergyGenerated
        '
        Me.txtM11_out_SmartElectricalTotalCycleElectricalEnergyGenerated.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM11_out_SmartElectricalTotalCycleElectricalEnergyGenerated.Location = New System.Drawing.Point(6, 75)
        Me.txtM11_out_SmartElectricalTotalCycleElectricalEnergyGenerated.Name = "txtM11_out_SmartElectricalTotalCycleElectricalEnergyGenerated"
        Me.txtM11_out_SmartElectricalTotalCycleElectricalEnergyGenerated.Size = New System.Drawing.Size(100, 20)
        Me.txtM11_out_SmartElectricalTotalCycleElectricalEnergyGenerated.TabIndex = 1
        '
        'txtM11_out_TotalCycleElectricalEnergyGenOverrunOnly
        '
        Me.txtM11_out_TotalCycleElectricalEnergyGenOverrunOnly.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM11_out_TotalCycleElectricalEnergyGenOverrunOnly.Location = New System.Drawing.Point(6, 36)
        Me.txtM11_out_TotalCycleElectricalEnergyGenOverrunOnly.Name = "txtM11_out_TotalCycleElectricalEnergyGenOverrunOnly"
        Me.txtM11_out_TotalCycleElectricalEnergyGenOverrunOnly.Size = New System.Drawing.Size(100, 20)
        Me.txtM11_out_TotalCycleElectricalEnergyGenOverrunOnly.TabIndex = 0
        '
        'chkSignalsSmartAirCompression
        '
        Me.chkSignalsSmartAirCompression.AutoSize = true
        Me.chkSignalsSmartAirCompression.Location = New System.Drawing.Point(10, 122)
        Me.chkSignalsSmartAirCompression.Name = "chkSignalsSmartAirCompression"
        Me.chkSignalsSmartAirCompression.Size = New System.Drawing.Size(131, 17)
        Me.chkSignalsSmartAirCompression.TabIndex = 53
        Me.chkSignalsSmartAirCompression.Text = "Smart Air Compression"
        Me.chkSignalsSmartAirCompression.UseVisualStyleBackColor = true
        '
        'chkSignalsSmartElectrics
        '
        Me.chkSignalsSmartElectrics.AutoSize = true
        Me.chkSignalsSmartElectrics.Location = New System.Drawing.Point(11, 98)
        Me.chkSignalsSmartElectrics.Name = "chkSignalsSmartElectrics"
        Me.chkSignalsSmartElectrics.Size = New System.Drawing.Size(96, 17)
        Me.chkSignalsSmartElectrics.TabIndex = 52
        Me.chkSignalsSmartElectrics.Text = "Smart Electrics"
        Me.chkSignalsSmartElectrics.UseVisualStyleBackColor = true
        '
        'pnlM10
        '
        Me.pnlM10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlM10.Controls.Add(Me.lblM10_FuelConsumptionWithSmartPneumaticsAndAverageElectricalPowerDemand)
        Me.pnlM10.Controls.Add(Me.lblM10_BaseFuelConsumptionAithAverageAuxLoads)
        Me.pnlM10.Controls.Add(Me.txtM10_out_FCWithSmartPSAndAvgElecPowerDemand)
        Me.pnlM10.Controls.Add(Me.txtM10_out_BaseFCWithAvgAuxLoads)
        Me.pnlM10.Location = New System.Drawing.Point(837, 447)
        Me.pnlM10.Name = "pnlM10"
        Me.pnlM10.Size = New System.Drawing.Size(308, 108)
        Me.pnlM10.TabIndex = 50
        '
        'lblM10_FuelConsumptionWithSmartPneumaticsAndAverageElectricalPowerDemand
        '
        Me.lblM10_FuelConsumptionWithSmartPneumaticsAndAverageElectricalPowerDemand.AutoSize = true
        Me.lblM10_FuelConsumptionWithSmartPneumaticsAndAverageElectricalPowerDemand.Location = New System.Drawing.Point(6, 48)
        Me.lblM10_FuelConsumptionWithSmartPneumaticsAndAverageElectricalPowerDemand.MaximumSize = New System.Drawing.Size(300, 0)
        Me.lblM10_FuelConsumptionWithSmartPneumaticsAndAverageElectricalPowerDemand.Name = "lblM10_FuelConsumptionWithSmartPneumaticsAndAverageElectricalPowerDemand"
        Me.lblM10_FuelConsumptionWithSmartPneumaticsAndAverageElectricalPowerDemand.Size = New System.Drawing.Size(267, 26)
        Me.lblM10_FuelConsumptionWithSmartPneumaticsAndAverageElectricalPowerDemand.TabIndex = 3
        Me.lblM10_FuelConsumptionWithSmartPneumaticsAndAverageElectricalPowerDemand.Text = "Fuel Consumption with Smart Pneumatics and average Electrical Power Demand"
        '
        'lblM10_BaseFuelConsumptionAithAverageAuxLoads
        '
        Me.lblM10_BaseFuelConsumptionAithAverageAuxLoads.AutoSize = true
        Me.lblM10_BaseFuelConsumptionAithAverageAuxLoads.Location = New System.Drawing.Point(6, 9)
        Me.lblM10_BaseFuelConsumptionAithAverageAuxLoads.Name = "lblM10_BaseFuelConsumptionAithAverageAuxLoads"
        Me.lblM10_BaseFuelConsumptionAithAverageAuxLoads.Size = New System.Drawing.Size(231, 13)
        Me.lblM10_BaseFuelConsumptionAithAverageAuxLoads.TabIndex = 2
        Me.lblM10_BaseFuelConsumptionAithAverageAuxLoads.Text = "Base Fuel Consumption with average Aux loads"
        '
        'txtM10_out_FCWithSmartPSAndAvgElecPowerDemand
        '
        Me.txtM10_out_FCWithSmartPSAndAvgElecPowerDemand.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM10_out_FCWithSmartPSAndAvgElecPowerDemand.Location = New System.Drawing.Point(6, 77)
        Me.txtM10_out_FCWithSmartPSAndAvgElecPowerDemand.Name = "txtM10_out_FCWithSmartPSAndAvgElecPowerDemand"
        Me.txtM10_out_FCWithSmartPSAndAvgElecPowerDemand.Size = New System.Drawing.Size(100, 20)
        Me.txtM10_out_FCWithSmartPSAndAvgElecPowerDemand.TabIndex = 1
        '
        'txtM10_out_BaseFCWithAvgAuxLoads
        '
        Me.txtM10_out_BaseFCWithAvgAuxLoads.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM10_out_BaseFCWithAvgAuxLoads.Location = New System.Drawing.Point(6, 25)
        Me.txtM10_out_BaseFCWithAvgAuxLoads.Name = "txtM10_out_BaseFCWithAvgAuxLoads"
        Me.txtM10_out_BaseFCWithAvgAuxLoads.Size = New System.Drawing.Size(100, 20)
        Me.txtM10_out_BaseFCWithAvgAuxLoads.TabIndex = 0
        '
        'lblM10_Title
        '
        Me.lblM10_Title.AutoSize = true
        Me.lblM10_Title.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblM10_Title.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lblM10_Title.Location = New System.Drawing.Point(838, 405)
        Me.lblM10_Title.MaximumSize = New System.Drawing.Size(250, 0)
        Me.lblM10_Title.Name = "lblM10_Title"
        Me.lblM10_Title.Size = New System.Drawing.Size(247, 39)
        Me.lblM10_Title.TabIndex = 51
        Me.lblM10_Title.Text = "M10 - Actual FC Calc 4 True Air Delivery for smart and non Smart Pneunmatics over"& _ 
    " the cycle"
        '
        'lblM9Title
        '
        Me.lblM9Title.AutoSize = true
        Me.lblM9Title.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblM9Title.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lblM9Title.Location = New System.Drawing.Point(831, 190)
        Me.lblM9Title.MaximumSize = New System.Drawing.Size(250, 0)
        Me.lblM9Title.Name = "lblM9Title"
        Me.lblM9Title.Size = New System.Drawing.Size(248, 26)
        Me.lblM9Title.TabIndex = 50
        Me.lblM9Title.Text = "M9-Air Delivery & Fuel Calculation Smart And Non Smart Pneumatics Over the cycle"
        '
        'pnlM9
        '
        Me.pnlM9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlM9.Controls.Add(Me.lblM9TotalCycleFuelConsumptionCompressorOFFContinuously)
        Me.pnlM9.Controls.Add(Me.txtM9_out_TotalCycleFuelConsumptionCompressorOFFContinuously)
        Me.pnlM9.Controls.Add(Me.lblM9TotalCycleFuelConsumptionCompressorOnContinuously)
        Me.pnlM9.Controls.Add(Me.txtM9_out_TotalCycleFuelConsumptionCompressorOnContinuously)
        Me.pnlM9.Controls.Add(Me.lblM9LitresOfAirCompressoryOnlyOnInOverrun)
        Me.pnlM9.Controls.Add(Me.lblM9LitresOfAirCompressorOnContinuously)
        Me.pnlM9.Controls.Add(Me.txtM9_out_LitresOfAirCompressorOnlyOnInOverrun)
        Me.pnlM9.Controls.Add(Me.txtM9_out_LitresOfAirConsumptionCompressorONContinuously)
        Me.pnlM9.Location = New System.Drawing.Point(837, 224)
        Me.pnlM9.Name = "pnlM9"
        Me.pnlM9.Size = New System.Drawing.Size(308, 176)
        Me.pnlM9.TabIndex = 49
        '
        'lblM9TotalCycleFuelConsumptionCompressorOFFContinuously
        '
        Me.lblM9TotalCycleFuelConsumptionCompressorOFFContinuously.AutoSize = true
        Me.lblM9TotalCycleFuelConsumptionCompressorOFFContinuously.Location = New System.Drawing.Point(6, 134)
        Me.lblM9TotalCycleFuelConsumptionCompressorOFFContinuously.Name = "lblM9TotalCycleFuelConsumptionCompressorOFFContinuously"
        Me.lblM9TotalCycleFuelConsumptionCompressorOFFContinuously.Size = New System.Drawing.Size(297, 13)
        Me.lblM9TotalCycleFuelConsumptionCompressorOFFContinuously.TabIndex = 8
        Me.lblM9TotalCycleFuelConsumptionCompressorOFFContinuously.Text = "Total Cycle Fuel Consumption : Compressor OFF Continuously"
        '
        'txtM9_out_TotalCycleFuelConsumptionCompressorOFFContinuously
        '
        Me.txtM9_out_TotalCycleFuelConsumptionCompressorOFFContinuously.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM9_out_TotalCycleFuelConsumptionCompressorOFFContinuously.Location = New System.Drawing.Point(6, 150)
        Me.txtM9_out_TotalCycleFuelConsumptionCompressorOFFContinuously.Name = "txtM9_out_TotalCycleFuelConsumptionCompressorOFFContinuously"
        Me.txtM9_out_TotalCycleFuelConsumptionCompressorOFFContinuously.Size = New System.Drawing.Size(100, 20)
        Me.txtM9_out_TotalCycleFuelConsumptionCompressorOFFContinuously.TabIndex = 7
        '
        'lblM9TotalCycleFuelConsumptionCompressorOnContinuously
        '
        Me.lblM9TotalCycleFuelConsumptionCompressorOnContinuously.AutoSize = true
        Me.lblM9TotalCycleFuelConsumptionCompressorOnContinuously.Location = New System.Drawing.Point(6, 91)
        Me.lblM9TotalCycleFuelConsumptionCompressorOnContinuously.Name = "lblM9TotalCycleFuelConsumptionCompressorOnContinuously"
        Me.lblM9TotalCycleFuelConsumptionCompressorOnContinuously.Size = New System.Drawing.Size(291, 13)
        Me.lblM9TotalCycleFuelConsumptionCompressorOnContinuously.TabIndex = 6
        Me.lblM9TotalCycleFuelConsumptionCompressorOnContinuously.Text = "Total Cycle Fuel Consumption : Compressor On Continuously"
        '
        'txtM9_out_TotalCycleFuelConsumptionCompressorOnContinuously
        '
        Me.txtM9_out_TotalCycleFuelConsumptionCompressorOnContinuously.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM9_out_TotalCycleFuelConsumptionCompressorOnContinuously.Location = New System.Drawing.Point(6, 107)
        Me.txtM9_out_TotalCycleFuelConsumptionCompressorOnContinuously.Name = "txtM9_out_TotalCycleFuelConsumptionCompressorOnContinuously"
        Me.txtM9_out_TotalCycleFuelConsumptionCompressorOnContinuously.Size = New System.Drawing.Size(100, 20)
        Me.txtM9_out_TotalCycleFuelConsumptionCompressorOnContinuously.TabIndex = 4
        '
        'lblM9LitresOfAirCompressoryOnlyOnInOverrun
        '
        Me.lblM9LitresOfAirCompressoryOnlyOnInOverrun.AutoSize = true
        Me.lblM9LitresOfAirCompressoryOnlyOnInOverrun.Location = New System.Drawing.Point(6, 48)
        Me.lblM9LitresOfAirCompressoryOnlyOnInOverrun.Name = "lblM9LitresOfAirCompressoryOnlyOnInOverrun"
        Me.lblM9LitresOfAirCompressoryOnlyOnInOverrun.Size = New System.Drawing.Size(219, 13)
        Me.lblM9LitresOfAirCompressoryOnlyOnInOverrun.TabIndex = 3
        Me.lblM9LitresOfAirCompressoryOnlyOnInOverrun.Text = "Litres Of Air : Compressor Only On In Overrun"
        '
        'lblM9LitresOfAirCompressorOnContinuously
        '
        Me.lblM9LitresOfAirCompressorOnContinuously.AutoSize = true
        Me.lblM9LitresOfAirCompressorOnContinuously.Location = New System.Drawing.Point(6, 9)
        Me.lblM9LitresOfAirCompressorOnContinuously.Name = "lblM9LitresOfAirCompressorOnContinuously"
        Me.lblM9LitresOfAirCompressorOnContinuously.Size = New System.Drawing.Size(205, 13)
        Me.lblM9LitresOfAirCompressorOnContinuously.TabIndex = 2
        Me.lblM9LitresOfAirCompressorOnContinuously.Text = "Litres Of Air : Compressor On Continuously"
        '
        'txtM9_out_LitresOfAirCompressorOnlyOnInOverrun
        '
        Me.txtM9_out_LitresOfAirCompressorOnlyOnInOverrun.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM9_out_LitresOfAirCompressorOnlyOnInOverrun.Location = New System.Drawing.Point(6, 64)
        Me.txtM9_out_LitresOfAirCompressorOnlyOnInOverrun.Name = "txtM9_out_LitresOfAirCompressorOnlyOnInOverrun"
        Me.txtM9_out_LitresOfAirCompressorOnlyOnInOverrun.Size = New System.Drawing.Size(100, 20)
        Me.txtM9_out_LitresOfAirCompressorOnlyOnInOverrun.TabIndex = 1
        '
        'txtM9_out_LitresOfAirConsumptionCompressorONContinuously
        '
        Me.txtM9_out_LitresOfAirConsumptionCompressorONContinuously.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM9_out_LitresOfAirConsumptionCompressorONContinuously.Location = New System.Drawing.Point(6, 25)
        Me.txtM9_out_LitresOfAirConsumptionCompressorONContinuously.Name = "txtM9_out_LitresOfAirConsumptionCompressorONContinuously"
        Me.txtM9_out_LitresOfAirConsumptionCompressorONContinuously.Size = New System.Drawing.Size(100, 20)
        Me.txtM9_out_LitresOfAirConsumptionCompressorONContinuously.TabIndex = 0
        '
        'lblM8_Title
        '
        Me.lblM8_Title.AutoSize = true
        Me.lblM8_Title.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblM8_Title.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lblM8_Title.Location = New System.Drawing.Point(838, 8)
        Me.lblM8_Title.Name = "lblM8_Title"
        Me.lblM8_Title.Size = New System.Drawing.Size(194, 13)
        Me.lblM8_Title.TabIndex = 48
        Me.lblM8_Title.Text = "M8-Full Assignment of Aux Loads"
        '
        'pnlM8
        '
        Me.pnlM8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlM8.Controls.Add(Me.lblM8CompressorFlag)
        Me.pnlM8.Controls.Add(Me.txtM8_out_CompressorFlag)
        Me.pnlM8.Controls.Add(Me.lblM8SmartElectricalAltPwrGenAtCrank)
        Me.pnlM8.Controls.Add(Me.lblM8AuxPowerAtCrankFromAllAncillaries)
        Me.pnlM8.Controls.Add(Me.txtM8_out_SmartElectricalAltPwrGenAtCrank)
        Me.pnlM8.Controls.Add(Me.txtM8_out_AuxPowerAtCrankFromAllAncillaries)
        Me.pnlM8.Location = New System.Drawing.Point(834, 36)
        Me.pnlM8.Name = "pnlM8"
        Me.pnlM8.Size = New System.Drawing.Size(308, 147)
        Me.pnlM8.TabIndex = 47
        '
        'lblM8CompressorFlag
        '
        Me.lblM8CompressorFlag.AutoSize = true
        Me.lblM8CompressorFlag.Location = New System.Drawing.Point(6, 91)
        Me.lblM8CompressorFlag.Name = "lblM8CompressorFlag"
        Me.lblM8CompressorFlag.Size = New System.Drawing.Size(85, 13)
        Me.lblM8CompressorFlag.TabIndex = 6
        Me.lblM8CompressorFlag.Text = "Compressor Flag"
        '
        'txtM8_out_CompressorFlag
        '
        Me.txtM8_out_CompressorFlag.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM8_out_CompressorFlag.Location = New System.Drawing.Point(6, 107)
        Me.txtM8_out_CompressorFlag.Name = "txtM8_out_CompressorFlag"
        Me.txtM8_out_CompressorFlag.Size = New System.Drawing.Size(100, 20)
        Me.txtM8_out_CompressorFlag.TabIndex = 4
        '
        'lblM8SmartElectricalAltPwrGenAtCrank
        '
        Me.lblM8SmartElectricalAltPwrGenAtCrank.AutoSize = true
        Me.lblM8SmartElectricalAltPwrGenAtCrank.Location = New System.Drawing.Point(6, 48)
        Me.lblM8SmartElectricalAltPwrGenAtCrank.Name = "lblM8SmartElectricalAltPwrGenAtCrank"
        Me.lblM8SmartElectricalAltPwrGenAtCrank.Size = New System.Drawing.Size(165, 13)
        Me.lblM8SmartElectricalAltPwrGenAtCrank.TabIndex = 3
        Me.lblM8SmartElectricalAltPwrGenAtCrank.Text = "Smart Elec Alt PowerGen@Crank"
        '
        'lblM8AuxPowerAtCrankFromAllAncillaries
        '
        Me.lblM8AuxPowerAtCrankFromAllAncillaries.AutoSize = true
        Me.lblM8AuxPowerAtCrankFromAllAncillaries.Location = New System.Drawing.Point(6, 9)
        Me.lblM8AuxPowerAtCrankFromAllAncillaries.Name = "lblM8AuxPowerAtCrankFromAllAncillaries"
        Me.lblM8AuxPowerAtCrankFromAllAncillaries.Size = New System.Drawing.Size(292, 13)
        Me.lblM8AuxPowerAtCrankFromAllAncillaries.TabIndex = 2
        Me.lblM8AuxPowerAtCrankFromAllAncillaries.Text = "Aux pwr@CrankFrom Elec,HVAC and Pneumatics Ancillaries"
        '
        'txtM8_out_SmartElectricalAltPwrGenAtCrank
        '
        Me.txtM8_out_SmartElectricalAltPwrGenAtCrank.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM8_out_SmartElectricalAltPwrGenAtCrank.Location = New System.Drawing.Point(6, 64)
        Me.txtM8_out_SmartElectricalAltPwrGenAtCrank.Name = "txtM8_out_SmartElectricalAltPwrGenAtCrank"
        Me.txtM8_out_SmartElectricalAltPwrGenAtCrank.Size = New System.Drawing.Size(100, 20)
        Me.txtM8_out_SmartElectricalAltPwrGenAtCrank.TabIndex = 1
        '
        'txtM8_out_AuxPowerAtCrankFromAllAncillaries
        '
        Me.txtM8_out_AuxPowerAtCrankFromAllAncillaries.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM8_out_AuxPowerAtCrankFromAllAncillaries.Location = New System.Drawing.Point(6, 25)
        Me.txtM8_out_AuxPowerAtCrankFromAllAncillaries.Name = "txtM8_out_AuxPowerAtCrankFromAllAncillaries"
        Me.txtM8_out_AuxPowerAtCrankFromAllAncillaries.Size = New System.Drawing.Size(100, 20)
        Me.txtM8_out_AuxPowerAtCrankFromAllAncillaries.TabIndex = 0
        '
        'lblM7_Title
        '
        Me.lblM7_Title.AutoSize = true
        Me.lblM7_Title.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblM7_Title.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lblM7_Title.Location = New System.Drawing.Point(529, 453)
        Me.lblM7_Title.MaximumSize = New System.Drawing.Size(300, 0)
        Me.lblM7_Title.Name = "lblM7_Title"
        Me.lblM7_Title.Size = New System.Drawing.Size(272, 26)
        Me.lblM7_Title.TabIndex = 46
        Me.lblM7_Title.Text = "M7-Full Cycle Definition of alt and Compressor Loads 54 Smart Systems"
        '
        'Panel4
        '
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.lblM7_SmartPneumaticsOnlyAux_AirCompPwrRegenAtCrank)
        Me.Panel4.Controls.Add(Me.lblM7_SmartElecOnly_AltPwrGenAtCrank)
        Me.Panel4.Controls.Add(Me.txtM7_out_SmartPneumaticsOnlyAux_AirCompPwrRegenAtCrank)
        Me.Panel4.Controls.Add(Me.txtM7_out_SmartElecOnlyAux_AltPwrGenAtCrank)
        Me.Panel4.Controls.Add(Me.lblM7_SmartElectricalAndPneumaticAux_AirCompPowerGenAtCrank)
        Me.Panel4.Controls.Add(Me.lblM7_SmarElectricalAndPneumaticsAux_AltPowerGenAtCrank)
        Me.Panel4.Controls.Add(Me.txtM7_out_SmartElectricalAndPneumaticAux_AirCompPowerGenAtCrank)
        Me.Panel4.Controls.Add(Me.txtM7_out_SmartElectricalAndPneumaticsAux_AltPowerGenAtCrank)
        Me.Panel4.Location = New System.Drawing.Point(532, 484)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(296, 179)
        Me.Panel4.TabIndex = 45
        '
        'lblM7_SmartPneumaticsOnlyAux_AirCompPwrRegenAtCrank
        '
        Me.lblM7_SmartPneumaticsOnlyAux_AirCompPwrRegenAtCrank.AutoSize = true
        Me.lblM7_SmartPneumaticsOnlyAux_AirCompPwrRegenAtCrank.Location = New System.Drawing.Point(6, 134)
        Me.lblM7_SmartPneumaticsOnlyAux_AirCompPwrRegenAtCrank.Name = "lblM7_SmartPneumaticsOnlyAux_AirCompPwrRegenAtCrank"
        Me.lblM7_SmartPneumaticsOnlyAux_AirCompPwrRegenAtCrank.Size = New System.Drawing.Size(282, 13)
        Me.lblM7_SmartPneumaticsOnlyAux_AirCompPwrRegenAtCrank.TabIndex = 7
        Me.lblM7_SmartPneumaticsOnlyAux_AirCompPwrRegenAtCrank.Text = "Smart Pneumatics Only Aux : Air Comp Pwr RegenAtCrank"
        '
        'lblM7_SmartElecOnly_AltPwrGenAtCrank
        '
        Me.lblM7_SmartElecOnly_AltPwrGenAtCrank.AutoSize = true
        Me.lblM7_SmartElecOnly_AltPwrGenAtCrank.Location = New System.Drawing.Point(6, 91)
        Me.lblM7_SmartElecOnly_AltPwrGenAtCrank.Name = "lblM7_SmartElecOnly_AltPwrGenAtCrank"
        Me.lblM7_SmartElecOnly_AltPwrGenAtCrank.Size = New System.Drawing.Size(207, 13)
        Me.lblM7_SmartElecOnly_AltPwrGenAtCrank.TabIndex = 6
        Me.lblM7_SmartElecOnly_AltPwrGenAtCrank.Text = "Smart Elec Only Aux : Alt Pwr Gen@Crank"
        '
        'txtM7_out_SmartPneumaticsOnlyAux_AirCompPwrRegenAtCrank
        '
        Me.txtM7_out_SmartPneumaticsOnlyAux_AirCompPwrRegenAtCrank.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM7_out_SmartPneumaticsOnlyAux_AirCompPwrRegenAtCrank.Location = New System.Drawing.Point(6, 151)
        Me.txtM7_out_SmartPneumaticsOnlyAux_AirCompPwrRegenAtCrank.Name = "txtM7_out_SmartPneumaticsOnlyAux_AirCompPwrRegenAtCrank"
        Me.txtM7_out_SmartPneumaticsOnlyAux_AirCompPwrRegenAtCrank.Size = New System.Drawing.Size(100, 20)
        Me.txtM7_out_SmartPneumaticsOnlyAux_AirCompPwrRegenAtCrank.TabIndex = 5
        '
        'txtM7_out_SmartElecOnlyAux_AltPwrGenAtCrank
        '
        Me.txtM7_out_SmartElecOnlyAux_AltPwrGenAtCrank.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM7_out_SmartElecOnlyAux_AltPwrGenAtCrank.Location = New System.Drawing.Point(6, 107)
        Me.txtM7_out_SmartElecOnlyAux_AltPwrGenAtCrank.Name = "txtM7_out_SmartElecOnlyAux_AltPwrGenAtCrank"
        Me.txtM7_out_SmartElecOnlyAux_AltPwrGenAtCrank.Size = New System.Drawing.Size(100, 20)
        Me.txtM7_out_SmartElecOnlyAux_AltPwrGenAtCrank.TabIndex = 4
        '
        'lblM7_SmartElectricalAndPneumaticAux_AirCompPowerGenAtCrank
        '
        Me.lblM7_SmartElectricalAndPneumaticAux_AirCompPowerGenAtCrank.AutoSize = true
        Me.lblM7_SmartElectricalAndPneumaticAux_AirCompPowerGenAtCrank.Location = New System.Drawing.Point(6, 48)
        Me.lblM7_SmartElectricalAndPneumaticAux_AirCompPowerGenAtCrank.Name = "lblM7_SmartElectricalAndPneumaticAux_AirCompPowerGenAtCrank"
        Me.lblM7_SmartElectricalAndPneumaticAux_AirCompPowerGenAtCrank.Size = New System.Drawing.Size(287, 13)
        Me.lblM7_SmartElectricalAndPneumaticAux_AirCompPowerGenAtCrank.TabIndex = 3
        Me.lblM7_SmartElectricalAndPneumaticAux_AirCompPowerGenAtCrank.Text = "Smart Elec and Pneumatic Aux : Air Comp Pwr Gen@Crank"
        '
        'lblM7_SmarElectricalAndPneumaticsAux_AltPowerGenAtCrank
        '
        Me.lblM7_SmarElectricalAndPneumaticsAux_AltPowerGenAtCrank.AutoSize = true
        Me.lblM7_SmarElectricalAndPneumaticsAux_AltPowerGenAtCrank.Location = New System.Drawing.Point(6, 9)
        Me.lblM7_SmarElectricalAndPneumaticsAux_AltPowerGenAtCrank.Name = "lblM7_SmarElectricalAndPneumaticsAux_AltPowerGenAtCrank"
        Me.lblM7_SmarElectricalAndPneumaticsAux_AltPowerGenAtCrank.Size = New System.Drawing.Size(262, 13)
        Me.lblM7_SmarElectricalAndPneumaticsAux_AltPowerGenAtCrank.TabIndex = 2
        Me.lblM7_SmarElectricalAndPneumaticsAux_AltPowerGenAtCrank.Text = "Smart Elec and Pneumatics Aux : Alt Pwr Gen@Crank"
        '
        'txtM7_out_SmartElectricalAndPneumaticAux_AirCompPowerGenAtCrank
        '
        Me.txtM7_out_SmartElectricalAndPneumaticAux_AirCompPowerGenAtCrank.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM7_out_SmartElectricalAndPneumaticAux_AirCompPowerGenAtCrank.Location = New System.Drawing.Point(6, 64)
        Me.txtM7_out_SmartElectricalAndPneumaticAux_AirCompPowerGenAtCrank.Name = "txtM7_out_SmartElectricalAndPneumaticAux_AirCompPowerGenAtCrank"
        Me.txtM7_out_SmartElectricalAndPneumaticAux_AirCompPowerGenAtCrank.Size = New System.Drawing.Size(100, 20)
        Me.txtM7_out_SmartElectricalAndPneumaticAux_AirCompPowerGenAtCrank.TabIndex = 1
        '
        'txtM7_out_SmartElectricalAndPneumaticsAux_AltPowerGenAtCrank
        '
        Me.txtM7_out_SmartElectricalAndPneumaticsAux_AltPowerGenAtCrank.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM7_out_SmartElectricalAndPneumaticsAux_AltPowerGenAtCrank.Location = New System.Drawing.Point(6, 25)
        Me.txtM7_out_SmartElectricalAndPneumaticsAux_AltPowerGenAtCrank.Name = "txtM7_out_SmartElectricalAndPneumaticsAux_AltPowerGenAtCrank"
        Me.txtM7_out_SmartElectricalAndPneumaticsAux_AltPowerGenAtCrank.Size = New System.Drawing.Size(100, 20)
        Me.txtM7_out_SmartElectricalAndPneumaticsAux_AltPowerGenAtCrank.TabIndex = 0
        '
        'lblM6Title
        '
        Me.lblM6Title.AutoSize = true
        Me.lblM6Title.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblM6Title.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lblM6Title.Location = New System.Drawing.Point(529, 8)
        Me.lblM6Title.MaximumSize = New System.Drawing.Size(250, 0)
        Me.lblM6Title.Name = "lblM6Title"
        Me.lblM6Title.Size = New System.Drawing.Size(235, 26)
        Me.lblM6Title.TabIndex = 44
        Me.lblM6Title.Text = "M6-OVER-RUN smart/non-smart and alt and air comp load calcs"
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.lblM6_SmartPneumaticsOnlyCompressorFlag)
        Me.Panel2.Controls.Add(Me.txtM6_out_SmartPneumaticsOnlyCompressorFlag)
        Me.Panel2.Controls.Add(Me.lblM6_AveragePowerDemandAtCrankFromElectricsIncHVAC)
        Me.Panel2.Controls.Add(Me.lblM6_SmartPneumaticOnlyAirCompPowerGenAtCrank)
        Me.Panel2.Controls.Add(Me.txtM6_out_AveragePowerDemandAtCrankFromElectricsIncHVAC)
        Me.Panel2.Controls.Add(Me.txtM6_out_SmartPneumaticOnlyAirCompPowerGenAtCrank)
        Me.Panel2.Controls.Add(Me.lblM6_AveragePowerDemandAtCrankFromPneumatics)
        Me.Panel2.Controls.Add(Me.lblM6_SmarElectricalOnlyAltPowerGenAtCrank)
        Me.Panel2.Controls.Add(Me.txtM6_out_AveragePowerDemandAtCrankFromPneumatics)
        Me.Panel2.Controls.Add(Me.txtM6_out_SmarElectricalOnlyAltPowerGenAtCrank)
        Me.Panel2.Controls.Add(Me.lblM6_SmartElectricalAndPneumaticAirCompPowerGenAtCrank)
        Me.Panel2.Controls.Add(Me.lblM6_SmartElectriclAndPneumaticsAltPowerGenAtCrank)
        Me.Panel2.Controls.Add(Me.txtM6_out_SmartElectricalAndPneumaticAirCompPowerGenAtCrank)
        Me.Panel2.Controls.Add(Me.txtM6_out_SmartElectriclAndPneumaticsAltPowerGenAtCrank)
        Me.Panel2.Controls.Add(Me.lblM6_SmartElectricalAndPneumaticsCompressorFlag)
        Me.Panel2.Controls.Add(Me.lblM6_OverrunFlag)
        Me.Panel2.Controls.Add(Me.txtM6_out_SmartElectricalAndPneumaticsCompressorFlag)
        Me.Panel2.Controls.Add(Me.txtM6_out_OverrunFlag)
        Me.Panel2.Location = New System.Drawing.Point(531, 35)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(297, 402)
        Me.Panel2.TabIndex = 43
        '
        'lblM6_SmartPneumaticsOnlyCompressorFlag
        '
        Me.lblM6_SmartPneumaticsOnlyCompressorFlag.AutoSize = true
        Me.lblM6_SmartPneumaticsOnlyCompressorFlag.Location = New System.Drawing.Point(6, 352)
        Me.lblM6_SmartPneumaticsOnlyCompressorFlag.Name = "lblM6_SmartPneumaticsOnlyCompressorFlag"
        Me.lblM6_SmartPneumaticsOnlyCompressorFlag.Size = New System.Drawing.Size(203, 13)
        Me.lblM6_SmartPneumaticsOnlyCompressorFlag.TabIndex = 18
        Me.lblM6_SmartPneumaticsOnlyCompressorFlag.Text = "Smart Pneumatics Only : Compressor Flag"
        '
        'txtM6_out_SmartPneumaticsOnlyCompressorFlag
        '
        Me.txtM6_out_SmartPneumaticsOnlyCompressorFlag.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM6_out_SmartPneumaticsOnlyCompressorFlag.Location = New System.Drawing.Point(6, 369)
        Me.txtM6_out_SmartPneumaticsOnlyCompressorFlag.Name = "txtM6_out_SmartPneumaticsOnlyCompressorFlag"
        Me.txtM6_out_SmartPneumaticsOnlyCompressorFlag.Size = New System.Drawing.Size(100, 20)
        Me.txtM6_out_SmartPneumaticsOnlyCompressorFlag.TabIndex = 16
        '
        'lblM6_AveragePowerDemandAtCrankFromElectricsIncHVAC
        '
        Me.lblM6_AveragePowerDemandAtCrankFromElectricsIncHVAC.AutoSize = true
        Me.lblM6_AveragePowerDemandAtCrankFromElectricsIncHVAC.Location = New System.Drawing.Point(6, 307)
        Me.lblM6_AveragePowerDemandAtCrankFromElectricsIncHVAC.Name = "lblM6_AveragePowerDemandAtCrankFromElectricsIncHVAC"
        Me.lblM6_AveragePowerDemandAtCrankFromElectricsIncHVAC.Size = New System.Drawing.Size(230, 13)
        Me.lblM6_AveragePowerDemandAtCrankFromElectricsIncHVAC.TabIndex = 15
        Me.lblM6_AveragePowerDemandAtCrankFromElectricsIncHVAC.Text = "Avg Pwr Dmd@Crank From Electrics Inc HVAC"
        '
        'lblM6_SmartPneumaticOnlyAirCompPowerGenAtCrank
        '
        Me.lblM6_SmartPneumaticOnlyAirCompPowerGenAtCrank.AutoSize = true
        Me.lblM6_SmartPneumaticOnlyAirCompPowerGenAtCrank.Location = New System.Drawing.Point(6, 262)
        Me.lblM6_SmartPneumaticOnlyAirCompPowerGenAtCrank.Name = "lblM6_SmartPneumaticOnlyAirCompPowerGenAtCrank"
        Me.lblM6_SmartPneumaticOnlyAirCompPowerGenAtCrank.Size = New System.Drawing.Size(245, 13)
        Me.lblM6_SmartPneumaticOnlyAirCompPowerGenAtCrank.TabIndex = 14
        Me.lblM6_SmartPneumaticOnlyAirCompPowerGenAtCrank.Text = "Smart Pneumatic Only : Air Comp Pwr Gen@Crank"
        '
        'txtM6_out_AveragePowerDemandAtCrankFromElectricsIncHVAC
        '
        Me.txtM6_out_AveragePowerDemandAtCrankFromElectricsIncHVAC.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM6_out_AveragePowerDemandAtCrankFromElectricsIncHVAC.Location = New System.Drawing.Point(6, 323)
        Me.txtM6_out_AveragePowerDemandAtCrankFromElectricsIncHVAC.Name = "txtM6_out_AveragePowerDemandAtCrankFromElectricsIncHVAC"
        Me.txtM6_out_AveragePowerDemandAtCrankFromElectricsIncHVAC.Size = New System.Drawing.Size(100, 20)
        Me.txtM6_out_AveragePowerDemandAtCrankFromElectricsIncHVAC.TabIndex = 13
        '
        'txtM6_out_SmartPneumaticOnlyAirCompPowerGenAtCrank
        '
        Me.txtM6_out_SmartPneumaticOnlyAirCompPowerGenAtCrank.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM6_out_SmartPneumaticOnlyAirCompPowerGenAtCrank.Location = New System.Drawing.Point(6, 278)
        Me.txtM6_out_SmartPneumaticOnlyAirCompPowerGenAtCrank.Name = "txtM6_out_SmartPneumaticOnlyAirCompPowerGenAtCrank"
        Me.txtM6_out_SmartPneumaticOnlyAirCompPowerGenAtCrank.Size = New System.Drawing.Size(100, 20)
        Me.txtM6_out_SmartPneumaticOnlyAirCompPowerGenAtCrank.TabIndex = 12
        '
        'lblM6_AveragePowerDemandAtCrankFromPneumatics
        '
        Me.lblM6_AveragePowerDemandAtCrankFromPneumatics.AutoSize = true
        Me.lblM6_AveragePowerDemandAtCrankFromPneumatics.Location = New System.Drawing.Point(6, 220)
        Me.lblM6_AveragePowerDemandAtCrankFromPneumatics.Name = "lblM6_AveragePowerDemandAtCrankFromPneumatics"
        Me.lblM6_AveragePowerDemandAtCrankFromPneumatics.Size = New System.Drawing.Size(195, 13)
        Me.lblM6_AveragePowerDemandAtCrankFromPneumatics.TabIndex = 11
        Me.lblM6_AveragePowerDemandAtCrankFromPneumatics.Text = "Avg Pwr Dmd@Crank From Pneumatics"
        '
        'lblM6_SmarElectricalOnlyAltPowerGenAtCrank
        '
        Me.lblM6_SmarElectricalOnlyAltPowerGenAtCrank.AutoSize = true
        Me.lblM6_SmarElectricalOnlyAltPowerGenAtCrank.Location = New System.Drawing.Point(6, 176)
        Me.lblM6_SmarElectricalOnlyAltPowerGenAtCrank.Name = "lblM6_SmarElectricalOnlyAltPowerGenAtCrank"
        Me.lblM6_SmarElectricalOnlyAltPowerGenAtCrank.Size = New System.Drawing.Size(186, 13)
        Me.lblM6_SmarElectricalOnlyAltPowerGenAtCrank.TabIndex = 10
        Me.lblM6_SmarElectricalOnlyAltPowerGenAtCrank.Text = "Smart Elec Only : Alt Pwr Gen@Crank"
        '
        'txtM6_out_AveragePowerDemandAtCrankFromPneumatics
        '
        Me.txtM6_out_AveragePowerDemandAtCrankFromPneumatics.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM6_out_AveragePowerDemandAtCrankFromPneumatics.Location = New System.Drawing.Point(6, 236)
        Me.txtM6_out_AveragePowerDemandAtCrankFromPneumatics.Name = "txtM6_out_AveragePowerDemandAtCrankFromPneumatics"
        Me.txtM6_out_AveragePowerDemandAtCrankFromPneumatics.Size = New System.Drawing.Size(100, 20)
        Me.txtM6_out_AveragePowerDemandAtCrankFromPneumatics.TabIndex = 9
        '
        'txtM6_out_SmarElectricalOnlyAltPowerGenAtCrank
        '
        Me.txtM6_out_SmarElectricalOnlyAltPowerGenAtCrank.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM6_out_SmarElectricalOnlyAltPowerGenAtCrank.Location = New System.Drawing.Point(6, 192)
        Me.txtM6_out_SmarElectricalOnlyAltPowerGenAtCrank.Name = "txtM6_out_SmarElectricalOnlyAltPowerGenAtCrank"
        Me.txtM6_out_SmarElectricalOnlyAltPowerGenAtCrank.Size = New System.Drawing.Size(100, 20)
        Me.txtM6_out_SmarElectricalOnlyAltPowerGenAtCrank.TabIndex = 8
        '
        'lblM6_SmartElectricalAndPneumaticAirCompPowerGenAtCrank
        '
        Me.lblM6_SmartElectricalAndPneumaticAirCompPowerGenAtCrank.AutoSize = true
        Me.lblM6_SmartElectricalAndPneumaticAirCompPowerGenAtCrank.Location = New System.Drawing.Point(6, 134)
        Me.lblM6_SmartElectricalAndPneumaticAirCompPowerGenAtCrank.Name = "lblM6_SmartElectricalAndPneumaticAirCompPowerGenAtCrank"
        Me.lblM6_SmartElectricalAndPneumaticAirCompPowerGenAtCrank.Size = New System.Drawing.Size(266, 13)
        Me.lblM6_SmartElectricalAndPneumaticAirCompPowerGenAtCrank.TabIndex = 7
        Me.lblM6_SmartElectricalAndPneumaticAirCompPowerGenAtCrank.Text = "Smart Elec and Pneumatic : Air Comp Pwr Gen@Crank"
        '
        'lblM6_SmartElectriclAndPneumaticsAltPowerGenAtCrank
        '
        Me.lblM6_SmartElectriclAndPneumaticsAltPowerGenAtCrank.AutoSize = true
        Me.lblM6_SmartElectriclAndPneumaticsAltPowerGenAtCrank.Location = New System.Drawing.Point(6, 91)
        Me.lblM6_SmartElectriclAndPneumaticsAltPowerGenAtCrank.Name = "lblM6_SmartElectriclAndPneumaticsAltPowerGenAtCrank"
        Me.lblM6_SmartElectriclAndPneumaticsAltPowerGenAtCrank.Size = New System.Drawing.Size(242, 13)
        Me.lblM6_SmartElectriclAndPneumaticsAltPowerGenAtCrank.TabIndex = 6
        Me.lblM6_SmartElectriclAndPneumaticsAltPowerGenAtCrank.Text = "Smart Elec and Pneumatic : Alt Pwr Gen @ Crank"
        '
        'txtM6_out_SmartElectricalAndPneumaticAirCompPowerGenAtCrank
        '
        Me.txtM6_out_SmartElectricalAndPneumaticAirCompPowerGenAtCrank.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM6_out_SmartElectricalAndPneumaticAirCompPowerGenAtCrank.Location = New System.Drawing.Point(6, 150)
        Me.txtM6_out_SmartElectricalAndPneumaticAirCompPowerGenAtCrank.Name = "txtM6_out_SmartElectricalAndPneumaticAirCompPowerGenAtCrank"
        Me.txtM6_out_SmartElectricalAndPneumaticAirCompPowerGenAtCrank.Size = New System.Drawing.Size(100, 20)
        Me.txtM6_out_SmartElectricalAndPneumaticAirCompPowerGenAtCrank.TabIndex = 5
        '
        'txtM6_out_SmartElectriclAndPneumaticsAltPowerGenAtCrank
        '
        Me.txtM6_out_SmartElectriclAndPneumaticsAltPowerGenAtCrank.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM6_out_SmartElectriclAndPneumaticsAltPowerGenAtCrank.Location = New System.Drawing.Point(6, 107)
        Me.txtM6_out_SmartElectriclAndPneumaticsAltPowerGenAtCrank.Name = "txtM6_out_SmartElectriclAndPneumaticsAltPowerGenAtCrank"
        Me.txtM6_out_SmartElectriclAndPneumaticsAltPowerGenAtCrank.Size = New System.Drawing.Size(100, 20)
        Me.txtM6_out_SmartElectriclAndPneumaticsAltPowerGenAtCrank.TabIndex = 4
        '
        'lblM6_SmartElectricalAndPneumaticsCompressorFlag
        '
        Me.lblM6_SmartElectricalAndPneumaticsCompressorFlag.AutoSize = true
        Me.lblM6_SmartElectricalAndPneumaticsCompressorFlag.Location = New System.Drawing.Point(6, 48)
        Me.lblM6_SmartElectricalAndPneumaticsCompressorFlag.Name = "lblM6_SmartElectricalAndPneumaticsCompressorFlag"
        Me.lblM6_SmartElectricalAndPneumaticsCompressorFlag.Size = New System.Drawing.Size(191, 13)
        Me.lblM6_SmartElectricalAndPneumaticsCompressorFlag.TabIndex = 3
        Me.lblM6_SmartElectricalAndPneumaticsCompressorFlag.Text = "Smart Elec and Pneumatic : Comp Flag"
        '
        'lblM6_OverrunFlag
        '
        Me.lblM6_OverrunFlag.AutoSize = true
        Me.lblM6_OverrunFlag.Location = New System.Drawing.Point(6, 9)
        Me.lblM6_OverrunFlag.Name = "lblM6_OverrunFlag"
        Me.lblM6_OverrunFlag.Size = New System.Drawing.Size(70, 13)
        Me.lblM6_OverrunFlag.TabIndex = 2
        Me.lblM6_OverrunFlag.Text = "OverRunFlag"
        '
        'txtM6_out_SmartElectricalAndPneumaticsCompressorFlag
        '
        Me.txtM6_out_SmartElectricalAndPneumaticsCompressorFlag.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM6_out_SmartElectricalAndPneumaticsCompressorFlag.Location = New System.Drawing.Point(6, 64)
        Me.txtM6_out_SmartElectricalAndPneumaticsCompressorFlag.Name = "txtM6_out_SmartElectricalAndPneumaticsCompressorFlag"
        Me.txtM6_out_SmartElectricalAndPneumaticsCompressorFlag.Size = New System.Drawing.Size(100, 20)
        Me.txtM6_out_SmartElectricalAndPneumaticsCompressorFlag.TabIndex = 1
        '
        'txtM6_out_OverrunFlag
        '
        Me.txtM6_out_OverrunFlag.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM6_out_OverrunFlag.Location = New System.Drawing.Point(6, 25)
        Me.txtM6_out_OverrunFlag.Name = "txtM6_out_OverrunFlag"
        Me.txtM6_out_OverrunFlag.Size = New System.Drawing.Size(100, 20)
        Me.txtM6_out_OverrunFlag.TabIndex = 0
        '
        'lblPreExistingAuxPower
        '
        Me.lblPreExistingAuxPower.AutoSize = true
        Me.lblPreExistingAuxPower.Location = New System.Drawing.Point(9, 164)
        Me.lblPreExistingAuxPower.Name = "lblPreExistingAuxPower"
        Me.lblPreExistingAuxPower.Size = New System.Drawing.Size(116, 13)
        Me.lblPreExistingAuxPower.TabIndex = 42
        Me.lblPreExistingAuxPower.Text = "Pre-Existing Aux Power"
        '
        'txtPreExistingAuxPower
        '
        Me.txtPreExistingAuxPower.Location = New System.Drawing.Point(10, 183)
        Me.txtPreExistingAuxPower.Name = "txtPreExistingAuxPower"
        Me.txtPreExistingAuxPower.Size = New System.Drawing.Size(100, 20)
        Me.txtPreExistingAuxPower.TabIndex = 41
        '
        'chkIdle
        '
        Me.chkIdle.AutoSize = true
        Me.chkIdle.Location = New System.Drawing.Point(10, 72)
        Me.chkIdle.Name = "chkIdle"
        Me.chkIdle.Size = New System.Drawing.Size(43, 17)
        Me.chkIdle.TabIndex = 40
        Me.chkIdle.Text = "Idle"
        Me.chkIdle.UseVisualStyleBackColor = true
        '
        'chkInNeutral
        '
        Me.chkInNeutral.AutoSize = true
        Me.chkInNeutral.Location = New System.Drawing.Point(10, 45)
        Me.chkInNeutral.Name = "chkInNeutral"
        Me.chkInNeutral.Size = New System.Drawing.Size(69, 17)
        Me.chkInNeutral.TabIndex = 39
        Me.chkInNeutral.Text = "InNeutral"
        Me.chkInNeutral.UseVisualStyleBackColor = true
        '
        'lblM5_SmartAltSetGeneration
        '
        Me.lblM5_SmartAltSetGeneration.AutoSize = true
        Me.lblM5_SmartAltSetGeneration.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblM5_SmartAltSetGeneration.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lblM5_SmartAltSetGeneration.Location = New System.Drawing.Point(327, 472)
        Me.lblM5_SmartAltSetGeneration.Name = "lblM5_SmartAltSetGeneration"
        Me.lblM5_SmartAltSetGeneration.Size = New System.Drawing.Size(168, 13)
        Me.lblM5_SmartAltSetGeneration.TabIndex = 37
        Me.lblM5_SmartAltSetGeneration.Text = "M5-Smart Alt Set Generation"
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.lblM5_AlternatorRegenPowerAtCrankTractionWatts)
        Me.Panel3.Controls.Add(Me.M5_AlternatorGenerationPowerAtCrankOverrunWatts)
        Me.Panel3.Controls.Add(Me.txtM5_out_AltRegenPowerAtCrankOverrunWatts)
        Me.Panel3.Controls.Add(Me.lblM5_AltRegenPowerAtCrankIdleW)
        Me.Panel3.Controls.Add(Me.txtM5_out_AltRegenPowerAtCrankTractionWatts)
        Me.Panel3.Controls.Add(Me.txtM5_out_AltRegenPowerAtCrankIdleWatts)
        Me.Panel3.Location = New System.Drawing.Point(328, 488)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(191, 175)
        Me.Panel3.TabIndex = 36
        '
        'lblM5_AlternatorRegenPowerAtCrankTractionWatts
        '
        Me.lblM5_AlternatorRegenPowerAtCrankTractionWatts.AutoSize = true
        Me.lblM5_AlternatorRegenPowerAtCrankTractionWatts.Location = New System.Drawing.Point(5, 48)
        Me.lblM5_AlternatorRegenPowerAtCrankTractionWatts.Name = "lblM5_AlternatorRegenPowerAtCrankTractionWatts"
        Me.lblM5_AlternatorRegenPowerAtCrankTractionWatts.Size = New System.Drawing.Size(173, 13)
        Me.lblM5_AlternatorRegenPowerAtCrankTractionWatts.TabIndex = 6
        Me.lblM5_AlternatorRegenPowerAtCrankTractionWatts.Text = "Alt Regen Pwr@tCrank Traction W"
        '
        'M5_AlternatorGenerationPowerAtCrankOverrunWatts
        '
        Me.M5_AlternatorGenerationPowerAtCrankOverrunWatts.AutoSize = true
        Me.M5_AlternatorGenerationPowerAtCrankOverrunWatts.Location = New System.Drawing.Point(8, 87)
        Me.M5_AlternatorGenerationPowerAtCrankOverrunWatts.Name = "M5_AlternatorGenerationPowerAtCrankOverrunWatts"
        Me.M5_AlternatorGenerationPowerAtCrankOverrunWatts.Size = New System.Drawing.Size(169, 13)
        Me.M5_AlternatorGenerationPowerAtCrankOverrunWatts.TabIndex = 5
        Me.M5_AlternatorGenerationPowerAtCrankOverrunWatts.Text = "Alt Regen Pwr@Crank Overrun W"
        '
        'txtM5_out_AltRegenPowerAtCrankOverrunWatts
        '
        Me.txtM5_out_AltRegenPowerAtCrankOverrunWatts.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM5_out_AltRegenPowerAtCrankOverrunWatts.Location = New System.Drawing.Point(8, 105)
        Me.txtM5_out_AltRegenPowerAtCrankOverrunWatts.Name = "txtM5_out_AltRegenPowerAtCrankOverrunWatts"
        Me.txtM5_out_AltRegenPowerAtCrankOverrunWatts.Size = New System.Drawing.Size(100, 20)
        Me.txtM5_out_AltRegenPowerAtCrankOverrunWatts.TabIndex = 4
        '
        'lblM5_AltRegenPowerAtCrankIdleW
        '
        Me.lblM5_AltRegenPowerAtCrankIdleW.AutoSize = true
        Me.lblM5_AltRegenPowerAtCrankIdleW.Location = New System.Drawing.Point(5, 5)
        Me.lblM5_AltRegenPowerAtCrankIdleW.Name = "lblM5_AltRegenPowerAtCrankIdleW"
        Me.lblM5_AltRegenPowerAtCrankIdleW.Size = New System.Drawing.Size(165, 13)
        Me.lblM5_AltRegenPowerAtCrankIdleW.TabIndex = 2
        Me.lblM5_AltRegenPowerAtCrankIdleW.Text = "Alt Regen Pwr@tCrank IdleWatts"
        '
        'txtM5_out_AltRegenPowerAtCrankTractionWatts
        '
        Me.txtM5_out_AltRegenPowerAtCrankTractionWatts.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM5_out_AltRegenPowerAtCrankTractionWatts.Location = New System.Drawing.Point(8, 64)
        Me.txtM5_out_AltRegenPowerAtCrankTractionWatts.Name = "txtM5_out_AltRegenPowerAtCrankTractionWatts"
        Me.txtM5_out_AltRegenPowerAtCrankTractionWatts.Size = New System.Drawing.Size(100, 20)
        Me.txtM5_out_AltRegenPowerAtCrankTractionWatts.TabIndex = 1
        '
        'txtM5_out_AltRegenPowerAtCrankIdleWatts
        '
        Me.txtM5_out_AltRegenPowerAtCrankIdleWatts.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM5_out_AltRegenPowerAtCrankIdleWatts.Location = New System.Drawing.Point(8, 21)
        Me.txtM5_out_AltRegenPowerAtCrankIdleWatts.Name = "txtM5_out_AltRegenPowerAtCrankIdleWatts"
        Me.txtM5_out_AltRegenPowerAtCrankIdleWatts.Size = New System.Drawing.Size(100, 20)
        Me.txtM5_out_AltRegenPowerAtCrankIdleWatts.TabIndex = 0
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(13, 429)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(100, 94)
        Me.PictureBox1.TabIndex = 34
        Me.PictureBox1.TabStop = false
        '
        'lblM4_AirCompressor
        '
        Me.lblM4_AirCompressor.AutoSize = true
        Me.lblM4_AirCompressor.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblM4_AirCompressor.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lblM4_AirCompressor.Location = New System.Drawing.Point(327, 267)
        Me.lblM4_AirCompressor.Name = "lblM4_AirCompressor"
        Me.lblM4_AirCompressor.Size = New System.Drawing.Size(112, 13)
        Me.lblM4_AirCompressor.TabIndex = 33
        Me.lblM4_AirCompressor.Text = "M4-Air Compressor"
        '
        'btnFinish
        '
        Me.btnFinish.Location = New System.Drawing.Point(10, 580)
        Me.btnFinish.Name = "btnFinish"
        Me.btnFinish.Size = New System.Drawing.Size(100, 23)
        Me.btnFinish.TabIndex = 4
        Me.btnFinish.Text = "Stop Processing"
        Me.btnFinish.UseVisualStyleBackColor = true
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.lblM4_PowerAtCrankFromPSCompressorON)
        Me.Panel1.Controls.Add(Me.lblM4_PowerAtCrankFromPSCompressorOFF)
        Me.Panel1.Controls.Add(Me.lblM4_CompressorOnOffPowerDelta)
        Me.Panel1.Controls.Add(Me.lblM4_CompressorFlowRate)
        Me.Panel1.Controls.Add(Me.txtM4_out_PowerAtCrankFromPneumaticsCompressorON)
        Me.Panel1.Controls.Add(Me.txtM4_out_PowerAtCrankFromPneumaticsCompressorOFF)
        Me.Panel1.Controls.Add(Me.txtM4_out_CompresssorPwrOnMinusPwrOff)
        Me.Panel1.Controls.Add(Me.txtM4_out_CompressorFlowRate)
        Me.Panel1.Location = New System.Drawing.Point(327, 283)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(192, 183)
        Me.Panel1.TabIndex = 32
        '
        'lblM4_PowerAtCrankFromPSCompressorON
        '
        Me.lblM4_PowerAtCrankFromPSCompressorON.AutoSize = true
        Me.lblM4_PowerAtCrankFromPSCompressorON.Location = New System.Drawing.Point(13, 129)
        Me.lblM4_PowerAtCrankFromPSCompressorON.Name = "lblM4_PowerAtCrankFromPSCompressorON"
        Me.lblM4_PowerAtCrankFromPSCompressorON.Size = New System.Drawing.Size(153, 13)
        Me.lblM4_PowerAtCrankFromPSCompressorON.TabIndex = 7
        Me.lblM4_PowerAtCrankFromPSCompressorON.Text = "Pwr@Crank From PS CompON"
        '
        'lblM4_PowerAtCrankFromPSCompressorOFF
        '
        Me.lblM4_PowerAtCrankFromPSCompressorOFF.AutoSize = true
        Me.lblM4_PowerAtCrankFromPSCompressorOFF.Location = New System.Drawing.Point(13, 87)
        Me.lblM4_PowerAtCrankFromPSCompressorOFF.Name = "lblM4_PowerAtCrankFromPSCompressorOFF"
        Me.lblM4_PowerAtCrankFromPSCompressorOFF.Size = New System.Drawing.Size(157, 13)
        Me.lblM4_PowerAtCrankFromPSCompressorOFF.TabIndex = 6
        Me.lblM4_PowerAtCrankFromPSCompressorOFF.Text = "Pwr@Crank From PS CompOFF"
        '
        'lblM4_CompressorOnOffPowerDelta
        '
        Me.lblM4_CompressorOnOffPowerDelta.AutoSize = true
        Me.lblM4_CompressorOnOffPowerDelta.Location = New System.Drawing.Point(13, 50)
        Me.lblM4_CompressorOnOffPowerDelta.Name = "lblM4_CompressorOnOffPowerDelta"
        Me.lblM4_CompressorOnOffPowerDelta.Size = New System.Drawing.Size(162, 13)
        Me.lblM4_CompressorOnOffPowerDelta.TabIndex = 5
        Me.lblM4_CompressorOnOffPowerDelta.Text = "Compressor PwrOn minus PwrOff"
        '
        'lblM4_CompressorFlowRate
        '
        Me.lblM4_CompressorFlowRate.AutoSize = true
        Me.lblM4_CompressorFlowRate.Location = New System.Drawing.Point(13, 7)
        Me.lblM4_CompressorFlowRate.Name = "lblM4_CompressorFlowRate"
        Me.lblM4_CompressorFlowRate.Size = New System.Drawing.Size(134, 13)
        Me.lblM4_CompressorFlowRate.TabIndex = 4
        Me.lblM4_CompressorFlowRate.Text = "Compressor Flow Rate L/S"
        '
        'txtM4_out_PowerAtCrankFromPneumaticsCompressorON
        '
        Me.txtM4_out_PowerAtCrankFromPneumaticsCompressorON.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM4_out_PowerAtCrankFromPneumaticsCompressorON.Location = New System.Drawing.Point(13, 144)
        Me.txtM4_out_PowerAtCrankFromPneumaticsCompressorON.Name = "txtM4_out_PowerAtCrankFromPneumaticsCompressorON"
        Me.txtM4_out_PowerAtCrankFromPneumaticsCompressorON.Size = New System.Drawing.Size(100, 20)
        Me.txtM4_out_PowerAtCrankFromPneumaticsCompressorON.TabIndex = 3
        '
        'txtM4_out_PowerAtCrankFromPneumaticsCompressorOFF
        '
        Me.txtM4_out_PowerAtCrankFromPneumaticsCompressorOFF.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM4_out_PowerAtCrankFromPneumaticsCompressorOFF.Location = New System.Drawing.Point(13, 103)
        Me.txtM4_out_PowerAtCrankFromPneumaticsCompressorOFF.Name = "txtM4_out_PowerAtCrankFromPneumaticsCompressorOFF"
        Me.txtM4_out_PowerAtCrankFromPneumaticsCompressorOFF.Size = New System.Drawing.Size(100, 20)
        Me.txtM4_out_PowerAtCrankFromPneumaticsCompressorOFF.TabIndex = 2
        '
        'txtM4_out_CompresssorPwrOnMinusPwrOff
        '
        Me.txtM4_out_CompresssorPwrOnMinusPwrOff.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM4_out_CompresssorPwrOnMinusPwrOff.Location = New System.Drawing.Point(13, 65)
        Me.txtM4_out_CompresssorPwrOnMinusPwrOff.Name = "txtM4_out_CompresssorPwrOnMinusPwrOff"
        Me.txtM4_out_CompresssorPwrOnMinusPwrOff.Size = New System.Drawing.Size(100, 20)
        Me.txtM4_out_CompresssorPwrOnMinusPwrOff.TabIndex = 1
        '
        'txtM4_out_CompressorFlowRate
        '
        Me.txtM4_out_CompressorFlowRate.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM4_out_CompressorFlowRate.Location = New System.Drawing.Point(13, 24)
        Me.txtM4_out_CompressorFlowRate.Name = "txtM4_out_CompressorFlowRate"
        Me.txtM4_out_CompressorFlowRate.Size = New System.Drawing.Size(100, 20)
        Me.txtM4_out_CompressorFlowRate.TabIndex = 0
        '
        'lblM3_AveragePneumaticLoad
        '
        Me.lblM3_AveragePneumaticLoad.AutoSize = true
        Me.lblM3_AveragePneumaticLoad.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblM3_AveragePneumaticLoad.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lblM3_AveragePneumaticLoad.Location = New System.Drawing.Point(324, 143)
        Me.lblM3_AveragePneumaticLoad.Name = "lblM3_AveragePneumaticLoad"
        Me.lblM3_AveragePneumaticLoad.Size = New System.Drawing.Size(195, 13)
        Me.lblM3_AveragePneumaticLoad.TabIndex = 31
        Me.lblM3_AveragePneumaticLoad.Text = "M3-Avg Pneumatic Load Demand"
        '
        'pnl_M3_Displays
        '
        Me.pnl_M3_Displays.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnl_M3_Displays.Controls.Add(Me.lblM3_TotAirConsumptionPerCycleLitres)
        Me.pnl_M3_Displays.Controls.Add(Me.lbl_M3_AvgPowerAtCrankFromPneumatics)
        Me.pnl_M3_Displays.Controls.Add(Me.txtM3_out_TotalAirConsumedPerCycleInLitres)
        Me.pnl_M3_Displays.Controls.Add(Me.txtM3_out_AveragePowerAtCrankFromPneumatics)
        Me.pnl_M3_Displays.Location = New System.Drawing.Point(327, 161)
        Me.pnl_M3_Displays.Name = "pnl_M3_Displays"
        Me.pnl_M3_Displays.Size = New System.Drawing.Size(192, 100)
        Me.pnl_M3_Displays.TabIndex = 30
        '
        'lblM3_TotAirConsumptionPerCycleLitres
        '
        Me.lblM3_TotAirConsumptionPerCycleLitres.AutoSize = true
        Me.lblM3_TotAirConsumptionPerCycleLitres.Location = New System.Drawing.Point(8, 49)
        Me.lblM3_TotAirConsumptionPerCycleLitres.Name = "lblM3_TotAirConsumptionPerCycleLitres"
        Me.lblM3_TotAirConsumptionPerCycleLitres.Size = New System.Drawing.Size(136, 13)
        Me.lblM3_TotAirConsumptionPerCycleLitres.TabIndex = 3
        Me.lblM3_TotAirConsumptionPerCycleLitres.Text = "Total Air Cosumed/Cycle(L)"
        '
        'lbl_M3_AvgPowerAtCrankFromPneumatics
        '
        Me.lbl_M3_AvgPowerAtCrankFromPneumatics.AutoSize = true
        Me.lbl_M3_AvgPowerAtCrankFromPneumatics.Location = New System.Drawing.Point(9, 4)
        Me.lbl_M3_AvgPowerAtCrankFromPneumatics.Name = "lbl_M3_AvgPowerAtCrankFromPneumatics"
        Me.lbl_M3_AvgPowerAtCrankFromPneumatics.Size = New System.Drawing.Size(176, 13)
        Me.lbl_M3_AvgPowerAtCrankFromPneumatics.TabIndex = 2
        Me.lbl_M3_AvgPowerAtCrankFromPneumatics.Text = "Avg Power@Crank Frm Pneumatics"
        '
        'txtM3_out_TotalAirConsumedPerCycleInLitres
        '
        Me.txtM3_out_TotalAirConsumedPerCycleInLitres.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM3_out_TotalAirConsumedPerCycleInLitres.Location = New System.Drawing.Point(10, 67)
        Me.txtM3_out_TotalAirConsumedPerCycleInLitres.Name = "txtM3_out_TotalAirConsumedPerCycleInLitres"
        Me.txtM3_out_TotalAirConsumedPerCycleInLitres.Size = New System.Drawing.Size(100, 20)
        Me.txtM3_out_TotalAirConsumedPerCycleInLitres.TabIndex = 1
        '
        'txtM3_out_AveragePowerAtCrankFromPneumatics
        '
        Me.txtM3_out_AveragePowerAtCrankFromPneumatics.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM3_out_AveragePowerAtCrankFromPneumatics.Location = New System.Drawing.Point(10, 22)
        Me.txtM3_out_AveragePowerAtCrankFromPneumatics.Name = "txtM3_out_AveragePowerAtCrankFromPneumatics"
        Me.txtM3_out_AveragePowerAtCrankFromPneumatics.Size = New System.Drawing.Size(100, 20)
        Me.txtM3_out_AveragePowerAtCrankFromPneumatics.TabIndex = 0
        '
        'btnStart
        '
        Me.btnStart.Location = New System.Drawing.Point(10, 524)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(102, 23)
        Me.btnStart.TabIndex = 1
        Me.btnStart.Text = "Start Processing"
        Me.btnStart.UseVisualStyleBackColor = true
        '
        'lblM2AverageElectricalLoadTitle
        '
        Me.lblM2AverageElectricalLoadTitle.AutoSize = true
        Me.lblM2AverageElectricalLoadTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblM2AverageElectricalLoadTitle.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lblM2AverageElectricalLoadTitle.Location = New System.Drawing.Point(327, 8)
        Me.lblM2AverageElectricalLoadTitle.Name = "lblM2AverageElectricalLoadTitle"
        Me.lblM2AverageElectricalLoadTitle.Size = New System.Drawing.Size(189, 13)
        Me.lblM2AverageElectricalLoadTitle.TabIndex = 29
        Me.lblM2AverageElectricalLoadTitle.Text = "M2-Avg Electrical Load Demand"
        '
        'pnl_M2_Displays
        '
        Me.pnl_M2_Displays.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnl_M2_Displays.Controls.Add(Me.lblM2_AvgPwrDmdAtCrankFromElectrics)
        Me.pnl_M2_Displays.Controls.Add(Me.lblM2_AveragePowerDemandAtAlternatorFromElectrics)
        Me.pnl_M2_Displays.Controls.Add(Me.txtM2_out_AvgPowerAtCrankFromElectrics)
        Me.pnl_M2_Displays.Controls.Add(Me.txtM2_out_AvgPowerAtAltFromElectrics)
        Me.pnl_M2_Displays.Location = New System.Drawing.Point(329, 35)
        Me.pnl_M2_Displays.Name = "pnl_M2_Displays"
        Me.pnl_M2_Displays.Size = New System.Drawing.Size(190, 100)
        Me.pnl_M2_Displays.TabIndex = 28
        '
        'lblM2_AvgPwrDmdAtCrankFromElectrics
        '
        Me.lblM2_AvgPwrDmdAtCrankFromElectrics.AutoSize = true
        Me.lblM2_AvgPwrDmdAtCrankFromElectrics.Location = New System.Drawing.Point(7, 52)
        Me.lblM2_AvgPwrDmdAtCrankFromElectrics.Name = "lblM2_AvgPwrDmdAtCrankFromElectrics"
        Me.lblM2_AvgPwrDmdAtCrankFromElectrics.Size = New System.Drawing.Size(177, 13)
        Me.lblM2_AvgPwrDmdAtCrankFromElectrics.TabIndex = 3
        Me.lblM2_AvgPwrDmdAtCrankFromElectrics.Text = "Avg Pwr Dmd@Crank from Electrics"
        '
        'lblM2_AveragePowerDemandAtAlternatorFromElectrics
        '
        Me.lblM2_AveragePowerDemandAtAlternatorFromElectrics.AutoSize = true
        Me.lblM2_AveragePowerDemandAtAlternatorFromElectrics.Location = New System.Drawing.Point(8, 9)
        Me.lblM2_AveragePowerDemandAtAlternatorFromElectrics.Name = "lblM2_AveragePowerDemandAtAlternatorFromElectrics"
        Me.lblM2_AveragePowerDemandAtAlternatorFromElectrics.Size = New System.Drawing.Size(170, 13)
        Me.lblM2_AveragePowerDemandAtAlternatorFromElectrics.TabIndex = 2
        Me.lblM2_AveragePowerDemandAtAlternatorFromElectrics.Text = "Avg Pwr Dmd @ Alt From Electrics"
        '
        'txtM2_out_AvgPowerAtCrankFromElectrics
        '
        Me.txtM2_out_AvgPowerAtCrankFromElectrics.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM2_out_AvgPowerAtCrankFromElectrics.Location = New System.Drawing.Point(8, 68)
        Me.txtM2_out_AvgPowerAtCrankFromElectrics.Name = "txtM2_out_AvgPowerAtCrankFromElectrics"
        Me.txtM2_out_AvgPowerAtCrankFromElectrics.Size = New System.Drawing.Size(100, 20)
        Me.txtM2_out_AvgPowerAtCrankFromElectrics.TabIndex = 1
        '
        'txtM2_out_AvgPowerAtAltFromElectrics
        '
        Me.txtM2_out_AvgPowerAtAltFromElectrics.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM2_out_AvgPowerAtAltFromElectrics.Location = New System.Drawing.Point(9, 25)
        Me.txtM2_out_AvgPowerAtAltFromElectrics.Name = "txtM2_out_AvgPowerAtAltFromElectrics"
        Me.txtM2_out_AvgPowerAtAltFromElectrics.Size = New System.Drawing.Size(100, 20)
        Me.txtM2_out_AvgPowerAtAltFromElectrics.TabIndex = 0
        '
        'lblM1_HVACAverageLoad
        '
        Me.lblM1_HVACAverageLoad.AutoSize = true
        Me.lblM1_HVACAverageLoad.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblM1_HVACAverageLoad.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lblM1_HVACAverageLoad.Location = New System.Drawing.Point(142, 425)
        Me.lblM1_HVACAverageLoad.Name = "lblM1_HVACAverageLoad"
        Me.lblM1_HVACAverageLoad.Size = New System.Drawing.Size(143, 13)
        Me.lblM1_HVACAverageLoad.TabIndex = 27
        Me.lblM1_HVACAverageLoad.Text = "M1-HVAC_AverageLoad"
        '
        'pnl_M1_Displays
        '
        Me.pnl_M1_Displays.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnl_M1_Displays.Controls.Add(Me.lblM1_HVACFuelling)
        Me.pnl_M1_Displays.Controls.Add(Me.lblM1_AvgPowerDemandAtCrankHVACElectrics)
        Me.pnl_M1_Displays.Controls.Add(Me.lblM1_AveragePowerDemandAtAlternatorElectrics)
        Me.pnl_M1_Displays.Controls.Add(Me.lblM1_AveragePowerDemandAtCrank)
        Me.pnl_M1_Displays.Controls.Add(Me.txtM1_out_HVACFuelling)
        Me.pnl_M1_Displays.Controls.Add(Me.txtM1_out_AvgPwrAtCrankFromHVACElec)
        Me.pnl_M1_Displays.Controls.Add(Me.txtM1_out_AvgPowerDemandAtAlternatorHvacElectrics)
        Me.pnl_M1_Displays.Controls.Add(Me.txtM1_out_AvgPowerDemandAtCrankMech)
        Me.pnl_M1_Displays.Location = New System.Drawing.Point(144, 444)
        Me.pnl_M1_Displays.Name = "pnl_M1_Displays"
        Me.pnl_M1_Displays.Size = New System.Drawing.Size(172, 219)
        Me.pnl_M1_Displays.TabIndex = 26
        '
        'lblM1_HVACFuelling
        '
        Me.lblM1_HVACFuelling.AutoSize = true
        Me.lblM1_HVACFuelling.Location = New System.Drawing.Point(4, 133)
        Me.lblM1_HVACFuelling.Name = "lblM1_HVACFuelling"
        Me.lblM1_HVACFuelling.Size = New System.Drawing.Size(75, 13)
        Me.lblM1_HVACFuelling.TabIndex = 7
        Me.lblM1_HVACFuelling.Text = "HVAC Fuelling"
        '
        'lblM1_AvgPowerDemandAtCrankHVACElectrics
        '
        Me.lblM1_AvgPowerDemandAtCrankHVACElectrics.AutoSize = true
        Me.lblM1_AvgPowerDemandAtCrankHVACElectrics.Location = New System.Drawing.Point(4, 92)
        Me.lblM1_AvgPowerDemandAtCrankHVACElectrics.Name = "lblM1_AvgPowerDemandAtCrankHVACElectrics"
        Me.lblM1_AvgPowerDemandAtCrankHVACElectrics.Size = New System.Drawing.Size(167, 13)
        Me.lblM1_AvgPowerDemandAtCrankHVACElectrics.TabIndex = 6
        Me.lblM1_AvgPowerDemandAtCrankHVACElectrics.Text = "Avg Pwr Dmd@Crank HVAC Elec"
        '
        'lblM1_AveragePowerDemandAtAlternatorElectrics
        '
        Me.lblM1_AveragePowerDemandAtAlternatorElectrics.AutoSize = true
        Me.lblM1_AveragePowerDemandAtAlternatorElectrics.Location = New System.Drawing.Point(5, 51)
        Me.lblM1_AveragePowerDemandAtAlternatorElectrics.Name = "lblM1_AveragePowerDemandAtAlternatorElectrics"
        Me.lblM1_AveragePowerDemandAtAlternatorElectrics.Size = New System.Drawing.Size(157, 13)
        Me.lblM1_AveragePowerDemandAtAlternatorElectrics.TabIndex = 5
        Me.lblM1_AveragePowerDemandAtAlternatorElectrics.Text = "Avg Pwr Dmd @ Alt HVAC Elec"
        '
        'lblM1_AveragePowerDemandAtCrank
        '
        Me.lblM1_AveragePowerDemandAtCrank.AutoSize = true
        Me.lblM1_AveragePowerDemandAtCrank.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblM1_AveragePowerDemandAtCrank.Location = New System.Drawing.Point(6, 8)
        Me.lblM1_AveragePowerDemandAtCrank.Name = "lblM1_AveragePowerDemandAtCrank"
        Me.lblM1_AveragePowerDemandAtCrank.Size = New System.Drawing.Size(147, 13)
        Me.lblM1_AveragePowerDemandAtCrank.TabIndex = 4
        Me.lblM1_AveragePowerDemandAtCrank.Text = "Avg Pwr Dmd @ Crank Mech"
        '
        'txtM1_out_HVACFuelling
        '
        Me.txtM1_out_HVACFuelling.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM1_out_HVACFuelling.Location = New System.Drawing.Point(7, 149)
        Me.txtM1_out_HVACFuelling.Name = "txtM1_out_HVACFuelling"
        Me.txtM1_out_HVACFuelling.Size = New System.Drawing.Size(100, 20)
        Me.txtM1_out_HVACFuelling.TabIndex = 3
        '
        'txtM1_out_AvgPwrAtCrankFromHVACElec
        '
        Me.txtM1_out_AvgPwrAtCrankFromHVACElec.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM1_out_AvgPwrAtCrankFromHVACElec.Location = New System.Drawing.Point(7, 108)
        Me.txtM1_out_AvgPwrAtCrankFromHVACElec.Name = "txtM1_out_AvgPwrAtCrankFromHVACElec"
        Me.txtM1_out_AvgPwrAtCrankFromHVACElec.Size = New System.Drawing.Size(100, 20)
        Me.txtM1_out_AvgPwrAtCrankFromHVACElec.TabIndex = 2
        '
        'txtM1_out_AvgPowerDemandAtAlternatorHvacElectrics
        '
        Me.txtM1_out_AvgPowerDemandAtAlternatorHvacElectrics.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM1_out_AvgPowerDemandAtAlternatorHvacElectrics.Location = New System.Drawing.Point(7, 65)
        Me.txtM1_out_AvgPowerDemandAtAlternatorHvacElectrics.Name = "txtM1_out_AvgPowerDemandAtAlternatorHvacElectrics"
        Me.txtM1_out_AvgPowerDemandAtAlternatorHvacElectrics.Size = New System.Drawing.Size(100, 20)
        Me.txtM1_out_AvgPowerDemandAtAlternatorHvacElectrics.TabIndex = 1
        '
        'txtM1_out_AvgPowerDemandAtCrankMech
        '
        Me.txtM1_out_AvgPowerDemandAtCrankMech.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM1_out_AvgPowerDemandAtCrankMech.Location = New System.Drawing.Point(7, 24)
        Me.txtM1_out_AvgPowerDemandAtCrankMech.Name = "txtM1_out_AvgPowerDemandAtCrankMech"
        Me.txtM1_out_AvgPowerDemandAtCrankMech.Size = New System.Drawing.Size(100, 20)
        Me.txtM1_out_AvgPowerDemandAtCrankMech.TabIndex = 0
        '
        'lblM05SmartalternatorSetEfficiency
        '
        Me.lblM05SmartalternatorSetEfficiency.AutoSize = true
        Me.lblM05SmartalternatorSetEfficiency.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblM05SmartalternatorSetEfficiency.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lblM05SmartalternatorSetEfficiency.Location = New System.Drawing.Point(141, 143)
        Me.lblM05SmartalternatorSetEfficiency.Name = "lblM05SmartalternatorSetEfficiency"
        Me.lblM05SmartalternatorSetEfficiency.Size = New System.Drawing.Size(169, 13)
        Me.lblM05SmartalternatorSetEfficiency.TabIndex = 25
        Me.lblM05SmartalternatorSetEfficiency.Text = "M05-Smart Alt Set Efficiency"
        '
        'pnl_M05_Displays
        '
        Me.pnl_M05_Displays.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnl_M05_Displays.Controls.Add(Me.lblM05_AlternatorsEfficiencyOverrun)
        Me.pnl_M05_Displays.Controls.Add(Me.lblM05SmartOverrunCurrent)
        Me.pnl_M05_Displays.Controls.Add(Me.lblM05_AlternatorsEfficiencyTraction)
        Me.pnl_M05_Displays.Controls.Add(Me.lblM05_SmartTractionCurrent)
        Me.pnl_M05_Displays.Controls.Add(Me.lblM05AlternatorsEfficiencyIdle)
        Me.pnl_M05_Displays.Controls.Add(Me.lblM05_SmartIdleCurrent)
        Me.pnl_M05_Displays.Controls.Add(Me.txtM05_out_AlternatorsEfficiencyOverrun)
        Me.pnl_M05_Displays.Controls.Add(Me.txtM05_out_SmartOverrunCurrent)
        Me.pnl_M05_Displays.Controls.Add(Me.txtM05_out_AlternatorsEfficiencyTraction)
        Me.pnl_M05_Displays.Controls.Add(Me.txtM05_out_SmartTractionCurrent)
        Me.pnl_M05_Displays.Controls.Add(Me.txtM05_Out_AlternatorsEfficiencyIdle)
        Me.pnl_M05_Displays.Controls.Add(Me.txtM05_OutSmartIdleCurrent)
        Me.pnl_M05_Displays.Location = New System.Drawing.Point(142, 160)
        Me.pnl_M05_Displays.Name = "pnl_M05_Displays"
        Me.pnl_M05_Displays.Size = New System.Drawing.Size(174, 259)
        Me.pnl_M05_Displays.TabIndex = 24
        '
        'lblM05_AlternatorsEfficiencyOverrun
        '
        Me.lblM05_AlternatorsEfficiencyOverrun.AutoSize = true
        Me.lblM05_AlternatorsEfficiencyOverrun.Location = New System.Drawing.Point(11, 213)
        Me.lblM05_AlternatorsEfficiencyOverrun.Name = "lblM05_AlternatorsEfficiencyOverrun"
        Me.lblM05_AlternatorsEfficiencyOverrun.Size = New System.Drawing.Size(147, 13)
        Me.lblM05_AlternatorsEfficiencyOverrun.TabIndex = 11
        Me.lblM05_AlternatorsEfficiencyOverrun.Text = "Alternators Efficiency Overrun"
        '
        'lblM05SmartOverrunCurrent
        '
        Me.lblM05SmartOverrunCurrent.AutoSize = true
        Me.lblM05SmartOverrunCurrent.Location = New System.Drawing.Point(11, 172)
        Me.lblM05SmartOverrunCurrent.Name = "lblM05SmartOverrunCurrent"
        Me.lblM05SmartOverrunCurrent.Size = New System.Drawing.Size(112, 13)
        Me.lblM05SmartOverrunCurrent.TabIndex = 10
        Me.lblM05SmartOverrunCurrent.Text = "Smart Overrun Current"
        '
        'lblM05_AlternatorsEfficiencyTraction
        '
        Me.lblM05_AlternatorsEfficiencyTraction.AutoSize = true
        Me.lblM05_AlternatorsEfficiencyTraction.Location = New System.Drawing.Point(10, 130)
        Me.lblM05_AlternatorsEfficiencyTraction.Name = "lblM05_AlternatorsEfficiencyTraction"
        Me.lblM05_AlternatorsEfficiencyTraction.Size = New System.Drawing.Size(148, 13)
        Me.lblM05_AlternatorsEfficiencyTraction.TabIndex = 9
        Me.lblM05_AlternatorsEfficiencyTraction.Text = "Alternators Efficiency Traction"
        '
        'lblM05_SmartTractionCurrent
        '
        Me.lblM05_SmartTractionCurrent.AutoSize = true
        Me.lblM05_SmartTractionCurrent.Location = New System.Drawing.Point(10, 87)
        Me.lblM05_SmartTractionCurrent.Name = "lblM05_SmartTractionCurrent"
        Me.lblM05_SmartTractionCurrent.Size = New System.Drawing.Size(113, 13)
        Me.lblM05_SmartTractionCurrent.TabIndex = 8
        Me.lblM05_SmartTractionCurrent.Text = "Smart Traction Current"
        '
        'lblM05AlternatorsEfficiencyIdle
        '
        Me.lblM05AlternatorsEfficiencyIdle.AutoSize = true
        Me.lblM05AlternatorsEfficiencyIdle.Location = New System.Drawing.Point(11, 45)
        Me.lblM05AlternatorsEfficiencyIdle.Name = "lblM05AlternatorsEfficiencyIdle"
        Me.lblM05AlternatorsEfficiencyIdle.Size = New System.Drawing.Size(126, 13)
        Me.lblM05AlternatorsEfficiencyIdle.TabIndex = 7
        Me.lblM05AlternatorsEfficiencyIdle.Text = "Alternators Efficiency Idle"
        '
        'lblM05_SmartIdleCurrent
        '
        Me.lblM05_SmartIdleCurrent.AutoSize = true
        Me.lblM05_SmartIdleCurrent.Location = New System.Drawing.Point(11, 5)
        Me.lblM05_SmartIdleCurrent.Name = "lblM05_SmartIdleCurrent"
        Me.lblM05_SmartIdleCurrent.Size = New System.Drawing.Size(91, 13)
        Me.lblM05_SmartIdleCurrent.TabIndex = 6
        Me.lblM05_SmartIdleCurrent.Text = "Smart Idle Current"
        '
        'txtM05_out_AlternatorsEfficiencyOverrun
        '
        Me.txtM05_out_AlternatorsEfficiencyOverrun.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM05_out_AlternatorsEfficiencyOverrun.Location = New System.Drawing.Point(11, 226)
        Me.txtM05_out_AlternatorsEfficiencyOverrun.Name = "txtM05_out_AlternatorsEfficiencyOverrun"
        Me.txtM05_out_AlternatorsEfficiencyOverrun.Size = New System.Drawing.Size(100, 20)
        Me.txtM05_out_AlternatorsEfficiencyOverrun.TabIndex = 5
        '
        'txtM05_out_SmartOverrunCurrent
        '
        Me.txtM05_out_SmartOverrunCurrent.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM05_out_SmartOverrunCurrent.Location = New System.Drawing.Point(11, 187)
        Me.txtM05_out_SmartOverrunCurrent.Name = "txtM05_out_SmartOverrunCurrent"
        Me.txtM05_out_SmartOverrunCurrent.Size = New System.Drawing.Size(100, 20)
        Me.txtM05_out_SmartOverrunCurrent.TabIndex = 4
        '
        'txtM05_out_AlternatorsEfficiencyTraction
        '
        Me.txtM05_out_AlternatorsEfficiencyTraction.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM05_out_AlternatorsEfficiencyTraction.Location = New System.Drawing.Point(11, 143)
        Me.txtM05_out_AlternatorsEfficiencyTraction.Name = "txtM05_out_AlternatorsEfficiencyTraction"
        Me.txtM05_out_AlternatorsEfficiencyTraction.Size = New System.Drawing.Size(100, 20)
        Me.txtM05_out_AlternatorsEfficiencyTraction.TabIndex = 3
        '
        'txtM05_out_SmartTractionCurrent
        '
        Me.txtM05_out_SmartTractionCurrent.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM05_out_SmartTractionCurrent.Location = New System.Drawing.Point(11, 102)
        Me.txtM05_out_SmartTractionCurrent.Name = "txtM05_out_SmartTractionCurrent"
        Me.txtM05_out_SmartTractionCurrent.Size = New System.Drawing.Size(100, 20)
        Me.txtM05_out_SmartTractionCurrent.TabIndex = 2
        '
        'txtM05_Out_AlternatorsEfficiencyIdle
        '
        Me.txtM05_Out_AlternatorsEfficiencyIdle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM05_Out_AlternatorsEfficiencyIdle.Location = New System.Drawing.Point(11, 60)
        Me.txtM05_Out_AlternatorsEfficiencyIdle.Name = "txtM05_Out_AlternatorsEfficiencyIdle"
        Me.txtM05_Out_AlternatorsEfficiencyIdle.Size = New System.Drawing.Size(100, 20)
        Me.txtM05_Out_AlternatorsEfficiencyIdle.TabIndex = 1
        '
        'txtM05_OutSmartIdleCurrent
        '
        Me.txtM05_OutSmartIdleCurrent.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM05_OutSmartIdleCurrent.Location = New System.Drawing.Point(11, 19)
        Me.txtM05_OutSmartIdleCurrent.Name = "txtM05_OutSmartIdleCurrent"
        Me.txtM05_OutSmartIdleCurrent.Size = New System.Drawing.Size(100, 20)
        Me.txtM05_OutSmartIdleCurrent.TabIndex = 0
        '
        'lblM0Outputs
        '
        Me.lblM0Outputs.AutoSize = true
        Me.lblM0Outputs.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblM0Outputs.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lblM0Outputs.Location = New System.Drawing.Point(142, 8)
        Me.lblM0Outputs.MaximumSize = New System.Drawing.Size(150, 0)
        Me.lblM0Outputs.Name = "lblM0Outputs"
        Me.lblM0Outputs.Size = New System.Drawing.Size(133, 26)
        Me.lblM0Outputs.TabIndex = 23
        Me.lblM0Outputs.Text = "M0-Non-Smart Alt Set Efficiency"
        '
        'pnl_M0_Displays
        '
        Me.pnl_M0_Displays.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnl_M0_Displays.Controls.Add(Me.lblOutHVACElectricalCurrentDemand)
        Me.pnl_M0_Displays.Controls.Add(Me.lblAlternatorsEfficiency)
        Me.pnl_M0_Displays.Controls.Add(Me.txtM0_Out_AlternatorsEfficiency)
        Me.pnl_M0_Displays.Controls.Add(Me.txtM0_Out_HVacElectricalCurrentDemand)
        Me.pnl_M0_Displays.ForeColor = System.Drawing.Color.Black
        Me.pnl_M0_Displays.Location = New System.Drawing.Point(142, 35)
        Me.pnl_M0_Displays.Name = "pnl_M0_Displays"
        Me.pnl_M0_Displays.Size = New System.Drawing.Size(174, 100)
        Me.pnl_M0_Displays.TabIndex = 22
        '
        'lblOutHVACElectricalCurrentDemand
        '
        Me.lblOutHVACElectricalCurrentDemand.AutoSize = true
        Me.lblOutHVACElectricalCurrentDemand.Location = New System.Drawing.Point(9, 8)
        Me.lblOutHVACElectricalCurrentDemand.Name = "lblOutHVACElectricalCurrentDemand"
        Me.lblOutHVACElectricalCurrentDemand.Size = New System.Drawing.Size(144, 13)
        Me.lblOutHVACElectricalCurrentDemand.TabIndex = 25
        Me.lblOutHVACElectricalCurrentDemand.Text = "HVAC Electrical Current Dmd"
        '
        'lblAlternatorsEfficiency
        '
        Me.lblAlternatorsEfficiency.AutoSize = true
        Me.lblAlternatorsEfficiency.Location = New System.Drawing.Point(9, 49)
        Me.lblAlternatorsEfficiency.Name = "lblAlternatorsEfficiency"
        Me.lblAlternatorsEfficiency.Size = New System.Drawing.Size(106, 13)
        Me.lblAlternatorsEfficiency.TabIndex = 24
        Me.lblAlternatorsEfficiency.Text = "Alternators Efficiency"
        '
        'txtM0_Out_AlternatorsEfficiency
        '
        Me.txtM0_Out_AlternatorsEfficiency.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM0_Out_AlternatorsEfficiency.Location = New System.Drawing.Point(12, 65)
        Me.txtM0_Out_AlternatorsEfficiency.Name = "txtM0_Out_AlternatorsEfficiency"
        Me.txtM0_Out_AlternatorsEfficiency.Size = New System.Drawing.Size(100, 20)
        Me.txtM0_Out_AlternatorsEfficiency.TabIndex = 1
        '
        'txtM0_Out_HVacElectricalCurrentDemand
        '
        Me.txtM0_Out_HVacElectricalCurrentDemand.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0,Byte),Integer), CType(CType(192,Byte),Integer), CType(CType(0,Byte),Integer))
        Me.txtM0_Out_HVacElectricalCurrentDemand.Location = New System.Drawing.Point(9, 25)
        Me.txtM0_Out_HVacElectricalCurrentDemand.Name = "txtM0_Out_HVacElectricalCurrentDemand"
        Me.txtM0_Out_HVacElectricalCurrentDemand.Size = New System.Drawing.Size(100, 20)
        Me.txtM0_Out_HVacElectricalCurrentDemand.TabIndex = 0
        '
        'chkClutchEngaged
        '
        Me.chkClutchEngaged.AutoSize = true
        Me.chkClutchEngaged.Location = New System.Drawing.Point(10, 20)
        Me.chkClutchEngaged.Name = "chkClutchEngaged"
        Me.chkClutchEngaged.Size = New System.Drawing.Size(102, 17)
        Me.chkClutchEngaged.TabIndex = 20
        Me.chkClutchEngaged.Text = "Clutch Engaged"
        Me.chkClutchEngaged.UseVisualStyleBackColor = true
        '
        'lblTotalCycleTimeSeconds
        '
        Me.lblTotalCycleTimeSeconds.AutoSize = true
        Me.lblTotalCycleTimeSeconds.Location = New System.Drawing.Point(10, 386)
        Me.lblTotalCycleTimeSeconds.Name = "lblTotalCycleTimeSeconds"
        Me.lblTotalCycleTimeSeconds.Size = New System.Drawing.Size(134, 13)
        Me.lblTotalCycleTimeSeconds.TabIndex = 14
        Me.lblTotalCycleTimeSeconds.Text = "Total Cycle Time Seconds "
        '
        'lblEngineSpeed
        '
        Me.lblEngineSpeed.AutoSize = true
        Me.lblEngineSpeed.Location = New System.Drawing.Point(8, 341)
        Me.lblEngineSpeed.Name = "lblEngineSpeed"
        Me.lblEngineSpeed.Size = New System.Drawing.Size(107, 13)
        Me.lblEngineSpeed.TabIndex = 13
        Me.lblEngineSpeed.Text = "Engine Speed (RPM)"
        '
        'lblEngineMotoringPower
        '
        Me.lblEngineMotoringPower.AutoSize = true
        Me.lblEngineMotoringPower.Location = New System.Drawing.Point(9, 297)
        Me.lblEngineMotoringPower.Name = "lblEngineMotoringPower"
        Me.lblEngineMotoringPower.Size = New System.Drawing.Size(117, 13)
        Me.lblEngineMotoringPower.TabIndex = 12
        Me.lblEngineMotoringPower.Text = "Engine Motoring Power"
        '
        'lblEngineDrivelineTorque
        '
        Me.lblEngineDrivelineTorque.AutoSize = true
        Me.lblEngineDrivelineTorque.Location = New System.Drawing.Point(9, 252)
        Me.lblEngineDrivelineTorque.Name = "lblEngineDrivelineTorque"
        Me.lblEngineDrivelineTorque.Size = New System.Drawing.Size(121, 13)
        Me.lblEngineDrivelineTorque.TabIndex = 11
        Me.lblEngineDrivelineTorque.Text = "Engine Driveline Torque"
        '
        'lblEngineDrivelinePower
        '
        Me.lblEngineDrivelinePower.AutoSize = true
        Me.lblEngineDrivelinePower.Location = New System.Drawing.Point(7, 210)
        Me.lblEngineDrivelinePower.Name = "lblEngineDrivelinePower"
        Me.lblEngineDrivelinePower.Size = New System.Drawing.Size(120, 13)
        Me.lblEngineDrivelinePower.TabIndex = 10
        Me.lblEngineDrivelinePower.Text = "Engine Driveline Power "
        '
        'txtTotalCycleTimeSeconds
        '
        Me.txtTotalCycleTimeSeconds.Location = New System.Drawing.Point(10, 403)
        Me.txtTotalCycleTimeSeconds.Name = "txtTotalCycleTimeSeconds"
        Me.txtTotalCycleTimeSeconds.Size = New System.Drawing.Size(100, 20)
        Me.txtTotalCycleTimeSeconds.TabIndex = 5
        '
        'txtEngineSpeed
        '
        Me.txtEngineSpeed.Location = New System.Drawing.Point(10, 354)
        Me.txtEngineSpeed.Name = "txtEngineSpeed"
        Me.txtEngineSpeed.Size = New System.Drawing.Size(100, 20)
        Me.txtEngineSpeed.TabIndex = 4
        '
        'txtEngineMotoringPower
        '
        Me.txtEngineMotoringPower.Location = New System.Drawing.Point(10, 314)
        Me.txtEngineMotoringPower.Name = "txtEngineMotoringPower"
        Me.txtEngineMotoringPower.Size = New System.Drawing.Size(100, 20)
        Me.txtEngineMotoringPower.TabIndex = 3
        '
        'txtEngineDrivelineTorque
        '
        Me.txtEngineDrivelineTorque.Location = New System.Drawing.Point(10, 269)
        Me.txtEngineDrivelineTorque.Name = "txtEngineDrivelineTorque"
        Me.txtEngineDrivelineTorque.Size = New System.Drawing.Size(100, 20)
        Me.txtEngineDrivelineTorque.TabIndex = 2
        '
        'txtEngineDrivelinePower
        '
        Me.txtEngineDrivelinePower.Location = New System.Drawing.Point(10, 225)
        Me.txtEngineDrivelinePower.Name = "txtEngineDrivelinePower"
        Me.txtEngineDrivelinePower.Size = New System.Drawing.Size(100, 20)
        Me.txtEngineDrivelinePower.TabIndex = 1
        '
        'resultCardContextMenu
        '
        Me.resultCardContextMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DeleteToolStripMenuItem})
        Me.resultCardContextMenu.Name = "resultCardContextMenu"
        Me.resultCardContextMenu.Size = New System.Drawing.Size(108, 26)
        '
        'DeleteToolStripMenuItem
        '
        Me.DeleteToolStripMenuItem.Name = "DeleteToolStripMenuItem"
        Me.DeleteToolStripMenuItem.Size = New System.Drawing.Size(107, 22)
        Me.DeleteToolStripMenuItem.Text = "Delete"
        '
        'ErrorProvider
        '
        Me.ErrorProvider.ContainerControl = Me
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'Dashboard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange
        Me.ClientSize = New System.Drawing.Size(1502, 794)
        Me.Controls.Add(Me.pnlMain)
        Me.Name = "Dashboard"
        Me.Text = "Auxiliaries Configuration"
        Me.pnlMain.ResumeLayout(false)
        Me.tabMain.ResumeLayout(false)
        Me.tabGeneralConfig.ResumeLayout(false)
        Me.tabGeneralConfig.PerformLayout
        Me.tabElectricalConfig.ResumeLayout(false)
        Me.tabElectricalConfig.PerformLayout
        CType(Me.gvResultsCardOverrun,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.gvResultsCardTraction,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.gvResultsCardIdle,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.gvElectricalConsumables,System.ComponentModel.ISupportInitialize).EndInit
        Me.tabPneumaticConfig.ResumeLayout(false)
        Me.pnlPneumaticsUserInput.ResumeLayout(false)
        Me.pnlPneumaticsUserInput.PerformLayout
        Me.pnlPneumaticAuxillaries.ResumeLayout(false)
        Me.pnlPneumaticAuxillaries.PerformLayout
        Me.tabHVACConfig.ResumeLayout(false)
        Me.tabHVACConfig.PerformLayout
        Me.tabPlayground.ResumeLayout(false)
        Me.tabPlayground.PerformLayout
        Me.pnlM13.ResumeLayout(false)
        Me.pnlM13.PerformLayout
        Me.pnlM12.ResumeLayout(false)
        Me.pnlM12.PerformLayout
        Me.pnlM11.ResumeLayout(false)
        Me.pnlM11.PerformLayout
        Me.pnlM10.ResumeLayout(false)
        Me.pnlM10.PerformLayout
        Me.pnlM9.ResumeLayout(false)
        Me.pnlM9.PerformLayout
        Me.pnlM8.ResumeLayout(false)
        Me.pnlM8.PerformLayout
        Me.Panel4.ResumeLayout(false)
        Me.Panel4.PerformLayout
        Me.Panel2.ResumeLayout(false)
        Me.Panel2.PerformLayout
        Me.Panel3.ResumeLayout(false)
        Me.Panel3.PerformLayout
        CType(Me.PictureBox1,System.ComponentModel.ISupportInitialize).EndInit
        Me.Panel1.ResumeLayout(false)
        Me.Panel1.PerformLayout
        Me.pnl_M3_Displays.ResumeLayout(false)
        Me.pnl_M3_Displays.PerformLayout
        Me.pnl_M2_Displays.ResumeLayout(false)
        Me.pnl_M2_Displays.PerformLayout
        Me.pnl_M1_Displays.ResumeLayout(false)
        Me.pnl_M1_Displays.PerformLayout
        Me.pnl_M05_Displays.ResumeLayout(false)
        Me.pnl_M05_Displays.PerformLayout
        Me.pnl_M0_Displays.ResumeLayout(false)
        Me.pnl_M0_Displays.PerformLayout
        Me.resultCardContextMenu.ResumeLayout(false)
        CType(Me.ErrorProvider,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)

End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents resultCardContextMenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents DeleteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ErrorProvider As System.Windows.Forms.ErrorProvider
    Friend WithEvents tabMain As System.Windows.Forms.TabControl
    Friend WithEvents tabGeneralConfig As System.Windows.Forms.TabPage
    Friend WithEvents cboCycle As System.Windows.Forms.ComboBox
    Friend WithEvents lblCycle As System.Windows.Forms.Label
    Friend WithEvents lblVehiceWeight As System.Windows.Forms.Label
    Friend WithEvents txtVehicleWeightKG As System.Windows.Forms.TextBox
    Friend WithEvents tabElectricalConfig As System.Windows.Forms.TabPage
    Friend WithEvents gvResultsCardOverrun As System.Windows.Forms.DataGridView
    Friend WithEvents gvResultsCardTraction As System.Windows.Forms.DataGridView
    Friend WithEvents lblResultsOverrun As System.Windows.Forms.Label
    Friend WithEvents lblResultsTractionOn As System.Windows.Forms.Label
    Friend WithEvents lblResultsIdle As System.Windows.Forms.Label
    Friend WithEvents chkSmartElectricals As System.Windows.Forms.CheckBox
    Friend WithEvents lblElectricalConsumables As System.Windows.Forms.Label
    Friend WithEvents gvElectricalConsumables As System.Windows.Forms.DataGridView
    Friend WithEvents txtDoorActuationTimeSeconds As System.Windows.Forms.TextBox
    Friend WithEvents txtAlternatorGearEfficiency As System.Windows.Forms.TextBox
    Friend WithEvents txtAlternatorMapPath As System.Windows.Forms.TextBox
    Friend WithEvents txtPowernetVoltage As System.Windows.Forms.TextBox
    Friend WithEvents lblDoorActuationTimeSeconds As System.Windows.Forms.Label
    Friend WithEvents lblAlternatorGearEfficiency As System.Windows.Forms.Label
    Friend WithEvents lblAlternatormapPath As System.Windows.Forms.Label
    Friend WithEvents lblPowerNetVoltage As System.Windows.Forms.Label
    Friend WithEvents tabPneumaticConfig As System.Windows.Forms.TabPage
    Friend WithEvents pnlPneumaticsUserInput As System.Windows.Forms.Panel
    Friend WithEvents btnActuationsMap As System.Windows.Forms.Button
    Friend WithEvents btnCompressorMap As System.Windows.Forms.Button
    Friend WithEvents lblPneumaticsVariablesTitle As System.Windows.Forms.Label
    Friend WithEvents lblActuationsMap As System.Windows.Forms.Label
    Friend WithEvents chkSmartAirCompression As System.Windows.Forms.CheckBox
    Friend WithEvents chkSmartRegeneration As System.Windows.Forms.CheckBox
    Friend WithEvents lblAdBlueDosing As System.Windows.Forms.Label
    Friend WithEvents chkRetarderBrake As System.Windows.Forms.CheckBox
    Friend WithEvents txtKneelingHeightMillimeters As System.Windows.Forms.TextBox
    Friend WithEvents lblAirSuspensionControl As System.Windows.Forms.Label
    Friend WithEvents cboDoors As System.Windows.Forms.ComboBox
    Friend WithEvents txtCompressorMap As System.Windows.Forms.TextBox
    Friend WithEvents lblCompressorGearEfficiency As System.Windows.Forms.Label
    Friend WithEvents txtCompressorGearRatio As System.Windows.Forms.TextBox
    Friend WithEvents lblCompressorGearRatio As System.Windows.Forms.Label
    Friend WithEvents txtCompressorGearEfficiency As System.Windows.Forms.TextBox
    Friend WithEvents lblCompressorMap As System.Windows.Forms.Label
    Friend WithEvents cboAirSuspensionControl As System.Windows.Forms.ComboBox
    Friend WithEvents lblDoors As System.Windows.Forms.Label
    Friend WithEvents cboAdBlueDosing As System.Windows.Forms.ComboBox
    Friend WithEvents lblKneelingHeightMillimeters As System.Windows.Forms.Label
    Friend WithEvents txtActuationsMap As System.Windows.Forms.TextBox
    Friend WithEvents pnlPneumaticAuxillaries As System.Windows.Forms.Panel
    Friend WithEvents lblPneumaticAuxillariesTitle As System.Windows.Forms.Label
    Friend WithEvents lblAdBlueNIperMinute As System.Windows.Forms.Label
    Friend WithEvents lblAirControlledSuspensionNIperMinute As System.Windows.Forms.Label
    Friend WithEvents lblBrakingNoRetarderNIperKG As System.Windows.Forms.Label
    Friend WithEvents lblBrakingWithRetarderNIperKG As System.Windows.Forms.Label
    Friend WithEvents lblBreakingPerKneelingNIperKGinMM As System.Windows.Forms.Label
    Friend WithEvents lblDeadVolBlowOutsPerLitresperHour As System.Windows.Forms.Label
    Friend WithEvents lblDeadVolumeLitres As System.Windows.Forms.Label
    Friend WithEvents lblNonSmartRegenFractionTotalAirDemand As System.Windows.Forms.Label
    Friend WithEvents lblOverrunUtilisationForCompressionFraction As System.Windows.Forms.Label
    Friend WithEvents lblPerDoorOpeningNI As System.Windows.Forms.Label
    Friend WithEvents lblPerStopBrakeActuationNIperKG As System.Windows.Forms.Label
    Friend WithEvents lblSmartRegenFractionTotalAirDemand As System.Windows.Forms.Label
    Friend WithEvents txtAdBlueNIperMinute As System.Windows.Forms.TextBox
    Friend WithEvents txtAirControlledSuspensionNIperMinute As System.Windows.Forms.TextBox
    Friend WithEvents txtBrakingNoRetarderNIperKG As System.Windows.Forms.TextBox
    Friend WithEvents txtBrakingWithRetarderNIperKG As System.Windows.Forms.TextBox
    Friend WithEvents txtBreakingPerKneelingNIperKGinMM As System.Windows.Forms.TextBox
    Friend WithEvents txtDeadVolBlowOutsPerLitresperHour As System.Windows.Forms.TextBox
    Friend WithEvents txtDeadVolumeLitres As System.Windows.Forms.TextBox
    Friend WithEvents txtNonSmartRegenFractionTotalAirDemand As System.Windows.Forms.TextBox
    Friend WithEvents txtOverrunUtilisationForCompressionFraction As System.Windows.Forms.TextBox
    Friend WithEvents txtPerDoorOpeningNI As System.Windows.Forms.TextBox
    Friend WithEvents txtPerStopBrakeActuationNIperKG As System.Windows.Forms.TextBox
    Friend WithEvents txtSmartRegenFractionTotalAirDemand As System.Windows.Forms.TextBox
    Friend WithEvents tabHVACConfig As System.Windows.Forms.TabPage
    Friend WithEvents tabPlayground As System.Windows.Forms.TabPage
    Friend WithEvents btnAlternatorMapPath As System.Windows.Forms.Button
    Friend WithEvents gvResultsCardIdle As System.Windows.Forms.DataGridView
    Friend WithEvents txtHVACFuellingLitresPerHour As System.Windows.Forms.TextBox
    Friend WithEvents lblHVACFuellingLitresPerHour As System.Windows.Forms.Label
    Friend WithEvents txtHVACMechanicalLoadPowerWatts As System.Windows.Forms.TextBox
    Friend WithEvents lblHVACMechanicalLoadPowerWatts As System.Windows.Forms.Label
    Friend WithEvents txtHVACElectricalLoadPowerWatts As System.Windows.Forms.TextBox
    Friend WithEvents lblHVACElectricalLoadPowerWatts As System.Windows.Forms.Label
    Friend WithEvents lblHVACTitle As System.Windows.Forms.Label
    Friend WithEvents lblM0Outputs As System.Windows.Forms.Label
    Friend WithEvents pnl_M0_Displays As System.Windows.Forms.Panel
    Friend WithEvents lblAlternatorsEfficiency As System.Windows.Forms.Label
    Friend WithEvents txtM0_Out_AlternatorsEfficiency As System.Windows.Forms.TextBox
    Friend WithEvents txtM0_Out_HVacElectricalCurrentDemand As System.Windows.Forms.TextBox
    Friend WithEvents lblOutHVACElectricalCurrentDemand As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents pnl_M05_Displays As System.Windows.Forms.Panel
    Friend WithEvents lblM05SmartalternatorSetEfficiency As System.Windows.Forms.Label
    Friend WithEvents lblM05_AlternatorsEfficiencyOverrun As System.Windows.Forms.Label
    Friend WithEvents lblM05SmartOverrunCurrent As System.Windows.Forms.Label
    Friend WithEvents lblM05_AlternatorsEfficiencyTraction As System.Windows.Forms.Label
    Friend WithEvents lblM05_SmartTractionCurrent As System.Windows.Forms.Label
    Friend WithEvents lblM05AlternatorsEfficiencyIdle As System.Windows.Forms.Label
    Friend WithEvents lblM05_SmartIdleCurrent As System.Windows.Forms.Label
    Friend WithEvents txtM05_out_AlternatorsEfficiencyOverrun As System.Windows.Forms.TextBox
    Friend WithEvents txtM05_out_SmartOverrunCurrent As System.Windows.Forms.TextBox
    Friend WithEvents txtM05_out_AlternatorsEfficiencyTraction As System.Windows.Forms.TextBox
    Friend WithEvents txtM05_out_SmartTractionCurrent As System.Windows.Forms.TextBox
    Friend WithEvents txtM05_Out_AlternatorsEfficiencyIdle As System.Windows.Forms.TextBox
    Friend WithEvents txtM05_OutSmartIdleCurrent As System.Windows.Forms.TextBox
    Friend WithEvents lblM1_HVACAverageLoad As System.Windows.Forms.Label
    Friend WithEvents pnl_M1_Displays As System.Windows.Forms.Panel
    Friend WithEvents lblM1_HVACFuelling As System.Windows.Forms.Label
    Friend WithEvents lblM1_AvgPowerDemandAtCrankHVACElectrics As System.Windows.Forms.Label
    Friend WithEvents lblM1_AveragePowerDemandAtAlternatorElectrics As System.Windows.Forms.Label
    Friend WithEvents lblM1_AveragePowerDemandAtCrank As System.Windows.Forms.Label
    Friend WithEvents txtM1_out_HVACFuelling As System.Windows.Forms.TextBox
    Friend WithEvents txtM1_out_AvgPwrAtCrankFromHVACElec As System.Windows.Forms.TextBox
    Friend WithEvents txtM1_out_AvgPowerDemandAtAlternatorHvacElectrics As System.Windows.Forms.TextBox
    Friend WithEvents txtM1_out_AvgPowerDemandAtCrankMech As System.Windows.Forms.TextBox
    Friend WithEvents lblM2AverageElectricalLoadTitle As System.Windows.Forms.Label
    Friend WithEvents pnl_M2_Displays As System.Windows.Forms.Panel
    Friend WithEvents lblM2_AvgPwrDmdAtCrankFromElectrics As System.Windows.Forms.Label
    Friend WithEvents lblM2_AveragePowerDemandAtAlternatorFromElectrics As System.Windows.Forms.Label
    Friend WithEvents txtM2_out_AvgPowerAtCrankFromElectrics As System.Windows.Forms.TextBox
    Friend WithEvents txtM2_out_AvgPowerAtAltFromElectrics As System.Windows.Forms.TextBox
    Friend WithEvents lblM3_AveragePneumaticLoad As System.Windows.Forms.Label
    Friend WithEvents pnl_M3_Displays As System.Windows.Forms.Panel
    Friend WithEvents lblM3_TotAirConsumptionPerCycleLitres As System.Windows.Forms.Label
    Friend WithEvents lbl_M3_AvgPowerAtCrankFromPneumatics As System.Windows.Forms.Label
    Friend WithEvents txtM3_out_TotalAirConsumedPerCycleInLitres As System.Windows.Forms.TextBox
    Friend WithEvents txtM3_out_AveragePowerAtCrankFromPneumatics As System.Windows.Forms.TextBox
    Friend WithEvents lblM4_AirCompressor As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblM4_PowerAtCrankFromPSCompressorON As System.Windows.Forms.Label
    Friend WithEvents lblM4_PowerAtCrankFromPSCompressorOFF As System.Windows.Forms.Label
    Friend WithEvents lblM4_CompressorOnOffPowerDelta As System.Windows.Forms.Label
    Friend WithEvents lblM4_CompressorFlowRate As System.Windows.Forms.Label
    Friend WithEvents txtM4_out_PowerAtCrankFromPneumaticsCompressorON As System.Windows.Forms.TextBox
    Friend WithEvents txtM4_out_PowerAtCrankFromPneumaticsCompressorOFF As System.Windows.Forms.TextBox
    Friend WithEvents txtM4_out_CompresssorPwrOnMinusPwrOff As System.Windows.Forms.TextBox
    Friend WithEvents txtM4_out_CompressorFlowRate As System.Windows.Forms.TextBox
    Friend WithEvents lblM5_SmartAltSetGeneration As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents M5_AlternatorGenerationPowerAtCrankOverrunWatts As System.Windows.Forms.Label
    Friend WithEvents txtM5_out_AltRegenPowerAtCrankOverrunWatts As System.Windows.Forms.TextBox
    Friend WithEvents lblM5_AltRegenPowerAtCrankIdleW As System.Windows.Forms.Label
    Friend WithEvents txtM5_out_AltRegenPowerAtCrankTractionWatts As System.Windows.Forms.TextBox
    Friend WithEvents txtM5_out_AltRegenPowerAtCrankIdleWatts As System.Windows.Forms.TextBox
    Friend WithEvents lblM5_AlternatorRegenPowerAtCrankTractionWatts As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents btnFinish As System.Windows.Forms.Button
    Friend WithEvents btnStart As System.Windows.Forms.Button
    Friend WithEvents chkClutchEngaged As System.Windows.Forms.CheckBox
    Friend WithEvents lblTotalCycleTimeSeconds As System.Windows.Forms.Label
    Friend WithEvents lblEngineSpeed As System.Windows.Forms.Label
    Friend WithEvents lblEngineMotoringPower As System.Windows.Forms.Label
    Friend WithEvents lblEngineDrivelineTorque As System.Windows.Forms.Label
    Friend WithEvents lblEngineDrivelinePower As System.Windows.Forms.Label
    Friend WithEvents txtTotalCycleTimeSeconds As System.Windows.Forms.TextBox
    Friend WithEvents txtEngineSpeed As System.Windows.Forms.TextBox
    Friend WithEvents txtEngineMotoringPower As System.Windows.Forms.TextBox
    Friend WithEvents txtEngineDrivelineTorque As System.Windows.Forms.TextBox
    Friend WithEvents txtEngineDrivelinePower As System.Windows.Forms.TextBox
    Friend WithEvents chkInNeutral As System.Windows.Forms.CheckBox
    Friend WithEvents chkIdle As System.Windows.Forms.CheckBox
    Friend WithEvents lblPreExistingAuxPower As System.Windows.Forms.Label
    Friend WithEvents txtPreExistingAuxPower As System.Windows.Forms.TextBox
    Friend WithEvents lblM6Title As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblM6_SmartPneumaticsOnlyCompressorFlag As System.Windows.Forms.Label
    Friend WithEvents txtM6_out_SmartPneumaticsOnlyCompressorFlag As System.Windows.Forms.TextBox
    Friend WithEvents lblM6_AveragePowerDemandAtCrankFromElectricsIncHVAC As System.Windows.Forms.Label
    Friend WithEvents lblM6_SmartPneumaticOnlyAirCompPowerGenAtCrank As System.Windows.Forms.Label
    Friend WithEvents txtM6_out_AveragePowerDemandAtCrankFromElectricsIncHVAC As System.Windows.Forms.TextBox
    Friend WithEvents txtM6_out_SmartPneumaticOnlyAirCompPowerGenAtCrank As System.Windows.Forms.TextBox
    Friend WithEvents lblM6_AveragePowerDemandAtCrankFromPneumatics As System.Windows.Forms.Label
    Friend WithEvents lblM6_SmarElectricalOnlyAltPowerGenAtCrank As System.Windows.Forms.Label
    Friend WithEvents txtM6_out_AveragePowerDemandAtCrankFromPneumatics As System.Windows.Forms.TextBox
    Friend WithEvents txtM6_out_SmarElectricalOnlyAltPowerGenAtCrank As System.Windows.Forms.TextBox
    Friend WithEvents lblM6_SmartElectricalAndPneumaticAirCompPowerGenAtCrank As System.Windows.Forms.Label
    Friend WithEvents lblM6_SmartElectriclAndPneumaticsAltPowerGenAtCrank As System.Windows.Forms.Label
    Friend WithEvents txtM6_out_SmartElectricalAndPneumaticAirCompPowerGenAtCrank As System.Windows.Forms.TextBox
    Friend WithEvents txtM6_out_SmartElectriclAndPneumaticsAltPowerGenAtCrank As System.Windows.Forms.TextBox
    Friend WithEvents lblM6_SmartElectricalAndPneumaticsCompressorFlag As System.Windows.Forms.Label
    Friend WithEvents lblM6_OverrunFlag As System.Windows.Forms.Label
    Friend WithEvents txtM6_out_SmartElectricalAndPneumaticsCompressorFlag As System.Windows.Forms.TextBox
    Friend WithEvents txtM6_out_OverrunFlag As System.Windows.Forms.TextBox
    Friend WithEvents lblM7_Title As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents lblM7_SmartPneumaticsOnlyAux_AirCompPwrRegenAtCrank As System.Windows.Forms.Label
    Friend WithEvents lblM7_SmartElecOnly_AltPwrGenAtCrank As System.Windows.Forms.Label
    Friend WithEvents txtM7_out_SmartPneumaticsOnlyAux_AirCompPwrRegenAtCrank As System.Windows.Forms.TextBox
    Friend WithEvents txtM7_out_SmartElecOnlyAux_AltPwrGenAtCrank As System.Windows.Forms.TextBox
    Friend WithEvents lblM7_SmartElectricalAndPneumaticAux_AirCompPowerGenAtCrank As System.Windows.Forms.Label
    Friend WithEvents lblM7_SmarElectricalAndPneumaticsAux_AltPowerGenAtCrank As System.Windows.Forms.Label
    Friend WithEvents txtM7_out_SmartElectricalAndPneumaticAux_AirCompPowerGenAtCrank As System.Windows.Forms.TextBox
    Friend WithEvents txtM7_out_SmartElectricalAndPneumaticsAux_AltPowerGenAtCrank As System.Windows.Forms.TextBox
    Friend WithEvents lblM8_Title As System.Windows.Forms.Label
    Friend WithEvents pnlM8 As System.Windows.Forms.Panel
    Friend WithEvents lblM8CompressorFlag As System.Windows.Forms.Label
    Friend WithEvents txtM8_out_CompressorFlag As System.Windows.Forms.TextBox
    Friend WithEvents lblM8SmartElectricalAltPwrGenAtCrank As System.Windows.Forms.Label
    Friend WithEvents lblM8AuxPowerAtCrankFromAllAncillaries As System.Windows.Forms.Label
    Friend WithEvents txtM8_out_SmartElectricalAltPwrGenAtCrank As System.Windows.Forms.TextBox
    Friend WithEvents txtM8_out_AuxPowerAtCrankFromAllAncillaries As System.Windows.Forms.TextBox
    Friend WithEvents btnFuelMap As System.Windows.Forms.Button
    Friend WithEvents lblFuelMap As System.Windows.Forms.Label
    Friend WithEvents txtFuelMap As System.Windows.Forms.TextBox
    Friend WithEvents pnlM9 As System.Windows.Forms.Panel
    Friend WithEvents lblM9TotalCycleFuelConsumptionCompressorOnContinuously As System.Windows.Forms.Label
    Friend WithEvents txtM9_out_TotalCycleFuelConsumptionCompressorOnContinuously As System.Windows.Forms.TextBox
    Friend WithEvents lblM9LitresOfAirCompressoryOnlyOnInOverrun As System.Windows.Forms.Label
    Friend WithEvents lblM9LitresOfAirCompressorOnContinuously As System.Windows.Forms.Label
    Friend WithEvents txtM9_out_LitresOfAirCompressorOnlyOnInOverrun As System.Windows.Forms.TextBox
    Friend WithEvents txtM9_out_LitresOfAirConsumptionCompressorONContinuously As System.Windows.Forms.TextBox
    Friend WithEvents lblM9Title As System.Windows.Forms.Label
    Friend WithEvents lblM9TotalCycleFuelConsumptionCompressorOFFContinuously As System.Windows.Forms.Label
    Friend WithEvents txtM9_out_TotalCycleFuelConsumptionCompressorOFFContinuously As System.Windows.Forms.TextBox
    Friend WithEvents pnlM10 As System.Windows.Forms.Panel
    Friend WithEvents lblM10_FuelConsumptionWithSmartPneumaticsAndAverageElectricalPowerDemand As System.Windows.Forms.Label
    Friend WithEvents lblM10_BaseFuelConsumptionAithAverageAuxLoads As System.Windows.Forms.Label
    Friend WithEvents txtM10_out_FCWithSmartPSAndAvgElecPowerDemand As System.Windows.Forms.TextBox
    Friend WithEvents txtM10_out_BaseFCWithAvgAuxLoads As System.Windows.Forms.TextBox
    Friend WithEvents lblM10_Title As System.Windows.Forms.Label
    Friend WithEvents chkSignalsSmartAirCompression As System.Windows.Forms.CheckBox
    Friend WithEvents chkSignalsSmartElectrics As System.Windows.Forms.CheckBox
    Friend WithEvents mblM11_Title As System.Windows.Forms.Label
    Friend WithEvents pnlM11 As System.Windows.Forms.Panel
    Friend WithEvents lblM11_TotalCycleFuelConsumptionZeroElectricalLoad As System.Windows.Forms.Label
    Friend WithEvents txtM11_out_TotalCycleFuelConsumptionZeroElectricalLoad As System.Windows.Forms.TextBox
    Friend WithEvents lblM11_TotalCycleFuelConsumptionSmartElectricalLoad As System.Windows.Forms.Label
    Friend WithEvents txtM11_out_TotalCycleFuelConsumptionSmartElectricalLoad As System.Windows.Forms.TextBox
    Friend WithEvents lblM11_TotalCycleElectricalDemand As System.Windows.Forms.Label
    Friend WithEvents txtM11_out_TotalCycleElectricalDemand As System.Windows.Forms.TextBox
    Friend WithEvents lblM11_SmartElectricalTotalCycleElectricalEnergyGenerated As System.Windows.Forms.Label
    Friend WithEvents lblM11_TotalCycleElectricalEnergyGenOverrunOnly As System.Windows.Forms.Label
    Friend WithEvents txtM11_out_SmartElectricalTotalCycleElectricalEnergyGenerated As System.Windows.Forms.TextBox
    Friend WithEvents txtM11_out_TotalCycleElectricalEnergyGenOverrunOnly As System.Windows.Forms.TextBox
    Friend WithEvents pnlM12 As System.Windows.Forms.Panel
    Friend WithEvents lblM12_FuelConsumptionWithSmartElectricsAndAveragePneumaticPowerDemand As System.Windows.Forms.Label
    Friend WithEvents txtM12_out_FuelConsumptionWithSmartElectricsAndAveragePneumaticPowerDemand As System.Windows.Forms.TextBox
    Friend WithEvents lblM12_Title As System.Windows.Forms.Label
    Friend WithEvents lblM13_Title As System.Windows.Forms.Label
    Friend WithEvents pnlM13 As System.Windows.Forms.Panel
    Friend WithEvents lblM13_FinalFCOverTheCycle As System.Windows.Forms.Label
    Friend WithEvents txtM13_out_TotalCycleFuelCalculationGramsGRAMS As System.Windows.Forms.TextBox
    Friend WithEvents btnLoad As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents txtM13_out_TotalCycleFuelCalculationGramsLITRES As System.Windows.Forms.TextBox
    Friend WithEvents lblM13TotalFuelConsumptionTotalCycleLitres As System.Windows.Forms.Label
    Friend WithEvents btnSSMBSource As System.Windows.Forms.Button
    Friend WithEvents lblSSMFilePath As System.Windows.Forms.Label
    Friend WithEvents txtSSMFilePath As System.Windows.Forms.TextBox
    Friend WithEvents chkEngineStopped As System.Windows.Forms.CheckBox

End Class
